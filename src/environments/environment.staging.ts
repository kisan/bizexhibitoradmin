// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  greenCloudConfig:{
    ApiBaseUrl: "http://greencloud.kisanlab.com/v1/",
    AppKey: "LDH9nI9755yuGLiN9KoisVKJ32z9kf86",
    EventCode: "KISAN21",
    Source: "exhibitoradmin",
    logo_url_prefix: "http://greencloud.kisanlab.com/images/exhibitors/logo/",
    product_url_prefix:"http://greencloud.kisanlab.com/images/exhibitors/products/",
    vendor_stall_designs_url_prefix:"http://greencloud.kisanlab.com/images/exhibitors/stall_designs/",
    page_size:10,
    K19ApiBaseUrl: "http://greencloud.kisan.in/k19/v1/",
    K19EventCode: "KISAN19",
    ImagesMaxFileSizeInMB : 8
  },
  oAuthConfig:{
    ApiBaseUrl: "https://id.kisanlab.com/",
    ClientId: "1c714eee-0db2-431a-a941-9b27b2c4d64b",
    ClientSecret: "61678097-4baa-472c-9fa7-ea628a5d2675"
  },
   samvaadConfig:{
    webFormLink: "http://crm-webform-development.ap-south-1.elasticbeanstalk.com/index.php/Infos?phone_number=",
    webFormLinkOrg: "http://crm-webform-development.ap-south-1.elasticbeanstalk.com/index.php/InfosOrg?phone_number="
  }
};
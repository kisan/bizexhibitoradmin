// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  greenCloudConfig:{
    /*ApiBaseUrl: "http://greencloud.kisan.in/v1/",
    logo_url_prefix: "http://greencloud.kisan.in/images/exhibitors/logo/",
    product_url_prefix:"http://greencloud.kisan.in/images/exhibitors/products/",
    vendor_stall_designs_url_prefix:"http://greencloud.kisan.in/images/exhibitors/stall_designs/",
    vistor_report:"http://greenpassadmin.kisan.in/#/visitors",
    ApiBaseUrl: "https://greencloud.kisanlab.com/v1/",
    logo_url_prefix: "https://greencloud.kisanlab.com/images/exhibitors/logo/",
    product_url_prefix:"https://greencloud.kisanlab.com/images/exhibitors/products/",
    vendor_stall_designs_url_prefix:"https://greencloud.kisanlab.com/images/exhibitors/stall_designs/",
    vistor_report:"http://greenpassadmin.kisanlab.com/greenpass18admin/#/visitors",*/
    ApiBaseUrl: "http://localhost/greenpassservice/bizgreencloud/code/v1/",
    logo_url_prefix: "http://localhost/greenpassservice/bizgreencloud/code/images/exhibitors/logo/",
    product_url_prefix:"http://localhost/greenpassservice/bizgreencloud/code/images/exhibitors/products/",
    vendor_stall_designs_url_prefix:"http://localhost/greenpassservice/bizgreencloud/code/mages/exhibitors/stall_designs/",
    vistor_report:"http://localhost/greenpassservice/greenpassadmin/code/#/visitors",
    /*ApiBaseUrl: "http://localhost/greenpass18api/v1/",
    logo_url_prefix: "http://localhost/greenpass18api/images/exhibitors/logo/",
    product_url_prefix:"http://localhost/greenpass18api/images/exhibitors/products/",
    vendor_stall_designs_url_prefix:"http://localhost/greenpass18api/mages/exhibitors/stall_designs/",
    vistor_report:"http://localhost/greenpassservice/greenpassadmin/code/#/visitors",*/
    AppKey: "LDH9nI9755yuGLiN9KoisVKJ32z9kf86",
    EventCode: "KISANBIZ22",
    Source: "exhibitoradmin",   
    page_size:10,
    K19ApiBaseUrl: "http://greencloud.kisan.in/k19/v1/",
    K19EventCode: "KISAN19",
    ImagesMaxFileSizeInMB : 8
  },
  oAuthConfig:{
    // ApiBaseUrl: "https://www.id.kisanlab.com/",
    ApiBaseUrl: "http://127.0.0.1:8000/",
    ClientId: "1c714eee-0db2-431a-a941-9b27b2c4d64b",
    ClientSecret: "61678097-4baa-472c-9fa7-ea628a5d2675"
    // ApiBaseUrl: "https://id.kisan.in/",
    // ClientId: "b1b80273-e332-4e0f-955e-f244f8e29f05",
    // ClientSecret: "7d845439-1108-4f8e-8e96-4cca73508676"
  },
   samvaadConfig:{
    webFormLink: "http://crm-webform-development.ap-south-1.elasticbeanstalk.com/index.php/Infos?phone_number=",
    webFormLinkOrg: "http://crm-webform-development.ap-south-1.elasticbeanstalk.com/index.php/InfosOrg?phone_number="
  }
};

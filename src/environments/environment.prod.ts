// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  greenCloudConfig:{
    ApiBaseUrl: "https://greencloud.kisan.in/v1/",
    AppKey: "LDH9nI9755yuGLiN9KoisVKJ32z9kf86",
    EventCode: "KISAN21",
    Source: "exhibitoradmin",
    logo_url_prefix: "https://greencloud.kisan.in/images/exhibitors/logo/",
    vistor_report:"https://greenpassadmin.kisan.in/#/visitors",
    product_url_prefix:"https://greencloud.kisan.in/images/exhibitors/products/",
    vendor_stall_designs_url_prefix:"https://greencloud.kisan.in/images/exhibitors/stall_designs/",
    page_size:100,
    K19ApiBaseUrl: "https://greencloud.kisan.in/k19/v1/",
    K19EventCode: "KISAN19",
    ImagesMaxFileSizeInMB : 8
  },
  oAuthConfig:{
    ApiBaseUrl: "https://id.kisan.in/",
    ClientId: "b1b80273-e332-4e0f-955e-f244f8e29f05",
    ClientSecret: "7d845439-1108-4f8e-8e96-4cca73508676"
  },
   samvaadConfig:{
    webFormLink: "http://crm-webform-production.fxqavi34w8.ap-south-1.elasticbeanstalk.com/index.php/Infos?phone_number=",
    webFormLinkOrg: "http://crm-webform-production.fxqavi34w8.ap-south-1.elasticbeanstalk.com/index.php/InfosOrg?phone_number="
  }
};
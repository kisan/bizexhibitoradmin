import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { AuthModule } from './auth/auth.module';
// import { HttpModule } from '@angular/http';
import { CustomMaterialModule } from './custom-material/custom-material.module';
import { LoginComponent } from './auth/login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { AuthGuard } from './auth/auth-guard.service';
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './pagenotfound/pagenotfound.component';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { NgImageSliderModule } from 'ng-image-slider';
@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AuthModule,
    CoreModule,
    // HttpModule,
    CustomMaterialModule,
    HttpClientModule,
    Ng2GoogleChartsModule,
    AppRoutingModule,
    PdfViewerModule,
    CKEditorModule,
    NgImageSliderModule,
    TooltipModule.forRoot(),
  ],
  providers: [AuthGuard, LoginComponent],
  bootstrap: [AppComponent],
  exports:[CKEditorModule]
})
export class AppModule { }

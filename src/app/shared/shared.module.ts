import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { HttpModule } from '@angular/http';
import { TruncatePipe } from './pipes/truncate.pipe';
import { OrderByPipe } from './pipes/orderby.pipe';

@NgModule({
  imports: [
    CommonModule,
    // HttpModule
  ],
  providers: [],
  declarations: [TruncatePipe, OrderByPipe],
  exports: [TruncatePipe, OrderByPipe],
})
export class SharedModule { }

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from '../shared/shared.service';
import { DashboardService } from './dashboard.service';
import { StallsService  } from '../stalls/stalls.service';
import { environment } from '../../environments/environment';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [DashboardService , SharedService]
})
export class DashboardComponent implements OnInit {

    public bookStallLabels:string[] = [];
    public bookStallChartData:number[] = [];
    public bookStallChartType:string = 'doughnut';
    public bookStallChartColors: any[] = [{ backgroundColor: ["#008D48", "#795A2E", "#4ECCF2","#006835","#006368","#7E5435","#F99D1C","#00e600","#8C3030","#14B368","#9aa92f","#F99D1C","#3777BC","#059849","#DFC922","#C41425"] }];
    public salesDistribution: any[] ;
    public gstChartLabels:string[] = ["With GST","Without GST"];
    public gstChartData:number[] = [];
    public gstChartType:string = 'doughnut';
    public gstChartColors: any[] = [{ backgroundColor: ["#DFC922","#BFBFBF"] }];
    public sales:any;

  isSuccess: boolean;
  message : string;
  pieChartData : any;
  stalls=[];
  pagesize=environment.greenCloudConfig.page_size;
  masterdata={'pavillion_types':[],"stand_types":[],"stand_locations":[]}
  badges_issued={"total_badges": "32","used_badges": "21","percent": 30};
  
  constructor(public dashboardService: DashboardService, private router:Router, private sharedService: SharedService,private snackBar: MatSnackBar,
    private stallsService: StallsService) { }

    openMessage(message: string) {
        let action = null;
        this.snackBar.open(message, action, {
          duration: 10000,
        });
      }
  ngOnInit() {
    this.dashboardService.get_stalls_summary();
   
    document.getElementsByTagName("body")[0].setAttribute("id", "bgcolorGray");
    this.sharedService.getMasterData().then(response => {           
        //console.log(response);
        if(response.success)
        {
            this.masterdata=response.eventDetails; 
            this.get_stalls();
           
        }
      });
      this.dashboardService.get_sales_distribution_data().then(response => {
     
          if(response['success']){
            this.salesDistribution = response['data']['sales_distribution']['data'];
            let salesDataTable = [];
            salesDataTable.push(['Task', 'Hours per Day']);
            for(this.sales in this.salesDistribution){
                let sales_record = [];
               
                sales_record.push(this.salesDistribution[this.sales]['name']);
                sales_record.push(this.salesDistribution[this.sales]['count']);
                salesDataTable.push(sales_record);
               
                this.pieChartData =  {
                    chartType: 'PieChart',
                   
                    dataTable: salesDataTable,
                    options: {
                        pieHole:0.3,
                        title: 'Sales Distribution',
                        legend:'left',
                        chartArea:{left:10,top:10,width:"100%",height:"100%"},
                        
                       
                        height:350
                    },
                  };
                
            }
          
             //this.openMessage(response['message']);
          }
          else{
            //this.openMessage(response['message']);
          }
        })
        .catch(response => {
         console.log(response);
        })
   
  }

  setStallListFormStatus(status)
  {
    document.getElementById('loader').style.display="block"; 
    localStorage.setItem('stallListFormStatus', status);
     this.router.navigateByUrl('stalls/stall-list');
  }
  

   get_stalls(){ 
    document.getElementById('loader').style.display="block";     
    let stallListParams = {
        "app_key": environment.greenCloudConfig.AppKey,
        "source": environment.greenCloudConfig.Source,
        "eventCode": environment.greenCloudConfig.EventCode,
        "pagesize":this.pagesize,
        "currentpage":0,            
        "sort_col":"id",
        "sort_dir":"DESC"            
    }
    //console.log(JSON.stringify(stallListParams));
    this.stallsService.get_stalls(stallListParams).then(res => { 
        //console.log(res);
        if(res.responseCode==0 && res.success==true)
        {
            this.isSuccess = true;
            //console.log(res);
            this.stalls=res.overview.records;              
        }
        else
        {
            this.isSuccess = false;
            // console.log(res);
            this.message = res.message;
        }
        document.getElementById('loader').style.display="none";
    });
}

    get_pavillion_name(short_code)
  {
      for (let pav of this.masterdata.pavillion_types) {
          if(pav.short_code==short_code)
          {
            return pav.name;
          }
      }
      return '';
  }

  get_stand_type_name(short_code)
  {
      for (let stand_type of this.masterdata.stand_types) {
          if(stand_type.short_code==short_code)
          {
            return stand_type.name;
          }
      }
      return '';
  }

  get_stand_location_name(short_code)
  {
      for (let stand_loc of this.masterdata.stand_locations) {
          if(stand_loc.short_code==short_code)
          {
            return stand_loc.name;
          }
      }
      return '';
  }
  getBase64URIencode(x)
  {
      return this.sharedService.getBase64URIencode(x);
  }
  getBase64URIdecode(x)
  {
      return this.sharedService.getBase64URIdecode(x);
  }
}

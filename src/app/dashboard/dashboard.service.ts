import { Injectable } from '@angular/core';
import {Router} from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class DashboardService {
  stalls_status_summary={};
  public sales_distribution_data=[];
  isSuccess: boolean;
  message : string;
  constructor(private router:Router, private http: HttpClient) { }
   get_stalls_summary() {  
    const req = this.http.post(environment.greenCloudConfig.ApiBaseUrl +'report_stall_status_summary.php', {
      app_key: environment.greenCloudConfig.AppKey,
      source: environment.greenCloudConfig.Source,
      eventCode: environment.greenCloudConfig.EventCode    
     
    })
      .subscribe(
        (res:any) => {
          if(res.responseCode==0 && res.success==true){
              this.isSuccess = true;
              
              this.stalls_status_summary=res.overview;             
          }
          else{
            this.isSuccess = false;
            // console.log(res);
            this.message = res.message;
          }
        },
        err => {
          this.isSuccess = false;
          console.log("Error occurred");
          console.log(err);
          this.message = err.message;
        }
      );
  }
 
  get_sales_data(){
    this.get_sales_distribution_data();
    
    return this.sales_distribution_data;
  }
  get_sales_distribution_data(){
    let params = {
      "app_key": environment.greenCloudConfig.AppKey,
      "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode, 
      "reports":["sales_distribution"]   
    }

    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(environment.greenCloudConfig.ApiBaseUrl + "getusage.php" , JSON.stringify(params), {headers: headers})
      .toPromise();
  }

}

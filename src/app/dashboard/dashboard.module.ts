import { NgModule }      from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AuthGuard } from '../auth/auth-guard.service';
import { SharedService } from '../shared/shared.service';
import { DashboardService } from './dashboard.service';
import { StallsService } from '../stalls/stalls.service';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { CustomMaterialModule } from '../custom-material/custom-material.module';
import { ChartsModule } from 'ng2-charts'; 
import { Ng2GoogleChartsModule } from 'ng2-google-charts';

@NgModule({
  imports:      [ 
    DashboardRoutingModule,
    FormsModule,
    CommonModule,
    CustomMaterialModule,
    ChartsModule,
    Ng2GoogleChartsModule
   ],
   providers: [AuthGuard , DashboardService , SharedService , StallsService],
  declarations: [ 
    DashboardComponent
  ],
  entryComponents: [DashboardComponent],
})
export class DashboardModule { }
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommingsoonRoutingModule } from './commingsoon-routing.module';
import { CommingsoonComponent } from './commingsoon/commingsoon.component';

@NgModule({
  imports: [
    CommonModule,
    CommingsoonRoutingModule
  ],
  declarations: [CommingsoonComponent]
})
export class CommingsoonModule { }

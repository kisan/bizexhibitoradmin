import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommingsoonComponent } from './commingsoon/commingsoon.component';

const routes: Routes = [
  { path: '', redirectTo: 'comming-soon', pathMatch: 'full'},
  { path: 'comming-soon', component: CommingsoonComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommingsoonRoutingModule { }

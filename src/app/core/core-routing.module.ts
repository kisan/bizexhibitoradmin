import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { LoginComponent } from '../auth/login/login.component';
import { AuthGuard } from '../auth/auth-guard.service';
import { PageNotFoundComponent } from '../pagenotfound/pagenotfound.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'reports',
        loadChildren: () => import('../reports/reports.module').then(m => m.ReportsModule),
      },
      {
        path: 'stalls',
        loadChildren: () => import('../stalls/stalls.module').then(m => m.StallsModule),
        canActivate: [AuthGuard]
      },
      {
        path: 'dashboard',
        loadChildren: () => import('../dashboard/dashboard.module').then(m => m.DashboardModule),
        canActivate: [AuthGuard]
      },
      {
        path: 'comming-soon',
        loadChildren: () => import('../commingsoon/commingsoon.module').then(m => m.CommingsoonModule),
        canActivate: [AuthGuard]
      },
      {
        path: 'services',
        loadChildren: () => import('../services/services.module').then(m => m.ServicesModule),
        canActivate: [AuthGuard]
      },
      {
        path: 'profile',
        loadChildren: () => import('../profile/profile.module').then(m => m.ProfileModule),
        canActivate: [AuthGuard]
      },
      {
        path: 'proposal',
        loadChildren: () => import('../proposal/proposal.module').then(m => m.ProposalModule),
        canActivate: [AuthGuard]
      },
      {
        path: 'konnect',
        loadChildren: () => import('../konnect/konnect.module').then(m => m.KonnectModule),
        canActivate: [AuthGuard]
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }

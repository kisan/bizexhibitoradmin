import { Component, OnInit } from '@angular/core';
import { LoginComponent } from '../../auth/login/login.component';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { SharedService } from '../../shared/shared.service';

@Component({
  selector: 'app-left-menubar',
  templateUrl: './left-menubar.component.html',
  styleUrls: ['./left-menubar.component.scss']
})
export class LeftMenubarComponent implements OnInit {

  oauth_base_url = environment.oAuthConfig.ApiBaseUrl;
  constructor( private loginComponent: LoginComponent, public router: Router,
    private route: ActivatedRoute ,  private sharedService:SharedService) { }

  loggedInUserName: string;
  leftMenu: any = [];
  visitorlink: string;
  ngOnInit() {
    this.loggedInUserName = localStorage.getItem('loggedInUserName')
    //This is for displaying the left menubar
    this.route.queryParams.subscribe(params => {
      //console.log(params);
      let offer_type = params['offer_type'];
      if(offer_type=='proposal' || offer_type=='proforma invoice')
      {
        localStorage.setItem('offer_type', offer_type);
      }
      
    });
    
    this.visitorlink= environment.greenCloudConfig.vistor_report;
    this.leftMenu = [
    //   {
    //   title: 'Dashboard',
    //   icon: 'bar_chart',
    //   path: '/dashboard'
    // },
    /*{
      title: 'Booking Requests',
      icon: 'subject',
      path: '/stalls/stall-list'
    },
    {
      title: 'Marketing Services',
      icon: 'business_center',
      path: '/proposal/dashboard'
    },*/
    /*{
      title: 'Good2Go',
      icon: 'thumb_up_alt',
      path: '/stalls/good2go'
    },
    {
      title: 'Exhibitors',
      icon: 'person_outline',
      path: '/stalls/exhibitors'
    },
    {
      title: 'Fair Catalogue',
      icon: 'book',
      path: '/stalls/fair-catalogue-submited-list'
    },   
    {
       title: 'Furniture',
       icon: 'furniture',
       path: '/services/furniture'
    },
    {
      title: 'Vendor Forms',
      icon: 'assignment',
      path: '/services/vendor-list'
    },
    {
      title: 'Badges',
      icon: 'assignment_ind',
      path: '/services/badges'
    },
    {
      title: 'Invites',
      icon: 'mail_outline',
      path: '/services/invites'
    },
    {
      title: 'Kisan Konnect',
      icon: 'mail_outline',
      path: '/konnect/konnect-report'
    }*/
 
 
    /*{
      title: 'Reports',
      icon: 'trending_up',
      path: '/stalls/reports'
    }*/
    ];
  }

  logout() {
    this.loginComponent.logout();
  }

  checkAllowedRoles(roles) 
  {
    //console.log(roles);
    return this.sharedService.checkAllowedRoles(roles);
  }

  setOfferType(type)
  {
    localStorage.setItem('offer_type', type);
  }
  isOfferTypeMatched(type)
  {
    let offer_type=localStorage.getItem('offer_type');
    //console.log(offer_type);
    if(offer_type==undefined || offer_type==null)
    {
      offer_type="";
      // offer_type='proforma';
      // localStorage.setItem('offer_type', 'proforma invoice');
    }
   
    return offer_type==type;
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { LeftMenubarComponent } from './left-menubar/left-menubar.component';
import { ReportHeaderComponent } from './report-header/report-header.component';
import { CustomMaterialModule } from '../custom-material/custom-material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    CoreRoutingModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [LayoutComponent, LeftMenubarComponent , ReportHeaderComponent]
})
export class CoreModule { }

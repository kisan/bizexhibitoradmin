import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-report-header',
  templateUrl: './report-header.component.html',
  styleUrls: ['./report-header.component.scss']
})
export class ReportHeaderComponent implements OnInit {

  constructor( public router: Router, private route: ActivatedRoute) { }

  loggedInUserName: string;
  
  ngOnInit() {
    this.loggedInUserName = localStorage.getItem('loggedInUserName');  
    document.getElementsByTagName("body")[0].setAttribute("id", "bgcolorGreen"); 
  } 

}

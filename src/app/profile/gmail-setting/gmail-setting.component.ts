import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { ProfileService } from '../profile.service';
import { SharedService } from '../../shared/shared.service';
import { FormControl, FormBuilder, FormGroup, Validators, AbstractControl, NG_VALIDATORS, ValidatorFn, Validator } from '@angular/forms';


@Component({
  selector: 'app-gmail-setting',
  templateUrl: './gmail-setting.component.html',
  styleUrls: ['./gmail-setting.component.scss']
})
export class GmailSettingComponent implements OnInit {

  constructor(public dialog: MatDialog, private profileService: ProfileService, private router:Router, 
    private sharedService: SharedService ,private route: ActivatedRoute) { }

  pwd='';
  email='';
  isSuccess: boolean;
  message : string;
  returnUrl='';
  ngOnInit() {
    let gmailId=localStorage.getItem('gmailId');
    if(gmailId!=undefined && gmailId != null && gmailId!='')
    {
      this.email=gmailId;
    }
    this.route.queryParams.subscribe(params => {
      //console.log(params);
      this.returnUrl = params['returnUrl'];
    });
  }

  validateEmail()
  {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(this.email).toLowerCase());
  }

  onSubmit(form) {
    //console.log('form submitted');
    //console.log(form);
   
    if(form.valid && this.validateEmail() )
    {
      document.getElementById('loader').style.display="block"; 
      let params={"id":this.email,"password":this.sharedService.getBase64encode(this.pwd)};
      this.profileService.updateGmailSetting(params).then(res => {
        // console.log(res);
        //this.openApproveDialog();
        if(res.responseCode==0 && res.success==true)
        {
          this.isSuccess = true;
          localStorage.setItem('gmailId', this.email);
          this.openSuccessDialog(); 
        }
        else
        {
            this.isSuccess = false;
            // console.log(res);
            this.message = res.message;
        }
        document.getElementById('loader').style.display="none"; 
      },
      err=>{ 
        console.log(err.error);
        this.isSuccess = false;
        // console.log(res);
        this.message = err.error.message;
      }); 
    }
  }
    
  openSuccessDialog() {
    let dialogRef = this.dialog.open(SaveGmailPopupComponent);
    window.setTimeout(function() {dialogRef.close();},2000);
    dialogRef.afterClosed().subscribe(value => {
      //console.log(`Dialog sent: ${value}`); 
      if(this.returnUrl==undefined || this.returnUrl==null || this.returnUrl=='')
      {
        this.router.navigateByUrl('/dashboard');
      }
      else
      {
        this.router.navigateByUrl(this.sharedService.getBase64URIdecode(this.returnUrl));
      }
      
    });
  }
}

/*save popup */
@Component({
  selector: 'app-save-gmail-popup',
  template: `
<div class="errorMsgPopup">
<mat-dialog-actions class="pad-all-sm">
        <button mat-dialog-close class="closeBtn">
        <i class="material-icons">close</i>
        </button>
    </mat-dialog-actions>
  <mat-dialog-content>
  <mat-list>
  <mat-list-item>
    <mat-icon mat-list-icon><i class="material-icons">check</i></mat-icon>
    <h4 mat-line class="font-bold-six" style="padding-top: 10px;">Gmail Settings Saved Successfully.</h4>
  </mat-list-item>
</mat-list>
  </mat-dialog-content>
  </div>
`
})
export class SaveGmailPopupComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<SaveGmailPopupComponent>, public dialog: MatDialog) { }
  ngOnInit() {
  }
}

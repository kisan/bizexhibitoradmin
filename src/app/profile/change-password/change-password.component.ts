import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { ProfileService } from '../profile.service';
import { SharedService } from '../../shared/shared.service';
import { FormControl, FormBuilder, FormGroup, Validators, AbstractControl, NG_VALIDATORS, ValidatorFn, Validator } from '@angular/forms';


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  constructor(public dialog: MatDialog, private profileService: ProfileService, private router:Router, private sharedService: SharedService) { }

  pwd='';
  confirm_pwd='';
  isSuccess: boolean;
  message : string;
  ngOnInit() {

  }

  validatePwd()
  {
    var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
		if(this.pwd!='' && strongRegex.test(this.pwd) && this.pwd==this.confirm_pwd)
		{
			return true;
		}
		else
		{
			return false;
		}
  }
  onSubmit(form) {
    //console.log('form submitted');
    //console.log(form);
    if(form.valid && this.validatePwd())
    {
      document.getElementById('loader').style.display="block"; 
      this.profileService.changePassword(this.pwd).then(res => {
        // console.log(res);
        //this.openApproveDialog();
        if(res.responseCode==0 && res.success==true)
        {
          this.isSuccess = true;
          this.openSuccessDialog(); 
        }
        else
        {
            this.isSuccess = false;
            // console.log(res);
            this.message = res.message;
        }
        document.getElementById('loader').style.display="none"; 
      }); 
    }
  }
    
  openSuccessDialog() {
    let dialogRef = this.dialog.open(SavePwdPopupComponent);
    window.setTimeout(function() {dialogRef.close();},2000);
    dialogRef.afterClosed().subscribe(value => {
      //console.log(`Dialog sent: ${value}`); 
      this.router.navigateByUrl('/dashboard');
    });
  }
}

/*save popup */
@Component({
  selector: 'app-save-popup',
  template: `
<div class="errorMsgPopup">
<mat-dialog-actions class="pad-all-sm">
        <button mat-dialog-close class="closeBtn">
        <i class="material-icons">close</i>
        </button>
    </mat-dialog-actions>
  <mat-dialog-content>
  <mat-list>
  <mat-list-item>
    <mat-icon mat-list-icon><i class="material-icons">check</i></mat-icon>
    <h4 mat-line class="font-bold-six" style="padding-top: 10px;">Password changed Successfully.</h4>
  </mat-list-item>
</mat-list>
  </mat-dialog-content>
  </div>
`
})
export class SavePwdPopupComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<SavePwdPopupComponent>, public dialog: MatDialog) { }
  ngOnInit() {
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CustomMaterialModule } from '../custom-material/custom-material.module';
import { ProfileRoutingModule } from './profile-routing.module';
import { ChangePasswordComponent,SavePwdPopupComponent } from './change-password/change-password.component';
import { ProfileService } from './profile.service';
import { SharedService } from '../shared/shared.service';
import {  GmailSettingComponent , SaveGmailPopupComponent } from './gmail-setting/gmail-setting.component';
import { BlockCopyPasteDirective } from '../shared/block-copy-paste.directive';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CustomMaterialModule,
    ProfileRoutingModule    
  ],
  declarations: [ChangePasswordComponent,SavePwdPopupComponent,BlockCopyPasteDirective,GmailSettingComponent,SaveGmailPopupComponent],
  entryComponents:[SavePwdPopupComponent,SaveGmailPopupComponent],
  providers: [ProfileService, SharedService],
})
export class ProfileModule { }

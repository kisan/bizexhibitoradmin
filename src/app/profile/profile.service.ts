import { Injectable } from '@angular/core';
// import { Observable } from 'rxjs/Observable';
import {Router} from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class ProfileService {
  isSuccess: boolean;
  message : string;
  sessionId : string;

  constructor(private router:Router, private http: HttpClient) {  }

  changePassword(password: string): Promise<any> {
  
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let Params={
        "app_key": environment.greenCloudConfig.AppKey,
        "source": environment.greenCloudConfig.Source,
        "eventCode": environment.greenCloudConfig.EventCode,
        "sessionId": localStorage.getItem('sessionId'),
        "password": password
    }; 
    let body = JSON.stringify(Params);
    //console.log( body); 
   // console.log(environment.greenCloudConfig.ApiBaseUrl+"updatestallinfo.php");
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"change_password.php", body, { headers: headers })
    .toPromise()
  }
  
  updateGmailSetting(params): Promise<any> {
  
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let Params={
        "app_key": environment.greenCloudConfig.AppKey,
        "source": environment.greenCloudConfig.Source,
        "eventCode": environment.greenCloudConfig.EventCode,
        "sessionId": localStorage.getItem('sessionId'),
        "id": params.id,
        "password": params.password
    }; 
    let body = JSON.stringify(Params);
    //console.log( body); 
   // console.log(environment.greenCloudConfig.ApiBaseUrl+"updatestallinfo.php");
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"updateusergmailsetting.php", body, { headers: headers })
    .toPromise()
  } 
}
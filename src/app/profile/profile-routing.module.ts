import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/auth-guard.service';
import { ChangePasswordComponent } from './change-password/change-password.component';
import {  GmailSettingComponent  } from './gmail-setting/gmail-setting.component';


const routes: Routes = [
  { path: '', redirectTo: 'change-password', pathMatch: 'full'},
  {path: 'change-password', component: ChangePasswordComponent},
  {path: 'gmail-setting', component: GmailSettingComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }

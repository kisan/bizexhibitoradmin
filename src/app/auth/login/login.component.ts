import { Component, OnInit, HostBinding } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { SharedService } from '../../shared/shared.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  username: string;
  password: string;

  constructor(public authService: AuthService, private router:Router, private sharedService: SharedService) {
   /* console.log('In this log in constructor');
    console.log(this.router.url);
    console.log(this.router);
    if(this.authService.isLoggedIn())
    {
        this.router.navigate(['dashboard/overview']);
    }*/
  }
  ngOnInit() {
    if(this.authService.isLoggedIn())
    {
        this.router.navigate(['proposal/stands']);
        return false;
    }
    document.getElementsByTagName("body")[0].setAttribute("id", "bgcolorGreen");
    document.getElementById('login').style.display="block";
    document.getElementById('loader').style.display="none";
  }
  login() {
    this.authService.login(this.username, this.password);
  }

  logout() {
    this.authService.logout();
    this.username = this.password = '';
    this.sharedService.clearLocalStorage();
    this.router.navigate(['login']);
  }

}

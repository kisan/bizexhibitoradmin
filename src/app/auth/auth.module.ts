import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { AuthService } from './auth.service';
import { FormsModule } from '@angular/forms';
import { AuthRoutingModule } from './auth-routing.module';
import { CustomMaterialModule } from '../custom-material/custom-material.module';
import { SharedService } from '../shared/shared.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AuthRoutingModule,
    CustomMaterialModule
  ],
  declarations: [
    LoginComponent
  ],
  providers: [AuthService, SharedService],
})
export class AuthModule { }

import { Injectable } from '@angular/core';
import { CanActivate, Router ,ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';
import { LoginComponent } from '../auth/login/login.component';
 
@Injectable()
export class AuthGuard implements CanActivate{
  public allowed: boolean;
  
  constructor(private authService: AuthService, private router: Router , private loginComponent: LoginComponent) {
    
   }
 
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    
    if(this.authService.isLoggedIn())
    {
      //return true;  
      this.authService.update_session_active().then(res => {
        // console.log(res);
        //this.openApproveDialog();
        if(res.responseCode==0 && res.success==true)
        {
          //return true;  
        }
        else
        {
          if(res.responseCode===104)
          {
            //$rootScope.logout();
            if(state.url!="/login")
            {
              localStorage.setItem('landing_page', state.url);
            } 
            this.loginComponent.logout();
          }
         // return true;  
        }
        
      },
      err=>{ 
        console.log(err.error);
       // return true; 
      }); 
      return true;
    }              
    else
    {
      if(state.url!="/login")
      {
         localStorage.setItem('landing_page', state.url);
      }     
      this.router.navigateByUrl('/login');
    }
      
    return false;
  }
}
import { Injectable } from '@angular/core';
//import { Observable } from 'rxjs/Observable';
import {Router} from "@angular/router";
import { HttpClient , HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthService {
  isSuccess: boolean;
  message : string;
  sessionId : string;

  constructor(private router:Router, private http: HttpClient) {  }

  login(uname: string, pwd: string) {
    this.isSuccess = true;
    this.message = '';
    document.getElementById('loader').style.display="block";
    const req = this.http.post(environment.greenCloudConfig.ApiBaseUrl +'signin.php', {
      app_key: environment.greenCloudConfig.AppKey,
      username: environment.greenCloudConfig.EventCode + '-' + uname,
      password: pwd,
      source: environment.greenCloudConfig.Source,
      eventCode: environment.greenCloudConfig.EventCode
    })
      .subscribe(
        (res:any) => {
          if(res.responseCode==0 && res.success==true){
            //console.log(res);
            this.isSuccess = true;
            // this.user = res.profileDetails;
            this.sessionId = res.profileDetails.sessionId;
            let roles=[];
            for (let role_detail of res.profileDetails.roleinfo) {            
              if(role_detail.eventcode==environment.greenCloudConfig.EventCode)
              {
                roles.push(role_detail.role);
              }
            }
            //console.log(roles);
            localStorage.setItem('loggedUserRoles', JSON.stringify(roles));
            //console.log(localStorage.getItem('loggedUserRoles'));
            localStorage.setItem('sessionId', this.sessionId);
            localStorage.setItem('loggedInUserName', res.profileDetails.firstname + ' ' + res.profileDetails.lastname);
            //console.log(localStorage.getItem('landing_page'));
            localStorage.setItem('gmailId', res.profileDetails.gmail_id);
            if(localStorage.getItem('landing_page') && localStorage.getItem('landing_page')!='')
            {
                 this.router.navigateByUrl(localStorage.getItem('landing_page'));
                 localStorage.removeItem('landing_page');
            }
            else
            {
               this.router.navigate(['proposal/stands']);
            }           
          }
          else
          {
            this.isSuccess = false;
            // console.log(res);
            this.message = res.message;
          }
          document.getElementById('loader').style.display="none";
        },
        err => {
         // this.isSuccess = false;
          // console.log("Error occurred");
          // console.log(err);
          //this.message = err.message;
          document.getElementById('loader').style.display="none";
        }
      );
  }

  logout() {
    //Signout API call
  }

  isLoggedIn(){
    const sessionId = localStorage.getItem('sessionId');
    // console.log("isLoggedIn called");
    // console.log("SessionId: ", sessionId);
    if(sessionId!=null){
      return true;
    }
    else{
      return false;
    }
  }

  //update_session_active(): Promise<any>
  update_session_active(): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let Params={
        "app_key": environment.greenCloudConfig.AppKey,
        "source": environment.greenCloudConfig.Source,
        "eventCode": environment.greenCloudConfig.EventCode,
        "sessionId": localStorage.getItem('sessionId')
    }; 
    //console.log(Params);
    let body = JSON.stringify(Params);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"update_session_active.php", body, { headers: headers })
    .toPromise()
  }
}
  

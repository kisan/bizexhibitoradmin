import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StallListComponent } from './stall-list/stall-list.component';
import { StallDetailsComponent } from './stall-details/stall-details.component';
import { EditStallDetailsComponent } from './edit-stall-details/edit-stall-details.component';
import { ExhibitorsComponent } from './exhibitors/exhibitors.component';
import { FairCatalogueSubmitedListComponent } from './fair-catalogue-submited-list/fair-catalogue-submited-list.component';
import { FairCataloguePendingListComponent } from './fair-catalogue-pending-list/fair-catalogue-pending-list.component'; 
import { Good2GoComponent } from './good2go/good2go.component';  
import { InvoicesComponent } from './invoices/invoices.component'; 
import { QuotaComponent } from './quota/quota.component'; 


const routes: Routes = [
  {path: '', redirectTo: 'stall-list',pathMatch: 'full'},
  {path: 'stall-list', component: StallListComponent},
  {path: 'stall-details/:id', component:StallDetailsComponent},
  {path: 'edit-stall-details/:id', component: EditStallDetailsComponent},
  {path: 'exhibitors', component: ExhibitorsComponent},
  {path: 'fair-catalogue-submited-list', component: FairCatalogueSubmitedListComponent},
  {path: 'fair-catalogue-pending-list', component: FairCataloguePendingListComponent},
  {path: 'good2go', component: Good2GoComponent},
  {path: 'invoices', component: InvoicesComponent},
  {path: 'quota', component: QuotaComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StallsRoutingModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FairCatalogueSubmitedListComponent } from './fair-catalogue-submited-list.component';

describe('FairCatalogueSubmitedListComponent', () => {
  let component: FairCatalogueSubmitedListComponent;
  let fixture: ComponentFixture<FairCatalogueSubmitedListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FairCatalogueSubmitedListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FairCatalogueSubmitedListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

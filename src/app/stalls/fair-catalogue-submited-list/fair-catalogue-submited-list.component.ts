import { Component, OnInit,ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { StallsService  } from '../stalls.service';
import { SharedService  } from '../../shared/shared.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-fair-catalogue-submited-list',
  templateUrl: './fair-catalogue-submited-list.component.html',
  styleUrls: ['./fair-catalogue-submited-list.component.scss']
})
export class FairCatalogueSubmitedListComponent implements OnInit {
  @ViewChildren('tooltip') tooltips;
  constructor(private stallsService: StallsService, private router:Router, private sharedService: SharedService) { }
  masterdata={'pavillion_types':[],"stand_types":[],"stand_locations":[]}    
  isSuccess: boolean;
  message : string;
  stalls=[];
  pagesize=environment.greenCloudConfig.page_size;
  stall_current_page=0;
  total_stalls=0;
  stall_filter={"pavillion_type":"","stand_type":"","stand_location":""};
  stall_pages=0;
  stall_sort_col="id";
  stall_sort_dir=1;
  stall_showall=0;
  stall_search='';
  stall_start_item=1;
  stall_end_item=environment.greenCloudConfig.page_size;
  ngOnInit() {
    this.sharedService.getMasterData().then(response => {           
      //console.log(response);
      if(response.success)
      {
          this.masterdata=response.eventDetails;  
          //console.log(this.masterdata);                   
          this.get_stalls();
          this.masterdata.pavillion_types.sort((a, b) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0);
      }
   });
  }

  get_stalls()
  {
    document.getElementById('loader').style.display="block"; 
    var sort_dir="ASC";
    if(this.stall_sort_dir==1)
    {
        sort_dir="DESC";
    }
    let stallListParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode,
      "pagesize":this.pagesize,
      "currentpage":this.stall_current_page,
      "search":this.stall_search,
      "sort_col":this.stall_sort_col,
      "sort_dir":sort_dir,
      "filter":this.stall_filter,
      "showall":this.stall_showall
    }
    
    this.stallsService.get_fair_cat_list(stallListParams).then(res => { 
       //console.log(res);
      if(res.responseCode==0 && res.success==true)
      {
          this.isSuccess = true;
          //console.log(res);
          this.stalls=res.overview.records;
          this.total_stalls=res.overview.total_stalls.cnt;
          this.update_stall_stastics();
      }
      else
      {
          this.isSuccess = false;
          // console.log(res);
          this.message = res.message;
      }
      document.getElementById('loader').style.display="none"; 
    });
  }

  update_stall_stastics() {    
    if(this.stall_showall)
    {
        this.stall_start_item = 1;
        this.stall_end_item = this.total_stalls;
        this.stall_pages=1;
    }
    else
    {
      this.stall_pages=Math.ceil(this.total_stalls/this.pagesize); 
      this.stall_start_item = ((this.stall_current_page*this.pagesize)+1);
      this.stall_end_item = ((this.stall_current_page*this.pagesize)+this.pagesize);	
      if( this.stall_end_item >this.total_stalls){
          this.stall_end_item =this.total_stalls;
      }
    }
  }

  sort_stalls(key) {
    if(this.stall_sort_col != key)
    {
        this.stall_sort_col=key;
        this.stall_sort_dir=0;
        this.stall_current_page = 0;
    }
    else
    {
        this.stall_sort_dir= 1-this.stall_sort_dir;
    }
    this.get_stalls();
  }

  toggle_show_all_stalls(){
    //this.stall_showall=1-this.stall_showall;
    this.stall_current_page=0;
		this.get_stalls();
  }
  search_stalls(event: any) { // without type info
    if(event.keyCode == 13)
		{
       // console.log(event.target.value);
        //this.stall_search=event.target.value;
        this.stall_current_page=0;
        this.get_stalls();
    }
  }
  get_pavillion_name(short_code)
  {
    for (let pav of this.masterdata.pavillion_types) {
        if(pav.short_code==short_code)
        {
          return pav.name;
        }
    }
    return '';
  }
  get_stand_type_name(short_code)
  {
    for (let stand_type of this.masterdata.stand_types) {
        if(stand_type.short_code==short_code)
        {
          return stand_type.name;
        }
    }
    return '';
  }
  get_stand_location_name(short_code)
  {
    for (let stand_loc of this.masterdata.stand_locations) {
        if(stand_loc.short_code==short_code)
        {
          return stand_loc.name;
        }
    }
    return '';
  }
  getBase64URIencode(x)
  {
    return this.sharedService.getBase64URIencode(x);
  }
  getBase64URIdecode(x)
  {
    return this.sharedService.getBase64URIdecode(x);
  }
  getFullLogoUrl(logourl)
  {
    if ((logourl) && (logourl != "")) 
    {
      return environment.greenCloudConfig.logo_url_prefix + logourl;
    }
    else 
    {
      return '../../assets/images/default_organization.png';
    }
  }
  checkAllowedRoles(roles)
  {
    //console.log(roles);
    return this.sharedService.checkAllowedRoles(roles);
  }
  exportFairCatCSV()
  {
    document.getElementById('loader').style.display="block"; 
    let stallListParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode,
      "pagesize":this.pagesize,
      "currentpage":0,     
      "showall":1
    }
    this.stallsService.get_fair_cat_list(stallListParams).then(res => { 
      //console.log(res);
      if(res.responseCode==0 && res.success==true)
      {
        var records=res.overview.records;
				records.forEach(function(v){ 
					delete v.exhibitor_id;									
					delete v.status;
					delete v.updated_date;				
					delete v.pavillion_type;
					delete v.stand_type;
					delete v.stand_location;});
        var csvdata=this.sharedService.JSONToCSVConvertor(records, "Fair Catalog Report", true);    
				if(csvdata)
				{
          var a         = document.createElement('a');
					a.href        = 'data:attachment/csv,' + encodeURIComponent(csvdata);
					a.target      = '_blank';
					a.download    = environment.greenCloudConfig.EventCode+'_FairCat.csv';
					document.body.appendChild(a);
          a.click();
          document.getElementById('loader').style.display="none"; 
        }
      }
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { EmailPopupComponent } from '../email-popup/email-popup.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss']
})
export class InvoicesComponent implements OnInit {

  constructor( public dialog: MatDialog,) { }

  ngOnInit() {
  }

  emailDialog() {
    this.dialog.open(EmailPopupComponent);
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute , Router ,} from '@angular/router';
import { environment } from '../../../environments/environment';
import { StallsService } from '../stalls.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SharedService } from '../../shared/shared.service';
import { LoginComponent } from '../../auth/login/login.component';
@Component({
  selector: 'app-stall-details',
  templateUrl: './stall-details.component.html',
  styleUrls: ['./stall-details.component.scss']
})
export class StallDetailsComponent implements OnInit {

  stallId: string;
  defaultExhibitorImage: string;
  stallDetails = {};
  stallDetailsNew = {};
  selectedFurnitureDetails = {};
  quotaDetails = [];
  pavillionImage: string;
  showFasciaName: boolean = false;
  showStallNumber: boolean = false;
  showTable: boolean = false;
  categoryList:any;
  tempCategory = [];
  finalCategory = [];
  productImages = [];
  prodImagePrefix: string;
  defaultVideoImage:String;
  defaultDocumentImage: string;
  supportCRMWebFormUrl: string;

  constructor(private loginComponent: LoginComponent, private route: ActivatedRoute, 
      private router:Router , private stallService: StallsService,
      public dialog: MatDialog, private sharedService:SharedService) { }

  ngOnInit() {
    this.route.params.forEach((urlParameters) => {
      this.stallId = this.getBase64URIdecode(urlParameters['id']);
      // console.log("Stall Id: ", this.stallId);
    });
    this.defaultVideoImage = "../../assets/images/greenvideo.png";
    this.defaultDocumentImage = "../../assets/images/greendoc.png";
    this.loaddata();
  }

  loaddata()
  {
   // document.getElementById('loader').style.display="block"; 
    let stallDetailsParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode,
      "sessionId": localStorage.getItem('sessionId'),
      "stallid": this.stallId
    }
    this.finalCategory = [];
    this.sharedService.getCategoryDetails().then(response => {
      this.categoryList = response["data"]["categories_list"];
      //console.log("this.categoryList",this.categoryList);
    this.stallService.getStallData(stallDetailsParams).then(stallData => {
      //console.log(stallData);
      if(!stallData.success && stallData.responseCode==104)
      {
        this.loginComponent.logout();
      }
      this.stallDetails = stallData;
      console.log(this.stallDetails['stallinfo']);
      this.selectedFurnitureDetails = this.stallDetails['stallinfo']['furniture_details'][0];

      if ((this.stallDetails['stallinfo']['logo_big_thumb']) && (this.stallDetails['stallinfo']['logo_big_thumb'] != "")) {
        this.defaultExhibitorImage =  this.stallDetails['stallinfo']['logo_big_thumb'];
      }
      else {
        this.defaultExhibitorImage = '../../assets/images/default_organization.png';
      }
      if( this.stallDetails["stallinfo"]["product_category"])
      {
        this.tempCategory = this.stallDetails["stallinfo"]["product_category"].split(',');
          for (let category in this.categoryList) {
            for (let j in this.tempCategory) {
              if (this.categoryList[category].name == this.tempCategory[j]) {
                this.finalCategory.push(this.categoryList[category]);
              }
            }
          }
      }
     
       
      this.prodImagePrefix = environment.greenCloudConfig.product_url_prefix;
      
      if(this.stallDetails["stallinfo"]["product_images"])
      {
          this.productImages = this.stallDetails["stallinfo"]["product_images"].split(',');
      }     
     
      this.quotaDetails = this.stallDetails['stallinfo']['quota_details'];

      if (this.stallDetails['stallinfo']['fascia_name']) {
        this.showFasciaName = true;
      }
      if (this.stallDetails['stallinfo']['alloted_stall_no']) {
        this.showStallNumber = true;
      }
      document.getElementById('loader').style.display="none"; 
      this.supportCRMWebFormUrl = environment.samvaadConfig.webFormLinkOrg+this.stallDetails['stallinfo']['booked_by_mobile'];
    });
  });
  }
 
    getBase64URIencode(x)
    {
        return this.sharedService.getBase64URIencode(x);
    }
    getBase64URIdecode(x)
    {
        return this.sharedService.getBase64URIdecode(x);
    }
  getRupeeFormat(amount)
  {
    return this.sharedService.getRupeeFormat(amount);
  }

  checkAllowedRoles(roles)
  {
    //console.log(roles);
    return this.sharedService.checkAllowedRoles(roles);
  }

  resendStallConfirmationFormLink() {
    document.getElementById('loader').style.display="block"; 
    let stallDetailsParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode,
      "sessionId": localStorage.getItem('sessionId'),
      "stallid": this.stallId
    }

    this.stallService.resendStallConfirmationFormLink(stallDetailsParams).then(res => {
      // console.log(res);
      document.getElementById('loader').style.display="none"; 
      this.openResendDialog();
    });

  }

  approveStall() {
    document.getElementById('loader').style.display="block"; 
    this.stallDetails['stallinfo']['status'] = 'Approved';
    let stallDetailsParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode,
      "sessionId": localStorage.getItem('sessionId'),
      "stallinfo": this.stallDetails['stallinfo']
    }

    this.stallService.changeStallStatus(stallDetailsParams).then(res => {
      // console.log(res);
      document.getElementById('loader').style.display="none"; 
      this.openApproveDialog();
    });
  }

  rejectStall() {
    document.getElementById('loader').style.display="block"; 
    this.stallDetails['stallinfo']['status'] = 'Rejected';
    this.stallDetails['stallinfo']['stall_reject_reason'] = 'Your product is not appropriate for Exhibition.';    
    let stallDetailsParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode,
      "sessionId": localStorage.getItem('sessionId'),
      "stallinfo": this.stallDetails['stallinfo']
    }    
    this.stallService.changeStallStatus(stallDetailsParams).then(res => {
      // console.log(res);
      this.loaddata();
    });
  }

  addVendorEntryForStall() {
    document.getElementById('loader').style.display="block"; 
    let stallDetailsParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode,
      "sessionId": localStorage.getItem('sessionId'),
      "stall_id": this.stallId
    }    
    this.stallService.send_vendor_form(stallDetailsParams).then(res => {
      // console.log(res);
      //this.loaddata();
      this.openVendorFormSentDialog();
    });
  }

  openResendDialog() {
    let dialogRef = this.dialog.open(ResendPopupComponent);
    dialogRef.afterClosed().subscribe(value => {
      //console.log(`Dialog sent: ${value}`); 
      //this.router.navigateByUrl('/stalls/stall-details/'+this.getBase64URIencode(this.stallId));
      document.getElementById('loader').style.display="block";
      this.loaddata();
    });
  }
  openApproveDialog() {
    let dialogRef = this.dialog.open(ApprovePopupComponent);
    dialogRef.afterClosed().subscribe(value => {
      //console.log(`Dialog sent: ${value}`); 
      //this.router.navigateByUrl('/stalls/stall-details/'+this.getBase64URIencode(this.stallId));
      document.getElementById('loader').style.display="block";
      this.loaddata();
    });
  }

  openVendorFormSentDialog() {
    document.getElementById('loader').style.display="none";
    let dialogRef = this.dialog.open(VendorFormSentComponent);
    dialogRef.afterClosed().subscribe(value => {
      //console.log(`Dialog sent: ${value}`); 
      //this.router.navigateByUrl('/stalls/stall-details/'+this.getBase64URIencode(this.stallId));
      document.getElementById('loader').style.display="block";
      this.loaddata();
    });
  }
  openApproveConfDialog() {
    let dialogRef = this.dialog.open(ApprovalConfPopupComponent,{
      panelClass: 'popup_dist',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'cancel') {
        return false;
      }
      if (result == 'save') {
        this.approveStall();
        return true;
      }
    }); 
  }

  openConfirmationDialog() {
    let dialogRef = this.dialog.open(ConfirmPopupComponent,{
      panelClass: 'popup_dist',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'cancel') {
        return false;
      }
      if (result == 'save') {
        this.rejectStall();
        return true;
      }
    }); //end dialogRef
  }

  openAddVendorEntryConfirmationDialog() {
    let dialogRef = this.dialog.open(AddVendorEntryConfirmPopupComponent,{
      panelClass: 'popup_dist',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'cancel') {
        return false;
      }
      if (result == 'save') {
        this.addVendorEntryForStall();
        return true;
      }
    }); //end dialogRef
  }
}

@Component({
  selector: 'app-error-popup',
  template: `
<div class="errorMsgPopup">
<mat-dialog-actions class="pad-all-sm">
        <button mat-dialog-close class="closeBtn">
        <i class="material-icons">close</i>
        </button>
    </mat-dialog-actions>
  <mat-dialog-content>
  <mat-list>
  <mat-list-item>
    <mat-icon mat-list-icon><i class="material-icons">check</i></mat-icon>
    <h4 mat-line class="font-bold-six">Exhibitor Details form link resent successfully</h4>
    <p mat-line>Exhibitor Details form link resent successfully. An email has been sent to the exhibitor for filling the form.</p>
  </mat-list-item>

</mat-list>
  </mat-dialog-content>
  </div>
`
})
export class ResendPopupComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<ResendPopupComponent>, public dialog: MatDialog) { }
  ngOnInit() {
  }
}

@Component({
  selector: 'app-approve-popup',
  template: `
<div class="errorMsgPopup">
<mat-dialog-actions class="pad-all-sm">
        <button mat-dialog-close class="closeBtn">
        <i class="material-icons">close</i>
        </button>
    </mat-dialog-actions>
  <mat-dialog-content>
  <mat-list>
  <mat-list-item>
    <mat-icon mat-list-icon><i class="material-icons">check</i></mat-icon>
    <h4 mat-line class="font-bold-six">Participation has been approved</h4>
    <p mat-line>The stall has been approved successfully. An email has been sent to the exhibitor for accessing the dashboard.</p>
  </mat-list-item>

</mat-list>
  </mat-dialog-content>
  </div>
`
})
export class ApprovePopupComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<ApprovePopupComponent>, public dialog: MatDialog) { }
  ngOnInit() {
  }
}

@Component({
  selector: 'app-approve-popup',
  template: `
<div class="errorMsgPopup">
<mat-dialog-actions class="pad-all-sm">
        <button mat-dialog-close class="closeBtn">
        <i class="material-icons">close</i>
        </button>
    </mat-dialog-actions>
  <mat-dialog-content>
  <mat-list>
  <mat-list-item>
    <mat-icon mat-list-icon><i class="material-icons">check</i></mat-icon>
    <h4 mat-line class="font-bold-six">Vendor form sent successfully</h4>  
  </mat-list-item>
</mat-list>
  </mat-dialog-content>
  </div>
`
})
export class VendorFormSentComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<VendorFormSentComponent>, public dialog: MatDialog) { }
  ngOnInit() {
  }
}

/* send confirmation popup */
@Component({
  providers: [StallDetailsComponent],
  selector: 'app-confirm-popup',
  template: `
  <div class="confirmMsgPopup mrgn-t-lg">
  <div class="pad-l-md pad-r-md">
  <mat-dialog-actions class="pad-all-sm">
  <button mat-dialog-close class="closeBtn">
  <i class="material-icons">close</i>
  </button>
  </mat-dialog-actions>
  <mat-dialog-content class="mrgn-b-md">
    <h4 class="mrgn-b-md">Confirmation</h4>
    <p>Are you sure you want to send vendor form to this exhibitor ?</p>
  </mat-dialog-content>
  </div>
  <div class="footer-btn">
  <mat-card-footer>
    <button type="button" mat-raised-button mat-dialog-close class="grayBtn" (click)="dialogRef.close('cancel')">Cancel</button>
    <button type="button" mat-raised-button class="greennewBtn" (click)="dialogRef.close('save')">Yes</button>
    </mat-card-footer>
  </div>
  </div>
  `
})
export class AddVendorEntryConfirmPopupComponent implements OnInit {
  stallDetailsLocal = {};
  constructor(public dialogRef: MatDialogRef<AddVendorEntryConfirmPopupComponent>, public dialog: MatDialog,
    private stallDetailsComponentRef: StallDetailsComponent) { }

  ngOnInit() {
  }
}

@Component({
  providers: [StallDetailsComponent],
  selector: 'app-confirm-popup',
  template: `
  <div class="confirmMsgPopup mrgn-t-lg">
  <div class="pad-l-md pad-r-md">
  <mat-dialog-actions class="pad-all-sm">
  <button mat-dialog-close class="closeBtn">
  <i class="material-icons">close</i>
  </button>
  </mat-dialog-actions>
  <mat-dialog-content class="mrgn-b-md">
    <h4 class="mrgn-b-md">Confirmation</h4>
    <p>Are you sure you wish to reject this participation. This action cannot be reversed.</p>
  </mat-dialog-content>
  </div>
  <div class="footer-btn">
  <mat-card-footer>
    <button type="button" mat-raised-button mat-dialog-close class="grayBtn" (click)="dialogRef.close('cancel')">Cancel</button>
    <button type="button" mat-raised-button class="redBtn" (click)="dialogRef.close('save')">Yes, Reject</button>
    </mat-card-footer>
  </div>
  </div>
  `
})
export class ConfirmPopupComponent implements OnInit {
  stallDetailsLocal = {};
  constructor(public dialogRef: MatDialogRef<ConfirmPopupComponent>, public dialog: MatDialog,
    private stallDetailsComponentRef: StallDetailsComponent) { }

  ngOnInit() {
  }
}
/* approval confirmation popup */
@Component({
  selector: 'app-check-popup',
  template: `
  <div class="confirmMsgPopup approvalConfPopup mrgn-t-lg">
   <div class="pad-l-md pad-r-md">
    <mat-dialog-actions class="pad-all-sm">
    <button mat-dialog-close class="closeBtn">
    <i class="material-icons">close</i>
    </button>
    </mat-dialog-actions>
    <mat-dialog-content class="mrgn-b-md">
      <h4 class="mrgn-b-md">Confirmation</h4>
      <p>Are you sure you want to approve this stand. Make sure you have checked the following -</p>

      <mat-list role="list">
        <mat-list-item role="listitem"> <mat-checkbox [(ngModel)]="cbProductDescription">I have checked the product description.</mat-checkbox></mat-list-item>
        <mat-list-item role="listitem"> <mat-checkbox [(ngModel)]="cbImages">I have reviewed the product images.</mat-checkbox></mat-list-item>
        <mat-list-item role="listitem"> <mat-checkbox [(ngModel)]="cbOrganizationDetails">I have reviewed the organization details.</mat-checkbox></mat-list-item>
        <mat-list-item role="listitem"> <mat-checkbox [(ngModel)]="cbOrganizationBlacklist">I have checked if the organization is not blacklisted.</mat-checkbox></mat-list-item>
      </mat-list>
    </mat-dialog-content>
   </div>
    <div class="footer-btn">
    <mat-card-footer>
      <button type="button" mat-raised-button mat-dialog-close class="blueBtn" (click)="dialogRef.close('cancel')">Cancel</button>
      <button type="button" mat-raised-button [disabled]="!cbImages || !cbProductDescription || !cbOrganizationDetails || !cbOrganizationBlacklist" 
      [ngClass]="(!cbImages || !cbProductDescription || !cbOrganizationDetails || !cbOrganizationBlacklist)?'grayBtn':'greennewBtn'"
      (click)="dialogRef.close('save')">Yes, Approve</button>
      </mat-card-footer>
    </div>
</div>
`
})
export class ApprovalConfPopupComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<ApprovalConfPopupComponent>, public dialog: MatDialog) { }
  ngOnInit() {
  }
}
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StallDetailsComponent } from './stall-details.component';

describe('StallDetailsComponent', () => {
  let component: StallDetailsComponent;
  let fixture: ComponentFixture<StallDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StallDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StallDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

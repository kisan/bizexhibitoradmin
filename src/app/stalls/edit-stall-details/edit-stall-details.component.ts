import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router , ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { StallsService } from '../stalls.service';
import { SharedService } from '../../shared/shared.service';
import { FormControl, FormBuilder, FormGroup, Validators, AbstractControl, NG_VALIDATORS, ValidatorFn, Validator } from '@angular/forms';
//import * as $ from 'jquery';
const ORG_NAME_MARATHI = /^[^a-zA-Z]*$/;
@Component({
  selector: 'app-edit-stall-details',
  templateUrl: './edit-stall-details.component.html',
  styleUrls: ['./edit-stall-details.component.scss']
})
export class EditStallDetailsComponent implements OnInit {
  masterdata={'pavillion_types':[],"stand_types":[],"stand_locations":[]}
  checked = true;
  stallId: string;
  defaultExhibitorImage: string;
  stallDetails = {};
  pavillionImage: string;
  showFasciaName: boolean = false;
  showStallNumber: boolean = false;
  showTable: boolean = false;
  categoryList:any;
  tempCategory = [];
  finalCategory = [];
  productImages = [];
  prodImagePrefix: string;
  countries={};
  states={};
  objectKeys = Object.keys;
  cities: any[] = [];
  showaddGST=false;
  isSKUValid=true;
  isSuccess: boolean;
  message : string;
  constructor(public dialog: MatDialog, private router:Router ,private route: ActivatedRoute, private stallService: StallsService,private sharedService:SharedService) {
     
  }
  orgNameMarathiFormControl = new FormControl('',[
  
    
    Validators.pattern(ORG_NAME_MARATHI)
  
  ]);

  ngOnInit() {

    //document.getElementById('loader').style.display="block";
    this.route.params.forEach((urlParameters) => {
      this.stallId = this.getBase64URIdecode(urlParameters['id']);
      // console.log("Stall Id: ", this.stallId);
    });

    let stallDetailsParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode,
      "sessionId": localStorage.getItem('sessionId'),
      "stallid": this.stallId
    }
    this.countries=Object.keys(this.sharedService.getCountries());
    this.states=this.sharedService.getStates();
    this.sharedService.getCategoryDetails().then(response => {
      this.categoryList = response["data"]["categories_list"];
      //console.log("this.categoryList",this.categoryList);
      this.sharedService.getMasterData().then(response => {           
        //console.log(response);
        if(response.success)
        {
          this.masterdata=response.eventDetails; 
          this.stallService.getStallData(stallDetailsParams).then(stallData => {
            // console.log(this.productImages);
          this.stallDetails = stallData;
          console.log(this.stallDetails);
          if ((this.stallDetails['stallinfo']['logo_big_thumb']) && (this.stallDetails['stallinfo']['logo_big_thumb'] != "")) {
            console.log(this.stallDetails['stallinfo']['logo_big_thumb']);
            this.defaultExhibitorImage =  this.stallDetails['stallinfo']['logo_big_thumb'];
          }
          else 
          {
            this.defaultExhibitorImage = '../../assets/images/default_organization.png';
          }
          if( this.stallDetails["stallinfo"]["product_category"])
          {
            this.tempCategory = this.stallDetails["stallinfo"]["product_category"].split(',');
              for (let category in this.categoryList) {
                for (let j in this.tempCategory) {
                  if (this.categoryList[category].name == this.tempCategory[j]) {
                    this.finalCategory.push(this.categoryList[category]);
                  }
                }
              }
          }      
          this.prodImagePrefix = environment.greenCloudConfig.product_url_prefix;
          if(this.stallDetails["stallinfo"]["product_images"])
          {
              this.productImages = this.stallDetails["stallinfo"]["product_images"].split(',');
              console.log(this.productImages);
          }     

          //this.quotaDetails = this.stallDetails['stallinfo']['quota_details'];

          if (this.stallDetails['stallinfo']['fascia_name']) {
            this.showFasciaName = true;
          }
          if (this.stallDetails['stallinfo']['alloted_stall_no']) {
            this.showStallNumber = true;
          }  
          if(this.stallDetails['stallinfo']['state'])
          {
              for (let i in this.states) {
                if (i === this.stallDetails['stallinfo']['state']) {
                  //this.showCities(this.states[i]);
                  this.cities = [];
                  this.cities = this.states[this.stallDetails['stallinfo']['state']];
              }
            } 
          }
          if (!this.stallDetails['stallinfo']['is_GSTIN']) {
            this.showaddGST = true;
          }            
        });
      }
      document.getElementById('loader').style.display="none";
    });
  });
  }
  changeSKU(){
    if(this.validateSKU())
    {
      //console.log('valid SKU now change params');
      this.isSKUValid=true;
      let sku=this.stallDetails['stallinfo']['sku'];
      var pav_type_code= sku.charAt(3);
      var stand_type_code=sku.charAt(4);
      var stand_loc_code=sku.charAt(5);
      this.stallDetails['stallinfo']['pavillion_type']=pav_type_code;
      this.stallDetails['stallinfo']['stand_type']=stand_type_code;
      this.stallDetails['stallinfo']['stand_location']=stand_loc_code;
      this.stallDetails['stallinfo']['area']=parseInt(sku.substring(6));
      var pav_name,stand_type_name,stand_loc_name,stand_img='';
      
      this.masterdata['pavillion_types'].forEach(function (item) {              
        if(item['short_code']==pav_type_code)
        {
          pav_name=item['name'];
        }
      });
      this.stallDetails['stallinfo']['pavillion_name']=pav_name;
      this.masterdata['stand_locations'].forEach(function (item) {              
        if(item['short_code']==stand_loc_code)
        {
          stand_type_name=item['name'];
        }
      });
      this.stallDetails['stallinfo']['stand_location_name']=stand_type_name;
      this.masterdata['stand_types'].forEach(function (item) {              
        if(item['short_code']==stand_type_code)
        {
          stand_loc_name=item['name'];
          stand_img=item[stand_loc_code];
        }
      });
      this.stallDetails['stallinfo']['stand_type_name']=stand_loc_name;
      this.stallDetails['stallinfo']['stand_image']=stand_img;
      //console.log(this.stallDetails['stallinfo']);
    }
    else
    {
      this.isSKUValid=false;     
    }
    if(!this.isSKUValid)
    {
      this.stallDetails['stallinfo']['area']=0;
      this.stallDetails['stallinfo']['pavillion_name']='';
      this.stallDetails['stallinfo']['pavillion_type']='';
      this.stallDetails['stallinfo']['stand_location']='';
      this.stallDetails['stallinfo']['stand_location_name']='';
      this.stallDetails['stallinfo']['stand_type_name']='';
      this.stallDetails['stallinfo']['stand_type']='';
    }
  }
  validateSKU()
  {
    let isValid=false;
    if(this.masterdata['details'] && this.masterdata['details']['sku_exh_stall_prefix'])
    {
      let sku_prefix=this.masterdata['details']['sku_exh_stall_prefix'];
      let sku=this.stallDetails['stallinfo']['sku'];
      //console.log(sku_prefix.length);
      if(sku.length >= (sku_prefix.length+4))
      {
       // console.log(sku.substring(0,3));
        if(sku.substring(0,3)==sku_prefix)
        {
          var pav_type_code=sku.charAt(3);
          var stand_type_code=sku.charAt(4);
          var stand_location_code=sku.charAt(5);
          var area=sku.substring(6);
          
          if(!isNaN(area) || area.includes('+'))
          {
            let pav_matched=false;
            let stand_type_matched=false;
            let stand_location_matched=false;
           // console.log(this.masterdata['pavillion_types']);
           
            this.masterdata['pavillion_types'].forEach(function (item) {              
              if(item['short_code']==pav_type_code)
              {
                pav_matched=true;
              }
            });
           // console.log(this.masterdata['stand_types']);            
            this.masterdata['stand_types'].forEach(function (item) {
              if(item['short_code']==stand_type_code)
              {
                stand_type_matched=true;
              }
            });
           // console.log(this.masterdata['stand_locations']);           
            this.masterdata['stand_locations'].forEach(function (item) {
              if(item['short_code']==stand_location_code)
              {
                stand_location_matched=true;
              }
            });            
            if(pav_matched && stand_type_matched && stand_location_matched)
            {
              isValid=true;
            }            
          }
        }       
      }
    }
    return isValid ;
  }
  changeQuota(index){
    if(parseInt(this.stallDetails['stallinfo']['quota_details'][index]['standard']) && parseInt(this.stallDetails['stallinfo']['quota_details'][index]['extra']))
		{
      this.stallDetails['stallinfo']['quota_details'][index]['standard']=parseInt(this.stallDetails['stallinfo']['quota_details'][index]['standard']);
      this.stallDetails['stallinfo']['quota_details'][index]['extra']=parseInt(this.stallDetails['stallinfo']['quota_details'][index]['extra']);     
      this.stallDetails['stallinfo']['quota_details'][index]['total']=parseInt(this.stallDetails['stallinfo']['quota_details'][index]['standard']) + parseInt(this.stallDetails['stallinfo']['quota_details'][index]['extra']);			
		}
		else
		{
      //quota_info.standard=0;
      this.stallDetails['stallinfo']['quota_details'][index]['total']=0;
      if(parseInt(this.stallDetails['stallinfo']['quota_details'][index]['standard']))
      {
        this.stallDetails['stallinfo']['quota_details'][index]['standard']=parseInt(this.stallDetails['stallinfo']['quota_details'][index]['standard']);     
        this.stallDetails['stallinfo']['quota_details'][index]['total']= parseInt(this.stallDetails['stallinfo']['quota_details'][index]['standard']);
      }
      if(parseInt(this.stallDetails['stallinfo']['quota_details'][index]['extra']))
      {
        this.stallDetails['stallinfo']['quota_details'][index]['extra']=parseInt(this.stallDetails['stallinfo']['quota_details'][index]['extra']);     
        this.stallDetails['stallinfo']['quota_details'][index]['total']= parseInt(this.stallDetails['stallinfo']['quota_details'][index]['extra']);
      }			
		}	
    this.stallDetails['stallinfo']['quota_details'][index]['balance']=this.stallDetails['stallinfo']['quota_details'][index]['total']-this.stallDetails['stallinfo']['quota_details'][index]['used'];
  }
  changeFacilities(std_name,extra_name){     
    if(parseInt(this.stallDetails['stallinfo']['furniture_details'][0][std_name])>0)
    {
      this.stallDetails['stallinfo']['furniture_details'][0][std_name]=parseInt(this.stallDetails['stallinfo']['furniture_details'][0][std_name]);
    }
    if(parseInt(this.stallDetails['stallinfo']['furniture_details'][0][extra_name])>0)
    {
      this.stallDetails['stallinfo']['furniture_details'][0][extra_name]=parseInt(this.stallDetails['stallinfo']['furniture_details'][0][extra_name]);
    }   
  }
  
  showCities(city) {
    this.cities = [];
    this.cities = this.states[city];
    this.stallDetails['stallinfo']['city'] = this.cities[0];
  }
  removestatecity(country)
  {
    if(country!='India')
    {
      this.stallDetails['stallinfo']['state']='';
      this.stallDetails['stallinfo']['city']='';
    }
  }
  changeIsGSTIN()
  {
    if(!this.stallDetails['stallinfo']['is_GSTIN'])
    {
      this.stallDetails['stallinfo']['GSTIN']='';
    }
  }
  getBase64URIencode(x)
  {
      return this.sharedService.getBase64URIencode(x);
  }
  getBase64URIdecode(x)
  {
      return this.sharedService.getBase64URIdecode(x);
  }
  getRupeeFormat(amount)
  {
    return this.sharedService.getRupeeFormat(amount);
  }
  openDialogCategory() {
    let dialogRef = this.dialog.open(CategoryPopupComponent, {
      panelClass: 'my-full-screen-dialog'
    });
  }
  onSubmit(form) {
  //console.log('form submitted');
  //console.log(form);
    if(form.valid && this.validateSKU())
    {
      
      document.getElementById('loader').style.display="block";
      let stallDetailsParams = {
        "app_key": environment.greenCloudConfig.AppKey,
        "source": environment.greenCloudConfig.Source,
        "eventCode": environment.greenCloudConfig.EventCode,
        "sessionId": localStorage.getItem('sessionId'),
        "stallinfo": this.stallDetails['stallinfo']
      }
      //console.log(JSON.stringify(stallDetailsParams));
      this.stallService.changeStallStatus(stallDetailsParams).then(res => {
       // console.log(res);
        //this.openApproveDialog();
        if(res.responseCode==0 && res.success==true)
        {
            this.isSuccess = true;
            //alert('Stall details updated successfully');
            this.openSaveDialog();          
            //console.log(res);         
        }
        else
        {
            this.isSuccess = false;
            
            this.message = res.message;
        }
        document.getElementById('loader').style.display="none";
      }); 
    } 
  }
  openSaveDialog() {
    let dialogRef = this.dialog.open(SavePopupComponent);
    dialogRef.afterClosed().subscribe(value => {
      //console.log(`Dialog sent: ${value}`); 
      this.router.navigateByUrl('/stalls/stall-details/'+this.getBase64URIencode(this.stallId));
    });
  }
}
/*save popup */
@Component({
  selector: 'app-save-popup',
  template: `
<div class="errorMsgPopup">
<mat-dialog-actions class="pad-all-sm">
        <button mat-dialog-close class="closeBtn">
        <i class="material-icons">close</i>
        </button>
    </mat-dialog-actions>
  <mat-dialog-content>
  <mat-list>
  <mat-list-item>
    <mat-icon mat-list-icon><i class="material-icons">check</i></mat-icon>
    <h4 mat-line class="font-bold-six" style="padding-top: 10px;">Stall Details Saved Successfully.</h4>
  </mat-list-item>
</mat-list>
  </mat-dialog-content>
  </div>
`
})
export class SavePopupComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<SavePopupComponent>, public dialog: MatDialog) { }
  ngOnInit() {
  }
}
/* category popup */
@Component({
  selector: 'app-category-popup',
  templateUrl: 'category-popup.html',
})
export class CategoryPopupComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<CategoryPopupComponent>, public dialog: MatDialog) { }
  ngOnInit() {
  }
}

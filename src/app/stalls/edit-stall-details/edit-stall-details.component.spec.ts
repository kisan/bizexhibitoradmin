import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditStallDetailsComponent } from './edit-stall-details.component';

describe('EditStallDetailsComponent', () => {
  let component: EditStallDetailsComponent;
  let fixture: ComponentFixture<EditStallDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditStallDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditStallDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

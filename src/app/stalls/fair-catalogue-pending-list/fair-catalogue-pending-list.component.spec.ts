import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FairCataloguePendingListComponent } from './fair-catalogue-pending-list.component';

describe('FairCataloguePendingListComponent', () => {
  let component: FairCataloguePendingListComponent;
  let fixture: ComponentFixture<FairCataloguePendingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FairCataloguePendingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FairCataloguePendingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

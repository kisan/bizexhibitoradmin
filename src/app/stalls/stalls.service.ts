import { Injectable } from '@angular/core';

//import { Observable } from 'rxjs/Observable';
import {Router} from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class StallsService {

  constructor(private router:Router, private http: HttpClient) { }
  get_stalls(stallListParams: any): Promise<any> {
  
    let headers = new HttpHeaders({'Content-Type':'application/json'}); 
    let body = JSON.stringify(stallListParams);
    
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"report_exhibitor_stall_list.php", body, { headers: headers })
    .toPromise()
  }
  

  getStallData(stallDetailsParams: any): Promise<any> {
  
    let headers = new HttpHeaders({'Content-Type':'application/json'}); 
    let body = JSON.stringify(stallDetailsParams);
    //console.log(body);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"getstalldetails.php", body, { headers: headers })
    .toPromise()
  }
  
  
  resendStallConfirmationFormLink(stallDetailsParams: any): Promise<any> {
  
    let headers = new HttpHeaders({'Content-Type':'application/json'}); 
    let body = JSON.stringify(stallDetailsParams);
   // console.log(body);
   // console.log(environment.greenCloudConfig.ApiBaseUrl+"sendstallconfirmationformlink.php");
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"sendstallconfirmationformlink.php", body, { headers: headers })
    .toPromise()
  }
  
   changeStallStatus(stallDetailsParams: any): Promise<any> {
  
    let headers = new HttpHeaders({'Content-Type':'application/json'}); 
    let body = JSON.stringify(stallDetailsParams);
    //console.log( body); 
   // console.log(environment.greenCloudConfig.ApiBaseUrl+"updatestallinfo.php");
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"updatestallinfo.php", body, { headers: headers })
    .toPromise()
  } 
  
  get_approved_stalls(stallListParams: any): Promise<any> {  

    let headers = new HttpHeaders({'Content-Type':'application/json'}); 
    let body = JSON.stringify(stallListParams);    

    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"report_exhibitor_booked_stall_details.php", body, { headers: headers })
    .toPromise()
  }

  get_fair_cat_list(stallListParams: any): Promise<any> {  

    let headers = new HttpHeaders({'Content-Type':'application/json'}); 
    let body = JSON.stringify(stallListParams);    
    
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"report_exhibitor_fair_cat_details.php", body, { headers: headers })
    .toPromise()
  }

  get_good_to_go_details(stallListParams: any): Promise<any> {  

    let headers = new HttpHeaders({'Content-Type':'application/json'}); 
    let body = JSON.stringify(stallListParams);    
    
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"report_exhibitor_stall_statistics_list.php", body, { headers: headers })
    .toPromise()
  }

  add_vendor_entry(stallListParams: any): Promise<any> {  
    let headers = new HttpHeaders({'Content-Type':'application/json'}); 
    let body = JSON.stringify(stallListParams);
    
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"force_add_vendor_coordinator.php", body, { headers: headers })
    .toPromise()
  }

  send_vendor_form(stallListParams: any): Promise<any> {  
    let headers = new HttpHeaders({'Content-Type':'application/json'}); 
    let body = JSON.stringify(stallListParams);
    
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"sendvendorcoordinatorform.php", body, { headers: headers })
    .toPromise()
  }

}

import { Component, OnInit ,ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { StallsService  } from '../stalls.service';
import { SharedService  } from '../../shared/shared.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-exhibitors',
  templateUrl: './exhibitors.component.html',
  styleUrls: ['./exhibitors.component.scss']
})
export class ExhibitorsComponent implements OnInit {
  @ViewChildren('tooltip') tooltips;
  constructor(private stallsService: StallsService, private router:Router, private sharedService: SharedService) { }
  
  masterdata={'pavillion_types':[],"stand_types":[],"stand_locations":[]}    
  isSuccess: boolean;
  message : string;
  exhibitors=[];
  pagesize=environment.greenCloudConfig.page_size;
  exhibitors_current_page=0;
  total_exhibitors=0;
  exhibitors_filter={"pavillion_type":"","stand_type":"","stand_location":""};
  exhibitors_pages=0;
  exhibitors_sort_col="id";
  exhibitors_sort_dir=1;
  exhibitors_showall=0;
  exhibitors_search='';
  exhibitors_start_item=1;
  exhibitors_end_item=environment.greenCloudConfig.page_size;
  ngOnInit() {
    this.sharedService.getMasterData().then(response => {           
      //console.log(response);
      if(response.success)
      {
          this.masterdata=response.eventDetails;                     
          this.get_stalls();
          this.masterdata.pavillion_types.sort((a, b) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0);
      }
   });
  }

  get_stalls()
  {
    document.getElementById('loader').style.display="block"; 
    var sort_dir="ASC";
    if(this.exhibitors_sort_dir==1)
    {
        sort_dir="DESC";
    }
    let stallListParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode,
      "pagesize":this.pagesize,
      "currentpage":this.exhibitors_current_page,
      "search":this.exhibitors_search,
      "sort_col":this.exhibitors_sort_col,
      "sort_dir":sort_dir,
      "filter":this.exhibitors_filter,
      "showall":this.exhibitors_showall
    }
    this.stallsService.get_approved_stalls(stallListParams).then(res => { 
      //console.log(res);
      if(res.responseCode==0 && res.success==true)
      {
          this.isSuccess = true;
          // console.log(res.overview.records);
          this.exhibitors=res.overview.records;
          this.total_exhibitors=res.overview.total_stalls.cnt;
          this.update_stall_stastics();
      }
      else
      {
          this.isSuccess = false;
          // console.log(res);
          this.message = res.message;
      }
      document.getElementById('loader').style.display="none"; 
    });
  }

  update_stall_stastics() {
    
    if(this.exhibitors_showall)
    {
        this.exhibitors_start_item = 1;
        this.exhibitors_end_item = this.total_exhibitors;
        this.exhibitors_pages=1;
    }
    else
    {
      this.exhibitors_pages=Math.ceil(this.total_exhibitors/this.pagesize); 
      this.exhibitors_start_item = ((this.exhibitors_current_page*this.pagesize)+1);
      this.exhibitors_end_item = ((this.exhibitors_current_page*this.pagesize)+this.pagesize);	
      if( this.exhibitors_end_item >this.total_exhibitors){
          this.exhibitors_end_item =this.total_exhibitors;
      }
    }
  }

  sort_stalls(key) {
    if(this.exhibitors_sort_col != key)
    {
        this.exhibitors_sort_col=key;
        this.exhibitors_sort_dir=0;
        this.exhibitors_current_page = 0;
    }
    else
    {
        this.exhibitors_sort_dir= 1-this.exhibitors_sort_dir;
    }
    this.get_stalls();
  }

  toggle_show_all_stalls(){
    //this.stall_showall=1-this.stall_showall;
    this.exhibitors_current_page=0;
		this.get_stalls();
  }
  search_stalls(event: any) { // without type info
    if(event.keyCode == 13)
		{
       // console.log(event.target.value);
        //this.stall_search=event.target.value;
        this.exhibitors_current_page=0;
        this.get_stalls();
    }
  }
  get_pavillion_name(short_code)
  {
    for (let pav of this.masterdata.pavillion_types) {
        if(pav.short_code==short_code)
        {
          return pav.name;
        }
    }
    return '';
  }
  get_stand_type_name(short_code)
  {
    for (let stand_type of this.masterdata.stand_types) {
        if(stand_type.short_code==short_code)
        {
          return stand_type.name;
        }
    }
    return '';
  }
  get_stand_location_name(short_code)
  {
    for (let stand_loc of this.masterdata.stand_locations) {
        if(stand_loc.short_code==short_code)
        {
          return stand_loc.name;
        }
    }
    return '';
  }
  getBase64URIencode(x)
  {
    return this.sharedService.getBase64URIencode(x);
  }
  getBase64URIdecode(x)
  {
    return this.sharedService.getBase64URIdecode(x);
  }
  getFullLogoUrl(logourl)
  {
    if ((logourl) && (logourl != "")) 
    {
      return logourl;
    }
    else 
    {
      return '../../assets/images/default_organization.png';
    }
  }
  checkAllowedRoles(roles)
  {
    //console.log(roles);
    return this.sharedService.checkAllowedRoles(roles);
  }
}

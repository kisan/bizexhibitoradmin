import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule ,ReactiveFormsModule } from '@angular/forms';
import { CustomMaterialModule } from '../custom-material/custom-material.module';
import { StallsRoutingModule } from './stalls-routing.module';
import { StallListComponent } from './stall-list/stall-list.component';
import { StallDetailsComponent, ResendPopupComponent, ApprovePopupComponent, ConfirmPopupComponent,ApprovalConfPopupComponent,AddVendorEntryConfirmPopupComponent , VendorFormSentComponent } from './stall-details/stall-details.component';
import { SharedService } from '../shared/shared.service';
import { StallsService } from './stalls.service';
import { EditStallDetailsComponent, CategoryPopupComponent , SavePopupComponent } from './edit-stall-details/edit-stall-details.component';
import { ExhibitorsComponent } from './exhibitors/exhibitors.component';
import { FairCatalogueSubmitedListComponent } from './fair-catalogue-submited-list/fair-catalogue-submited-list.component';
import { FairCataloguePendingListComponent } from './fair-catalogue-pending-list/fair-catalogue-pending-list.component';
import { Good2GoComponent } from './good2go/good2go.component';
import { QuotaComponent } from './quota/quota.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { EmailPopupComponent } from './email-popup/email-popup.component'; 
import { NgxEditorModule } from 'ngx-editor';
import { TooltipModule } from 'ngx-bootstrap';
import { EditQuotaComponent } from './edit-quota/edit-quota.component';
import {MatStepperModule} from '@angular/material/stepper';
 
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    StallsRoutingModule,
    CustomMaterialModule,
    NgxEditorModule,
    MatStepperModule,
    TooltipModule.forRoot(),
  ],
  declarations: [StallListComponent, StallDetailsComponent, ResendPopupComponent, ApprovePopupComponent, ConfirmPopupComponent, EditStallDetailsComponent, CategoryPopupComponent, ApprovalConfPopupComponent , SavePopupComponent, ExhibitorsComponent,ExhibitorsComponent, FairCatalogueSubmitedListComponent, FairCataloguePendingListComponent, Good2GoComponent , AddVendorEntryConfirmPopupComponent, QuotaComponent, InvoicesComponent, EmailPopupComponent, EditQuotaComponent , VendorFormSentComponent],
  entryComponents:[ResendPopupComponent, ApprovePopupComponent, ConfirmPopupComponent, CategoryPopupComponent, ApprovalConfPopupComponent , SavePopupComponent , AddVendorEntryConfirmPopupComponent, EmailPopupComponent , VendorFormSentComponent], 
  providers: [StallsService, SharedService],
})
export class StallsModule { }

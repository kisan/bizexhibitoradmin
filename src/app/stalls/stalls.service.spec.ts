import { TestBed, inject } from '@angular/core/testing';

import { StallsService } from './stalls.service';

describe('StallsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StallsService]
    });
  });

  it('should be created', inject([StallsService], (service: StallsService) => {
    expect(service).toBeTruthy();
  }));
});

import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Inject } from '@angular/core';
import { Router } from '@angular/router';
import { any } from 'underscore';
import { ProposalService  } from '../proposal.service';
import { SharedService  } from '../../shared/shared.service';
import { environment } from '../../../environments/environment';
// import { FormControl, FormBuilder, FormGroup, Validators, AbstractControl, NG_VALIDATORS, ValidatorFn, Validator } from '@angular/forms';


@Component({
  selector: 'app-update-stall-status',
  templateUrl: './update-stall-status.component.html',
  styleUrls: ['./update-stall-status.component.scss']
})
export class UpdateStallStatusComponent implements OnInit {

  stand_info:any;
  newstatus:'';
  status_list: string[] = [];
  comments: '';
  stand_status_history:any;
  total_stand_status_history=0;
  pagesize=environment.greenCloudConfig.page_size;
  constructor(private proposalService: ProposalService, private router:Router, public dialog: MatDialog,
    private sharedService: SharedService , @Inject(MAT_DIALOG_DATA) data:any, private dialogRef: MatDialogRef<UpdateStallStatusComponent>) { 
    this.stand_info = data;
    this.newstatus = this.stand_info.status;
    
   }

  ngOnInit() {
    //console.log(this.stand_info);
    let is_account = this.checkAllowedRoles(['exhibitoraccounts']);
    let is_superadmin = this.checkAllowedRoles(['superadmin' ]);
    let is_exhadmin = this.checkAllowedRoles(['exhibitoradmin']);
    let is_marketing = this.checkAllowedRoles(['exhibitormarketing']);
    
    if(is_superadmin == true)
    {
      if(this.stand_info.status=='available')
      {
        this.status_list = [ 'reserved',  'blocked'];
      }
      else
      {
        if(this.stand_info.status=='proposed')
        {
          this.status_list = ['available' ];
        }
        else
        {
          if(this.stand_info.status=='reserved')
          {
            this.status_list = ['available', 'partial','booked' ];
          }
          else
          {
            if(this.stand_info.status=='partial')
            {
              this.status_list = ['available', 'booked' ];
            }
            else
            {
              if(this.stand_info.status=='booked' || this.stand_info.status=='blocked')
              {
                this.status_list = ['available' ];
              }
              else
              {
                this.status_list=[];
              }
            }
          }
        }
      }
      
    }
    else
    {
      if(is_exhadmin == true)
      {
        //this.status_list = ['available', 'reserved', 'blocked'];
        if(this.stand_info.status=='available')
        {
          this.status_list = [ 'reserved',  'blocked'];
        }
        else
        {
          if(this.stand_info.status=='proposed')
          {
            this.status_list = ['available' ];
          }
          else
          {
            if(this.stand_info.status=='reserved' || this.stand_info.status=='partial' || this.stand_info.status=='booked' || this.stand_info.status=='blocked')
            {
              this.status_list = ['available' ];
            }
            else
            {
              this.status_list=[];
            }
          }
        }
      }
      else
      {
        if(is_account==true)
        {
          if(this.stand_info.status=='reserved')
          {
            this.status_list=  ['partial', 'booked'];
          }
          else
          {
            if(this.stand_info.status=='partial')
            {
              this.status_list=  [ 'booked'];
            }
            else
            {
              this.status_list=[];
            }
          }
        }
        else
        {
          if(is_marketing==true)
          {
            if(this.stand_info.status=='available' )
            {
              this.status_list=  ['reserved'];
            }
            else
            {
              if(this.stand_info.status=='proposed')
              {
                this.status_list=  ['available'];
              }
              else
              {
                if(this.stand_info.status=='reserved')
                {
                  this.status_list=  ['available'];
                }
                else
                {
                  this.status_list=[];
                }
              }
            }
          }
          else
          {
            this.status_list=[];
          }
        }
      }
    }
   
    this.getStandStatusHistory();
  }

  getStandStatusHistory()
  {
    document.getElementById('loader1').style.display="block";  
    var sort_dir="DESC";
    let stallListParams = {
        "app_key": environment.greenCloudConfig.AppKey,
        "source": environment.greenCloudConfig.Source,
        "eventCode": environment.greenCloudConfig.EventCode,
        "pagesize":this.pagesize,
        "currentpage":0,
        "search":"",
        "sort_col":"action_datetime",
        "sort_dir":sort_dir,
        "filter":{},
        "showall":1,
        "stallId":this.stand_info.id
    }
   //console.log(JSON.stringify(stallListParams));
    this.proposalService.get_stall_status_history(stallListParams).then(res => { 
        //console.log(res);
        if(res.responseCode==0 && res.success==true)
        {
            this.stand_status_history=res.stallstatushistory.records;
            this.total_stand_status_history = res.stallstatushistory.total;
            //this.total_stalls=res.stallslist.total;
        }
        else
        {
            //this.isSuccess = false;
            // console.log(res);
            this.stand_status_history=[];
        }
        document.getElementById('loader1').style.display="none"; 
    });
  }

  onSubmit(form) {
    //console.log('form submitted');
    //console.log(form);
      if(form.valid )
      {
        //console.log('valid');
        document.getElementById('loader1').style.display="block";
        let stallDetailsParams = {
          "app_key": environment.greenCloudConfig.AppKey,
          "source": environment.greenCloudConfig.Source,
          "eventCode": environment.greenCloudConfig.EventCode,
          "sessionId": localStorage.getItem('sessionId'),
          "stallId": this.stand_info['id'],
          "stallStatus": this.newstatus,
          "notes": this.comments
        };
        this.proposalService.update_stall_status(stallDetailsParams).then(res => { 
          //console.log(res);
          if(res.responseCode==0 && res.success==true)
          {
              this.dialogRef.close();
          }
        });
      }
      else{
        //console.log('invalid');
      }
  }

  checkAllowedRoles(roles) 
  {
    //console.log(roles);
    return this.sharedService.checkAllowedRoles(roles);
  }

  redirectEdit()
  {
    this.dialogRef.close();
    this.router.navigateByUrl('/proposal/create-stand?id='+this.stand_info.id);
  }

  openDeleteConfDialog() {
    let dialogRef = this.dialog.open(DeleteConfPopupComponent, {
      panelClass: 'email_conf',
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(value => {
      //console.log(value);
      if(value==true)
      {
        //this.sendoffer();
        document.getElementById('loader1').style.display="block";
        let stallDetailsParams = {
          "app_key": environment.greenCloudConfig.AppKey,
          "source": environment.greenCloudConfig.Source,
          "eventCode": environment.greenCloudConfig.EventCode,
          "sessionId": localStorage.getItem('sessionId'),
          "stallId": this.stand_info['id'],
          "stallStatus": "deleted",
          "notes": ""
        };
        this.proposalService.update_stall_status(stallDetailsParams).then(res => { 
          //console.log(res);
          if(res.responseCode==0 && res.success==true)
          {
              this.dialogRef.close();
          }
        });
      }
    });
}

}

/*email conf popup */
@Component({
  providers: [UpdateStallStatusComponent],
  selector: 'app-delete-confirm-popup',
  template: `
  <div class="confirmMsgPopup" style>
  
    <h4 class="mrgn-b-md mrgn-l-md mrgn-r-md">Confirmation</h4>
    <p class="mrgn-b-none mrgn-l-md mrgn-r-md">Are you sure you want to delete this stand?</p>
  
  <div class="footer-btn">
  <mat-card-footer>
    <button type="button" style="background:#B8B8B8;" mat-raised-button mat-dialog-close class="grayBtn" (click)="dialogRef.close(false)">Cancel</button>
    <button type="button" mat-raised-button class="greennewBtn" (click)="dialogRef.close(true)">Yes, Delete</button>
    </mat-card-footer>
  </div>
  </div>
  `
})
export class DeleteConfPopupComponent implements OnInit {
  //offer_type="";
  constructor(public dialogRef: MatDialogRef<DeleteConfPopupComponent>, public dialog: MatDialog ,  @Inject(MAT_DIALOG_DATA) public data: any,) { }
  ngOnInit() {
  
  }
}

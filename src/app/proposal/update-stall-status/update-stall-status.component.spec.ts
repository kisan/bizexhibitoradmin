import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateStallStatusComponent } from './update-stall-status.component';

describe('UpdateStallStatusComponent', () => {
  let component: UpdateStallStatusComponent;
  let fixture: ComponentFixture<UpdateStallStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateStallStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateStallStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

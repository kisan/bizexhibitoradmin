import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StallStatusComponent } from './stall-status.component';

describe('StallStatusComponent', () => {
  let component: StallStatusComponent;
  let fixture: ComponentFixture<StallStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StallStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StallStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

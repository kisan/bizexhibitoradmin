import { Component, OnInit ,ViewChildren, Inject} from '@angular/core';
import { Router , ActivatedRoute} from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProposalService  } from '../proposal.service';
import { SharedService  } from '../../shared/shared.service';
import { UpdateStallStatusComponent } from '../update-stall-status/update-stall-status.component';
import { environment } from '../../../environments/environment';
import { NativeDateAdapter, DateAdapter, MAT_DATE_FORMATS } from "@angular/material/core";
import { AppDateAdapter, APP_DATE_FORMATS} from '../../shared/date.adapter';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
@Component({
  selector: 'app-stall-status',
  templateUrl: './stall-status.component.html',
  styleUrls: ['./stall-status.component.scss'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
  ]
})
export class StallStatusComponent implements OnInit {
  @ViewChildren('tooltip') tooltips;
  constructor(private proposalService: ProposalService, private router:Router, 
    private sharedService: SharedService ,public dialog: MatDialog , private route: ActivatedRoute,) { }

  masterdata={'pavillion_types':[],"stand_types":[],"stand_locations":[]}
  isSuccess: boolean;
  message : string;
  stalls=[];
  pagesize=environment.greenCloudConfig.page_size;
  stall_current_page=0;
  total_stalls=0;
  //stall_form_status='';
  stall_filter={"pavillion_type":"","status":"","stand_type":"","stand_location":"",
    "area":"","proposed_by_userid":"","discount_type":""};
  stall_pages=0;
  stall_sort_col="stand_number";
  stall_sort_dir=0;
  stall_showall=0;
  stall_search='';
  stall_start_item=1;
  stall_end_item=environment.greenCloudConfig.page_size;
  area_list:[];
  proposed_by_users:[];
  selected_proposed_date_range="";
  selected_min_proposed_date = "";
  selected_max_proposed_date = "";
  min_proposed_date = "";
  max_proposed_date = "";
  minDate=new Date();
  maxDate=new Date();
  

  ngOnInit() {
    localStorage.setItem('offer_type', '');
    this.minDate.setDate(1);
    this.minDate.setMonth(1);
    this.sharedService.getMasterData().then(response => {           
      //console.log(response);
      if(response.success)
      {
        this.masterdata=response.eventDetails;          
        
        this.masterdata.pavillion_types.sort((a, b) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0);
        this.route.queryParams.subscribe(params => {
          if(params['sort_by'] && params['sort_by'] !='')
          {
            this.stall_sort_col=params['sort_by'];
            this.stall_sort_dir=1;
          }
          this.get_stalls();
        });
      }
   });
  }

  get_stalls(){
    document.getElementById('loader').style.display="block";  
    var sort_dir="ASC";
    if(this.stall_sort_dir==1)
    {
        sort_dir="DESC";
    }
   
    let stallListParams = {
        "app_key": environment.greenCloudConfig.AppKey,
        "source": environment.greenCloudConfig.Source,
        "eventCode": environment.greenCloudConfig.EventCode,
        "pagesize":this.pagesize,
        "currentpage":this.stall_current_page,
        "search":this.stall_search,
        "sort_col":this.stall_sort_col,
        "sort_dir":sort_dir,
        "filter":this.stall_filter,
        "showall":this.stall_showall,
        "min_proposed_date":this.min_proposed_date,
        "max_proposed_date":this.max_proposed_date
    }
   //console.log(JSON.stringify(stallListParams));
    this.proposalService.get_stalls(stallListParams).then(res => { 
        //console.log(res);
        if(res.responseCode==0 && res.success==true)
        {
            this.isSuccess = true;
            //console.log(res);
            this.stalls=res.stallslist.records;
            this.total_stalls=res.stallslist.total;
            this.area_list=res.stallslist.area_list;
            this.proposed_by_users=res.stallslist.proposed_by_users;
            this.update_stall_stastics();
        }
        else
        {
            this.isSuccess = false;
            // console.log(res);
            this.message = res.message;
        }
        document.getElementById('loader').style.display="none"; 
    });
}

update_stall_stastics() {

if(this.stall_showall)
{
    this.stall_start_item = 1;
    this.stall_end_item = this.total_stalls;
    this.stall_pages=1;
}
else
{
    this.stall_pages=Math.ceil(this.total_stalls/this.pagesize);
    this.stall_start_item = ((this.stall_current_page*this.pagesize)+1);
    this.stall_end_item = ((this.stall_current_page*this.pagesize)+this.pagesize);	
    if( this.stall_end_item >this.total_stalls){
       this.stall_end_item =this.total_stalls;
    }
}
}

sort_stalls(key) {
if(this.stall_sort_col != key)
{
    this.stall_sort_col=key;
    this.stall_sort_dir=0;
    this.stall_current_page = 0;
}
else
{
    this.stall_sort_dir= 1-this.stall_sort_dir;
}
this.get_stalls();
}

toggle_show_all_stalls(){
//this.stall_showall=1-this.stall_showall;
this.stall_current_page=0;
this.get_stalls();
}

search_stalls(event: any) { // without type info
if(event.keyCode == 13)
{
   // console.log(event.target.value);
    //this.stall_search=event.target.value;
    this.stall_current_page=0;
    this.get_stalls();
}
}

get_pavillion_name(short_code)
{
  for (let pav of this.masterdata.pavillion_types) {
      if(pav.short_code==short_code)
      {
        return pav.name;
      }
  }
  return '';
}

get_stand_type_name(short_code)
{
  for (let stand_type of this.masterdata.stand_types) {
      if(stand_type.short_code==short_code)
      {
        return stand_type.name;
      }
  }
  return '';
}

get_stand_location_name(short_code)
{
  for (let stand_loc of this.masterdata.stand_locations) {
      if(stand_loc.short_code==short_code)
      {
        return stand_loc.name;
      }
  }
  return '';
}

  openupdateStallstatusDialog(stand_info) {
    //localStorage.setItem('selectedSM', JSON.stringify(SM));
    let dialogRef = this.dialog.open(UpdateStallStatusComponent,{
      panelClass: 'update-stall-status-dialog',
      data:stand_info
    });
    dialogRef.afterClosed().subscribe(value => {
      //console.log(`Dialog sent: ${value}`); 
      this.get_stalls();
    });
  }

  checkAllowedRoles(roles) 
  {
    //console.log(roles);
    return this.sharedService.checkAllowedRoles(roles);
  }

  redirectAddStall()
  {
    this.router.navigateByUrl('/proposal/create-stand');
  }

  filter_proposed_date_range(){
    //this.stall_showall=1-this.stall_showall;
    let start_date = new Date();
    let end_date = new Date();
    if (this.selected_proposed_date_range == '2') {
      start_date.setDate(start_date.getDate() - 1);
      end_date.setDate(end_date.getDate() - 1);
    }
    if (this.selected_proposed_date_range == '3') {
      start_date.setDate(start_date.getDate() - 7);
    }
    if (this.selected_proposed_date_range == '4') {
      start_date.setDate(start_date.getDate() - 14);
    }
    if (this.selected_proposed_date_range == '5') {
      start_date.setDate(1);
    }
    if (this.selected_proposed_date_range =='')
    {
      this.min_proposed_date="";
      this.max_proposed_date="";
    }
    else
    {
      this.min_proposed_date=start_date.toISOString().split('T')[0];
      this.max_proposed_date=end_date.toISOString().split('T')[0];
    }
    if (this.selected_proposed_date_range !='6')
    {
      this.stall_current_page=0;
      this.get_stalls();
    }
  }

  filter_proposed_custom_date_range(){

    if(this.selected_min_proposed_date && this.selected_min_proposed_date!='')
    {
      this.min_proposed_date=this.sharedService.toMYSQLDate(this.selected_min_proposed_date);
    }
    else{
      this.min_proposed_date='';
    }
    if(this.selected_max_proposed_date && this.selected_max_proposed_date!='')
    {
      this.max_proposed_date=this.sharedService.toMYSQLDate(this.selected_max_proposed_date);
    }
    else{
      this.max_proposed_date='';
    }
    if(this.selected_min_proposed_date && this.selected_min_proposed_date!='' && this.selected_max_proposed_date && this.selected_max_proposed_date!='')
    {
      this.get_stalls();
    }

  }
  reset()
  {
    this.stall_current_page=0;
    this.stall_filter={"pavillion_type":"","status":"","stand_type":"",
      "stand_location":"","area":"","proposed_by_userid":"","discount_type":""};
    this.stall_sort_col="stand_number";
    this.stall_sort_dir=0;
    this.stall_showall=0;
    this.stall_search='';
    this.selected_proposed_date_range="";
    this.selected_max_proposed_date="";
    this.selected_min_proposed_date="";
    this.max_proposed_date="";
    this.min_proposed_date="";
    this.get_stalls();
  }

  getRupeeFormat(amount)
  {
    return this.sharedService.getRupeeFormat(this.roundNumberV1(amount,0));
  }
  roundNumberV1(num, scale) {
    if(!("" + num).includes("e")) {
      return +(Math.round(parseFloat(num + "e+" + scale))  + "e-" + scale);  
    } else {
      var arr = ("" + num).split("e");
      var sig = ""
      if(+arr[1] + scale > 0) {
        sig = "+";
      }
      var i = +arr[0] + "e" + sig + (+arr[1] + scale);
      var j = Math.round(parseFloat(i));
      var k = +(j + "e-" + scale);
      return k;  
    }
  }

  exportStandsCSV(){
    document.getElementById('loader').style.display="block";
    let stallListParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode,
      "pagesize":this.pagesize,
      "currentpage":0,
      "search":'',
      "sort_col":'stand_number',
      "sort_dir":'ASC',
      "filter":{},
      "showall":1
    }
 //console.log(JSON.stringify(stallListParams));
    this.proposalService.get_stalls(stallListParams).then(response => { 
      if(response.responseCode == 0 && response.success == true){
        var records = response.stallslist.records;
        records.forEach(function(row) {
          // code
          delete row['base_price'];delete row['blocked_by_userid'];delete row['booked_by_userid'];
          delete row['compare_at_price'];delete row['created_by_userid'];
          delete row['created_datetime'];delete row['deleted_by_userid'];
          delete row['deleted_datetime'];delete row['event_code'];delete row['id'];
          delete row['image_url'];delete row['partial_by_userid'];delete row['partial_datetime'];
          delete row['pavilion_type_name'];delete row['proposed_by_userid'];
          delete row['proposed_to_exhibitor_id'];delete row['reserved_by_userid'];
          delete row['stand_location_name'];delete row['stand_type_name'];
          delete row['status_code'];
          for (var key in row) {
            //console.log(row[key]);

            if(row[key]==null || row[key]=='null')
            {
              //console.log(key);
              //console.log(row[key]);
              row[key]='';
            }
          }
        });
        var csvdata = this.sharedService.JSONToCSVConvertor(records, "Stands", true);
          if(csvdata){
            var a         = document.createElement('a');
            a.href        = 'data:attachment/csv,' + escape(csvdata);
            a.target      = '_blank';
            a.download    = environment.greenCloudConfig.EventCode+'_Stands.csv';
            document.body.appendChild(a);
            a.click();
          }
      }  
      document.getElementById('loader').style.display="none"; 
    });
  }

  openstallstatusDialog() {
    let dialogRef = this.dialog.open(StatusConfPopupComponent, {
      panelClass: 'email_conf',
      disableClose: true,
    });
}

}

/*email conf popup */
@Component({
  providers: [StallStatusComponent],
  selector: 'app-confirm-popup',
  template: `
  <div class="stallStatusMsgPopup">
  <span class="material-icons close">close</span>
    <mat-icon mat-list-icon><i class="material-icons">check</i></mat-icon>
    <h4 class="mrgn-b-md mrgn-l-md mrgn-r-md">Great!</h4>
    <p class="mrgn-b-none mrgn-l-md mrgn-r-md">Stand - 103 has been successfully <br> reserved for KISAN Forum pvt ltd. </p>
    <p class="mrgn-b-none mrgn-l-md mrgn-r-md">Stand - 103 has been successfully proposed to <br> KISAN Forum pvt ltd. </p>
    <p class="mrgn-b-none mrgn-l-md mrgn-r-md">Stand - 103 has been successfully blocked. </p>
    </div>
  `
})
export class StatusConfPopupComponent implements OnInit {
  //offer_type="";
  constructor(public dialogRef: MatDialogRef<StatusConfPopupComponent>, public dialog: MatDialog ,  @Inject(MAT_DIALOG_DATA) public data: any,) { }
  ngOnInit() {
  
  }
}

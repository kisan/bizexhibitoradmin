import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateProposalComponent } from './generate-proposal.component';

describe('GenerateProposalComponent', () => {
  let component: GenerateProposalComponent;
  let fixture: ComponentFixture<GenerateProposalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateProposalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateProposalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Kisan18StallComponent } from '../kisan18-stall/kisan18-stall.component';
import { CompanyInfoComponent } from '../company-info/company-info.component';
import { Router, ActivatedRoute} from '@angular/router';
import { ProposalService  } from '../proposal.service';
import { SharedService  } from '../../shared/shared.service';
import { environment } from '../../../environments/environment';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NativeDateAdapter, DateAdapter, MAT_DATE_FORMATS } from "@angular/material/core";
import { AppDateAdapter, APP_DATE_FORMATS} from '../../shared/date.adapter';
import { EventEmitter } from 'events';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { distinctUntilChanged } from "rxjs/operators";
import { MatExpansionPanel } from '@angular/material/expansion';

@Component({
  selector: 'app-generate-proposal',
  templateUrl: './generate-proposal.component.html',
  styleUrls: ['./generate-proposal.component.scss'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
  ]
})
export class GenerateProposalComponent implements OnInit {
  username_for_offer:string='';
  exhinfo:any;
  history_info:any;
  offer_type="";
  products=[];
  extra_products = [];
  sponsorship_options=[];
  stalls_for_amenities=[];
  masterdata={'pavillion_types':[],"stand_types":[],"stand_locations":[]};
 
  minDate= new Date();
  payment_last_date:any;
  offer_generated=false;
  offer_id=0;
  selected_product_id=0;
  action="edit";
  isExtraReq = true;
 // extraReq: any;
  price = null;
  quantity = null;
  selected_stall_SKU: any;
  extraMasterData : any;
  standAmenityMasterData : any;
  sponsorshipMasterData : any;
  prod_index=0;
  filter:any;
  bankDetails: FormGroup;
  productServicesFormCtrl = new FormControl('', Validators.minLength(10));
  tdsPercentage: number | boolean;
  selectedSku = '';
  is_rebook=false;
  @ViewChild('accordian', {static: false}) accordian: MatExpansionPanel;
  @ViewChild('accordian1', {static: false}) accordian1: MatExpansionPanel;
  accordianexpand=false;
  accordian1expand=false;
  //subscription: Subscription;
  netamount='';
  totalamount='';

  constructor(
    public dialog: MatDialog,
    private proposalService: ProposalService,
    private router:Router,
    private sharedService: SharedService ,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private fb: FormBuilder
    ) { }

  ngOnInit() {
    //this.subscription = new Subscription();
    //console.log('this.productServicesFormCtrl', this.productServicesFormCtrl);

    this.offer_type=localStorage.getItem('offer_type');
    if(this.offer_type==undefined || this.offer_type==null)
    {
      this.offer_type='proforma invoice';
      localStorage.setItem('offer_type', 'proforma invoice');
    }

    //document.getElementById('loader').style.display="block";

    this.sharedService.getMasterData().then(response => {
     // console.log('getMasterData', response);
      if(response.success)
      {
        this.masterdata=response.eventDetails;
        // console.log(this.masterdata);
        this.route.queryParams.subscribe(params => {
          this.offer_id = params['offer_id'];

          if(this.offer_id==undefined || this.offer_id==null)
          {
            this.offer_id =0;
            this.username_for_offer= localStorage.getItem('username_for_offer');
            if(this.username_for_offer && this.username_for_offer!='')
            {
              this.getExhInfoByUsername();
              this.addproduct();
              //this.getExhibitorAddons();
            }
            else
            {
              this.router.navigate(['proposal/search-exhibitor']);
            }
            //console.log('---1---');

            // this.extraProd();
          }
          else
          {
            this.selected_product_id = params['product_id'];
            this.action=params['action'];
            if(this.action==undefined || this.offer_id==null)
            {
              this.action="edit";
            }
            if(this.selected_product_id==undefined || this.selected_product_id==null)
            {
              this.selected_product_id =0;
            }
            //console.log('---2---');
            this.getDetailsByOfferId();
          }
        });
      }
      else
      {
        this.snackBar.open(response.message, '', { duration: 5000, });
        //document.getElementById('loader').style.display="none";
      }
    },
    err=>{
      //console.log(err.error);
      this.snackBar.open(err.error.message, '', { duration: 5000, });
     // document.getElementById('loader').style.display="none";
    });
  }

  getDetailsByOfferId()
  {
    let params = {
      "app_key": environment.greenCloudConfig.AppKey, "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode, "sessionId": localStorage.getItem('sessionId'),
      "offerId":this.offer_id
    }
    //console.log(JSON.stringify(params));
    this.proposalService.get_offer_details(params).then(res => {
      //console.log(res);
      if(res.responseCode==0 && res.success==true)
      {
        //this.showAdditionalDetails(res['data']['details']);
        if(res.data.details.payment_date_expired==0)
        {
          let dateParts = res.data.details.payment_last_datetime.split("-");
          this.payment_last_date = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
        }
        if(res.data.details.is_rebook==1)
        {
          this.is_rebook=true;
        }
        //this.payment_last_date=res.data.details.payment_last_datetime;
        this.offer_type=res.data.details.offer_type;
        if(this.offer_type=='proforma')
        {
          this.offer_type='proforma invoice';
        }
        if(this.offer_type=='proposal' && this.action=="create_proforma")
        {
          this.offer_type='proforma invoice';
        }
        localStorage.setItem('offer_type', this.offer_type);

        for (let product of res.data.products) {
          if(this.selected_product_id >0)
          {
            if(product.id==this.selected_product_id)
            {
              this.addproduct(product);
            }
          }
          else
          {
            this.addproduct(product);
          }
        }
        for (let product of res.data.products) {
            //console.log()
            let exists=false;
            for (let item of this.stalls_for_amenities) {
              if(item.stand_id==product.stand_id)
              {
                exists=true;
              }
            }
            if(!exists)
            {
              let stand_for_amenity={"stand_id": product.stall_id,
                "stand_number": product.stand_number,
                "status":'available'};
              this.stalls_for_amenities.push(stand_for_amenity);
            }
          }
        if(res.data.extra_products && res.data.extra_products.length >0) 
        {
          //this.accordian.expanded = true;
          this.accordianexpand=true;
          this.isExtraReq = false;
          this.getExhibitorAddons();
          for (let product of res.data.extra_products) {
            //console.log('Enter');
            this.addExtraProducts(product);
          }
          
          //sponsorship_options
        } 
        else 
        {
          // this.extraProd();
        }

        if(res.data.sponsorship_options && res.data.sponsorship_options.length > 0) 
        {
          this.accordian1expand=true;
          if(!(res.data.extra_products && res.data.extra_products.length >0)) 
          {
            this.getExhibitorAddons();
          }
          
          for (let product of res.data.sponsorship_options) {
            //console.log('Enter');
            this.addSponsorshipOption(product);
          }
        }
        // if(this.products.length<=0)
        // {
        //   this.addproduct();
        // }
        this.username_for_offer= res.data.details.username;
        if(this.username_for_offer && this.username_for_offer!='')
        {
          this.getExhInfoByUsername();
          this.checkprice();
        }
        else
        {
          this.router.navigate(['proposal/search-exhibitor']);
        }
      }
      else
      {
        let snackBarRef = this.snackBar.open(res.message, '', { duration: 5000, });
        snackBarRef.afterDismissed().subscribe(() => {
          this.router.navigateByUrl('/proposal/logs');
          /*if(this.offer_type=='proforma invoice')
          {
            this.router.navigateByUrl('/proposal/proforma-history');
          }
          if(this.offer_type=='proposal')
          {
            this.router.navigateByUrl('/proposal/proposal-history');
          }*/
        });
      }
      //document.getElementById('loader').style.display="none";
    },
    err=>{
      //console.log(err.error);
      let snackBarRef = this.snackBar.open(err.error.message, '', { duration: 5000, });
      //document.getElementById('loader').style.display="none";
      snackBarRef.afterDismissed().subscribe(() => {
        this.router.navigateByUrl('/proposal/logs');
        /*if(this.offer_type=='proforma invoice')
        {
          this.router.navigateByUrl('/proposal/proforma-history');
        }
        if(this.offer_type=='proposal')
        {
          this.router.navigateByUrl('/proposal/proposal-history');
        }*/

      });
    });

  }
  ngAfterViewInit()
  {
    /*if(this.accordianexpand== true)
    {
      this.accordian.expanded=true;
    }
    if(this.accordian1expand== true)
    {
      this.accordian1.expanded=true;
    }*/
 }
  extraProd() {
    if(this.extra_products.length == 0) {
      this.extra_products = [
        {
          "stand_number":"",
          "id": "",
          "price": "",
          "quantity": 1,
          "stand_id":"",
          "is_valid": false
        }
      ];
    }
  }

  sponsorshipOptions() {
    if(this.sponsorship_options.length == 0) {
      this.sponsorship_options = [
        {
          "id": "",
          "price": "",
          "quantity": 1,
          "is_valid": false
        }
      ];
    }
  }

  getExhInfoByUsername()
  {
    let searchParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode,
      "type": "username",
      "username":this.username_for_offer
    }
    // console.log(searchParams);
    this.proposalService.search_exhibitor(searchParams).then(res => {
      //console.log(res);
      if(res.responseCode==0 && res.success==true)
      {
        this.exhinfo=res.exhinfo.details;
        this.history_info=res.exhinfo.booked_stalls_info_history;
        //this.stalls_for_amenities=res.exhinfo.stalls_for_amenities;
        for (let product of res.exhinfo.stalls_for_amenities) {
          //console.log()
          let exists=false;
          for (let item of this.stalls_for_amenities) {
            if(item.stand_id==product.stand_id)
            {
              exists=true;
            }
          }
          if(!exists)
          {
            let stand_for_amenity={"stand_id": product.stand_id,
              "stand_number": product.stand_number,
              "status":product.status};
            this.stalls_for_amenities.push(stand_for_amenity);
          }
        }
        document.getElementById('loader').style.display="none";
      }
      else
      {
        let snackBarRef = this.snackBar.open(res.message, '', { duration: 5000, });
        snackBarRef.afterDismissed().subscribe(() => {
          //console.log('The snack-bar was dismissed');
          //this.gstin="";
          if(this.offer_id >0)
          {
            this.router.navigateByUrl('/proposal/logs');
            /*if(this.offer_type=='proforma invoice')
            {
              this.router.navigateByUrl('/proposal/proforma-history');
            }
            if(this.offer_type=='proposal')
            {
              this.router.navigateByUrl('/proposal/proposal-history');
            }*/
          }
          else
          {
            this.router.navigate(['proposal/search-exhibitor']);
          }
        });
      }
    },
    err=>{
      //console.log(err.error);
      let snackBarRef = this.snackBar.open(err.error.message, '', { duration: 5000, });
      snackBarRef.afterDismissed().subscribe(() => {
        //console.log('The snack-bar was dismissed');
        //this.gstin="";
        if(this.offer_id >0)
        {
          this.router.navigateByUrl('/proposal/logs');
          /*if(this.offer_type=='proforma invoice')
          {
            this.router.navigateByUrl('/proposal/proforma-history');
          }
          if(this.offer_type=='proposal')
          {
            this.router.navigateByUrl('/proposal/proposal-history');
          }*/
        }
        else
        {
          this.router.navigate(['proposal/search-exhibitor']);
        }
      });
      //document.getElementById('loader').style.display="none";
    });
  }

  getExhibitorAddons() {
    let exSKU = [];
    let searchParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "eventCode": environment.greenCloudConfig.EventCode,
      "source": environment.greenCloudConfig.Source,
    }
    this.proposalService.getExhibitorAddons(searchParams)
    .subscribe(extraReqRes => {
      //console.log('extraReqRes', extraReqRes);
      this.standAmenityMasterData = extraReqRes['result']['stand_amenities'];
      this.sponsorshipMasterData = extraReqRes['result']['sponsorship_options'];
    });
  }
  
  openStallDialog() {
    //localStorage.setItem('selectedSM', JSON.stringify(SM));
    this.dialog.open(Kisan18StallComponent,{
      panelClass: 'my-stall-screen-dialog',
      data:this.history_info
    });
  }

  openCompDetailsDialog() {
    //localStorage.setItem('selectedSM', JSON.stringify(SM));
    this.dialog.open(CompanyInfoComponent,{
      panelClass: 'my-Comp-screen-dialog',
      data:this.exhinfo
    });
  }

  addproduct(prev_product=null)
  {
 // console.log('index', index);

    if(prev_product==null)
    {

      let product={ "stand_number":"" , "sku":"","id":"","price":0.0,"pavillion_type":"","stand_type":"",
        "stand_location":"","area":"","advance_type":"percentage","advance_amount":100,
        "discount_type":"percentage","discount_amount":0,"is_valid":false,"is_valid_discount":true,
        "is_checked": false,"pavillion_types":this.masterdata.pavillion_types ,
        "stand_types" : this.masterdata.stand_types ,"show_area_list":false,"index":this.prod_index ,
        "stand_locations" : this.masterdata.stand_locations , "area_list":[] , "offer_option":1 ,
        "pavillion_name" : "" , "stand_type_name" : "" ,"stand_location_name" : "" ,"is_stand_availabel":true  } ;
      this.products.push(product);

    }
    else
    {
      let product={ "sku":prev_product.sku,"id":prev_product.stall_id,"stand_number" : prev_product.stand_number ,
        "price":prev_product.price,"pavillion_type":prev_product.pavillion_type,
        "stand_type":prev_product.stand_type,"stand_location":prev_product.stand_location,
        "area":prev_product.area,"advance_type":prev_product.advance_type,
        "advance_amount":prev_product.advance_amount, "discount_type":prev_product.discount_type,
        "discount_amount":prev_product.discount_amount,"is_valid":true,"is_valid_discount":true,
        "is_checked": (prev_product.discount_type == 'percentage') ? false :true ,
        "pavillion_types":this.masterdata.pavillion_types ,"index":this.prod_index ,
        "stand_types" : this.masterdata.stand_types ,"show_area_list":false,
        "stand_locations" : this.masterdata.stand_locations , "area_list":[],"offer_option":1 ,
        "pavillion_name" : "" , "stand_type_name" : "" ,"stand_location_name" : "" ,"is_stand_availabel":true} ;
      if(prev_product.advance_type=='percentage' && prev_product.advance_amount==20)
      {
        product.offer_option=2;
      }
      if(prev_product.advance_type=='fixed_amount' && prev_product.advance_amount==1000)
      {
        product.offer_option=3;
      }
      var pav_type_code=prev_product.sku.charAt(3);
      var stand_type_code=prev_product.sku.charAt(4);
      var stand_location_code=prev_product.sku.charAt(5);

      this.masterdata['pavillion_types'].forEach(function (item) {
        if(item['short_code']==pav_type_code)
        {
          product.pavillion_name=item['name'];
        }
      });
     //console.log(this.masterdata['stand_types']);
      this.masterdata['stand_types'].forEach(function (item) {
        if(item['short_code']==stand_type_code)
        {
          product.stand_type_name=item['name'];
        }
      });
      //console.log(this.masterdata['stand_locations']);
      this.masterdata['stand_locations'].forEach(function (item) {
        if(item['short_code']==stand_location_code)
        {
          product.stand_location_name=item['name'];
        }
      });

      // this.selectedSku = product['sku'];
      //console.log('this.selectedSku', this.selectedSku);
      this.products.push(product);
    }
    this.prod_index +=1;
    //console.log('this.products', this.products);

  }
  addExtraProducts(prevProd?) {
    if (prevProd) {
      const idx = this.products.findIndex(prd => prd.id === prevProd.stall_id);
      //this.selected_stall_SKU = prevProd.stall_sku;
      let extraProducts = {
        "stand_number": prevProd.stand_number,
        "id": prevProd.addon_id,
        "price": prevProd.base_price,
        "quantity": prevProd.quantity,
        "stand_id":prevProd.stand_id,
        "is_valid": true
      }
      this.extra_products.push(extraProducts);
    } 
    else 
    {
      let extraProducts = {
        "stand_number":"",
        "id": "",
        "price": "",
        "quantity": 1,
        "stand_id": "",
        "is_valid": false
      }
      this.extra_products.push(extraProducts);
    }
    //console.log('this.extra_products', this.extra_products);
  }

  addSponsorshipOption(prevProd?) {
    if (prevProd) {
      let extraProducts = {
        "id": prevProd.addon_id,
        "price": prevProd.base_price,
        "quantity": prevProd.quantity,
        "is_valid": true
      }
      this.sponsorship_options.push(extraProducts);
    } 
    else 
    {
      let extraProducts = {
        "id": "",
        "price": "",
        "quantity": 1,
        "is_valid": false
      }
      this.sponsorship_options.push(extraProducts);
    }
    //console.log('this.extra_products', this.extra_products);
  }

  selectedProductIndx(event: string) {
    const idx = this.products.findIndex(prd => prd.sku === event)
    this.extra_products.forEach((elmt) => {
      if(elmt.stall_sku == event) {
        elmt.product_index = idx;
      }
    });
  }
  selectedStandForAmenities(extraproduct) {
    this.stalls_for_amenities.forEach((elmt) => {
      if(elmt.stand_id == extraproduct.stand_id) {
        extraproduct.stand_number = elmt.stand_number;
      }
    });
    if(extraproduct.quantity && extraproduct.id && extraproduct.stand_id ) {
      extraproduct.is_valid = true;
    }
    else
    {
      extraproduct.is_valid = false;
    }
    //console.log(extraproduct);
  }

  changeoffer(product)
  {
    if(product.offer_option==1)
    {
      product.advance_type="percentage";
      product.advance_amount=100;
    }
    if(product.offer_option==2)
    {
      product.advance_type="percentage";
      product.advance_amount=20;
    }
    if(product.offer_option==3)
    {
      product.advance_type="fixed_amount";
      product.advance_amount=1000;
    }
  }

  removeproduct(index)
  {
    //console.log(this.products{})
    let product=this.products[index];
    //console.log(product.id);
    if(product.id)
    {
      /*this.extra_products.forEach((elt) => {
        if(elt.stand_id==product.id)
        {
          elt.stand_id="";
          elt.stand_number="";
          elt.is_valid=false;
        }
      });*/
      var j = this.extra_products.length;
      while(j!=-1 && j-- ){
        if( this.extra_products[j] 
          && this.extra_products[j]['stand_id'] === product.id  )
        { 
              this.extra_products.splice(j,1);
              i=-1;
        }
      }
      var i = this.stalls_for_amenities.length;
      //console.log(i);
      //console.log(i);
      while(i!=-1 && i-- ){
        //console.log(this.stalls_for_amenities[i]);
        //console.log(this.stalls_for_amenities[i]['stand_id'] === product.id );
        if( this.stalls_for_amenities[i] 
            && this.stalls_for_amenities[i]['stand_id'] === product.id  )
        { 
              this.stalls_for_amenities.splice(i,1);
              i=-1;
        }
      }
    }
    this.products.splice(index, 1);
  }
  removeExtraProduct(index) {

    this.extra_products.splice(index, 1);
    if (this.extra_products.length===0) {
      this.isExtraReq = true;
      this.accordian.expanded = false;
    }
  }
  removeSponsorshipOptions(index) {

    this.sponsorship_options.splice(index, 1);
    if (this.sponsorship_options.length===0) {
      //this.isExtraReq = true;
      this.accordian1.expanded = false;
    }
  }
  
  selectedStandAmenity(extraproduct) {
    this.standAmenityMasterData.forEach((elt) => {
      if(extraproduct.id === elt['id']) {
        //this.extra_products.forEach((elmt, idx) => {
          //if (index === idx) {
            //extraproduct.sku = elt['SKU'];
            extraproduct.price = elt['online_price'];
            extraproduct.id = elt['id'];
         // }
       // });
      }
    });
  
    if(extraproduct.quantity && extraproduct.id && extraproduct.stand_id ) {
      extraproduct.is_valid = true;
    }
    else
    {
      extraproduct.is_valid = false;
    }
    //console.log(extraproduct);
  }

  selectedSponsershipOption(extraproduct) {
    this.sponsorshipMasterData.forEach((elt) => {
      if(extraproduct.id === elt['id']) {
        //this.extra_products.forEach((elmt, idx) => {
          //if (index === idx) {
            //extraproduct.sku = elt['SKU'];
            extraproduct.price = elt['online_price'];
            extraproduct.id = elt['id'];
         // }
       // });
      }
    });
    
    //console.log(extraproduct);
    if(extraproduct.quantity ) {
      extraproduct.is_valid = true;
    }
  }

  increaseQty(extraProduct) {
    extraProduct.quantity === ''?extraProduct.quantity = 0: extraProduct.quantity;
    extraProduct.quantity+=1;
    extraProduct.quantity = extraProduct.quantity;
    if(extraProduct.quantity && extraProduct.sku) {
      extraProduct.is_valid = true;
    }

  }
  decreaseQty(extraProduct) {
    extraProduct.quantity-=1;
    if(extraProduct.quantity<=0){
      extraProduct.quantity = 1;
    }
    extraProduct.quantity = extraProduct.quantity;
    if(extraProduct.quantity && extraProduct.sku) {
      extraProduct.is_valid = true;
    }
  }

  validateStandNumber(product)
  {
    this.check_stall_by_stand_number(product,'sku');
  }

  invalidSKU(product)
  {
    product.pavillion_type="";
    product.stand_type="";
    product.stand_location="";
    product.area="";
    product.id='';
    product.price=0.0;
    product.is_valid=false;
  }
  //"pavillion_name" : "" , "stand_type_name" : "" ,"stand_location_name" : ""
  invalidStand(product)
  {
    product.pavillion_type="";
    product.stand_type="";
    product.stand_location="";
    product.area="";
    product.id='';
    product.sku='';
    product.pavillion_name='';
    product.stand_type_name='';
    product.stand_location_name='';
    product.price=0.0;
    product.is_valid=false;
  }

  invalidProductDetails(product)
  {
    //product.sku="";
    product.id='';
    product.price=0.0;
    product.is_valid=false;
  }


  check_shopify_product(product,what_changed)
  {
    let params = {
      "app_key": environment.greenCloudConfig.AppKey,
      "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode,
      "sku": product.sku
    }
    this.proposalService.get_shopify_product_by_sku(params).then(
      res => {
        //console.log(res);
        if(res.responseCode==0 && res.success==true)
        {
          if(res.products && res.products.length==1)
          {
            product.id=res.products[0].id;
            product.price=res.products[0].price;
            product.is_valid=true;
            this.validateDiscount(product);
            if(what_changed=='sku')
            {
              this.changeDetailsBySKU(product);
            }
          }
          else
          {
            if(what_changed=='sku')
            {
              this.invalidSKU(product);
            }
            if(what_changed=='productdetails')
            {
              this.invalidProductDetails(product);
            }
          }
        }
        else
        {
          if(what_changed=='sku')
          {
            this.invalidSKU(product);
          }
          if(what_changed=='productdetails')
          {
            this.invalidProductDetails(product);
          }
        }
      },
      error=>{
        if(what_changed=='sku')
        {
          this.invalidSKU(product);
        }
        if(what_changed=='productdetails')
        {
          this.invalidProductDetails(product);
        }
    });
  }

  check_stall_by_stand_number(product,what_changed)
  {
    let prev_product_id=product.id;
    if(prev_product_id && prev_product_id!="")
    {
      var i = this.stalls_for_amenities.length;
      //console.log(i);
      //console.log(i);
      while(i!=-1 && i-- ){
        //console.log(this.stalls_for_amenities[i]);
        //console.log(this.stalls_for_amenities[i]['stand_id'] === product.id );
        if( this.stalls_for_amenities[i] 
            && this.stalls_for_amenities[i]['stand_id'] === prev_product_id  )
        { 
              this.stalls_for_amenities.splice(i,1);
              i=-1;
        }
      }
    }
    let params = {
      "app_key": environment.greenCloudConfig.AppKey,
      "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode,
      "stand_number": product.stand_number
    }
    this.proposalService.get_stall_by_stand_number(params).then(
      res => {
        //console.log(res);
        if(res.responseCode==0 && res.success==true)
        {
          if(res.products && res.products.length==1)
          {
            let stand_status = res.products[0].status
            if(stand_status !='available' && this.username_for_offer != res.products[0].proposed_exhibitor_username)
            {
              product.is_stand_availabel = false;
              if(what_changed=='sku')
              {
                this.invalidStand(product);
              }
              if(what_changed=='productdetails')
              {
                this.invalidProductDetails(product);
              }
            }
            else
            {
              product.id=res.products[0].id;
              product.sku=res.products[0].sku;
              product.price=res.products[0].price;
              product.is_valid=true;
              product.discount_amount=0;
              product.is_stand_availabel = true;
              this.validateDiscount(product);
              if(what_changed=='sku')
              {
                this.changeDetailsByStand(product);
              }
              
              let stand_for_amenity={"stand_id": res.products[0].id,
              "stand_number": product.stand_number,
              "status":"available"};
              this.stalls_for_amenities.push(stand_for_amenity);
       
            }
          }
          else
          {
            product.is_stand_availabel = true;
            if(what_changed=='sku')
            {
              this.invalidStand(product);
            }
            if(what_changed=='productdetails')
            {
              this.invalidProductDetails(product);
            }
          }
        }
        else
        {
          product.is_stand_availabel = true;
          if(what_changed=='sku')
          {
            this.invalidStand(product);
          }
          if(what_changed=='productdetails')
          {
            this.invalidProductDetails(product);
          }
        }
      },
      error=>{
        product.is_stand_availabel = true;
        if(what_changed=='sku')
        {
          this.invalidStand(product);
        }
        if(what_changed=='productdetails')
        {
          this.invalidProductDetails(product);
        }
    });
  }

  changeDetailsBySKU(product){
    let sku=product.sku;
    var pav_type_code= sku.charAt(3);
    var stand_type_code=sku.charAt(4);
    var stand_loc_code=sku.charAt(5);
    product.pavillion_type=pav_type_code;
    product.stand_type=stand_type_code;
    product.stand_location=stand_loc_code;
    var a1=sku.substring(6);
    var a2=a1.split('_');
    //console.log(a2);
    product.area=parseInt(a2[0]);
    //console.log(product.area);
  }

  changeDetailsByStand(product){
    let sku=product.sku;
    var pav_type_code= sku.charAt(3);
    var stand_type_code=sku.charAt(4);
    var stand_loc_code=sku.charAt(5);
    product.pavillion_type=pav_type_code;
    product.stand_type=stand_type_code;
    product.stand_location=stand_loc_code;
    var a1=sku.substring(6);
    var a2=a1.split('_');
    //console.log(a2);
    product.area=parseInt(a2[0]);
  this.masterdata['pavillion_types'].forEach(function (item) {
    if(item['short_code']==pav_type_code)
    {
      product.pavillion_name=item['name'];
    }
  });
 //console.log(this.masterdata['stand_types']);
  this.masterdata['stand_types'].forEach(function (item) {
    if(item['short_code']==stand_type_code)
    {
      product.stand_type_name=item['name'];
    }
  });
  //console.log(this.masterdata['stand_locations']);
  this.masterdata['stand_locations'].forEach(function (item) {
    if(item['short_code']==stand_loc_code)
    {
      product.stand_location_name=item['name'];
    }
  });
  }

  validateDiscount(product)
  {
   // this.subscription = evt.control.valueChanges.subscribe(val => {
      if(product.discount_type === 'percentage') {
        //evt.control.setValidators(Validators.compose([Validators.minLength(1), Validators.maxLength(3)]));
        // evt.control.updateValueAndValidity();
        if (product.discount_amount < 0 || product.discount_amount > 100) {
          product.discount_amount=0;
        }
      } else {
        if(product.price > 0 && product.discount_amount > product.price)
        {
          product.discount_amount=0;
        }
        // evt.control.setValidators(Validators.compose([Validators.minLength(2), Validators.maxLength(7), Validators.pattern("[0]{1}[0-9]{9}")]));
        // evt.control.updateValueAndValidity();
      }
    //});
  }

  validate_offer()
  {
    let isValid=true;
    if(this.payment_last_date!=undefined && this.payment_last_date!='' && this.payment_last_date!=null)
    {
      for (let product of this.products) {
        //console.log(product.is_valid);
        if(!product.is_valid)
        {
          isValid=false;
        }
        if(!product.is_valid_discount)
        {
          isValid=false;
        }
      }
    }
    else
    {
      isValid=false;
    }
    //console.log(isValid);
    return isValid;
  }
  validate_ExtraReq() {
    let isValid = true;
    if(this.payment_last_date!=undefined && this.payment_last_date!='' && this.payment_last_date!=null) {
      for (let product of this.extra_products) {
          if(!product.is_valid)
          {
            isValid=false;
          } else {
            isValid=true;
          }
      }
    } else
    {
      isValid=false;
    }
    // console.log('extra_prod', this.extra_products);

   // console.log('isValid', isValid)
    return isValid;
  }
  validate_SponsorshipOptions() {
    let isValid = true;
    if(this.payment_last_date!=undefined && this.payment_last_date!='' && this.payment_last_date!=null) {
      for (let product of this.sponsorship_options) {
          if(!product.is_valid)
          {
            isValid=false;
          } else {
            isValid=true;
          }
      }
    } else
    {
      isValid=false;
    }
    // console.log('extra_prod', this.extra_products);

   // console.log('isValid', isValid)
    return isValid;
  }

  create_offer()
  {
    if(this.validate_offer() )
    {
      document.getElementById('loader').style.display="block";
      //console.log(this.payment_last_date);
      //console.log(this.sharedService.toMYSQLDate(this.payment_last_date));
      let payment_last_datetime= this.sharedService.toMYSQLDate(this.payment_last_date);
      payment_last_datetime+=" 00:00:00";
      var offer_type_parts = this.offer_type.split(" ");
      let params = {
        "app_key": environment.greenCloudConfig.AppKey, "source": environment.greenCloudConfig.Source,
        "eventCode": environment.greenCloudConfig.EventCode, "sessionId": localStorage.getItem('sessionId'),
        "exhibitor_username":this.username_for_offer,"offer_type":offer_type_parts[0],
        "payment_last_datetime":payment_last_datetime,
       // "product_info": this.productServicesFormCtrl.value,
        //"tds_percentage": this.tdsPercentage == true ? this.tdsPercentage = 2 : this.tdsPercentage = 0, 
        //"payment_mode":this.bankDetails.controls.paymentMode.value, 
       // "payment_date": this.bankDetails.controls.paymentDate.value,
       // "bank_name" :this.bankDetails.controls.bankName.value,
        //"bank_transaction_ref_id": this.bankDetails.controls.chequeNo.value,
       // "is_rebook":this.is_rebook ? 1 : 0
      }
      let product_list=[];
      for (let prev_product of this.products) {
        let product={ "sku":prev_product.sku,"id":prev_product.id, "stand_number": prev_product.stand_number ,
        "price":prev_product.price,"pavillion_type":prev_product.pavillion_type,
        "stand_type":prev_product.stand_type,"stand_location":prev_product.stand_location,
        "area":prev_product.area,"advance_type":prev_product.advance_type,
        "advance_amount":prev_product.advance_amount, "discount_type":prev_product.discount_type,
        "discount_amount":prev_product.discount_amount,"index":prev_product.index ,"is_stand_availabel":true
      } ;
      product_list.push(product);
      }
      params["products"]=product_list;
      params["extra_products"] = this.extra_products;
      params["sponsorship_options"] = this.sponsorship_options;


    //  console.log(JSON.stringify(params));
      this.proposalService.create_offer(params).then(res => {
        //console.log(res);
        if(res.responseCode==0 && res.success==true)
        {
          this.offer_generated=true;
          let offer_data={"offer_id":res.offer_id,"pdf_url":res.pdf_url};
          localStorage.setItem('offer_info', JSON.stringify(offer_data));
          setTimeout(()=>{
            this.router.navigateByUrl('proposal/send-offer?offer_id='+res.offer_id);
            /*if(offer_type_parts[0]=='proforma')
            {
              this.router.navigateByUrl('proposal/preview-offer');
            }
            else
            {
              this.router.navigateByUrl('proposal/send-offer?offer_id='+res.offer_id);
            }*/
          }, 1000);
        }
        else
        {
          this.snackBar.open(res.message, '', { duration: 5000, });
        }
        document.getElementById('loader').style.display="none";
      },
      err=>{
        //console.log(err.error);
        this.snackBar.open(err.error.message, '', { duration: 5000, });
        document.getElementById('loader').style.display="none";
      });
    }
  }

  slideToggle(event: EventEmitter, product:any) {
    product.is_checked=event['checked'];
    if (event['checked']) {
      product.discount_type='fixed_amount';
    }
    else
    {
      product.discount_type = 'percentage';
    }
    this.validateDiscount(product);
    //console.log('this.products', this.products)
  }

  filter_product_details(product:any,changed_param='')
  {
   // console.log('product', product);
    let params = {
      "app_key": environment.greenCloudConfig.AppKey, "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode, "pavillion_type_code": product.pavillion_type,
      "stand_type_code":product.stand_type,"stand_location_code":product.stand_location
    }
    if(changed_param =='pavillion_type')
    {
      params.stand_type_code="";
      params.stand_location_code="";
      product.stand_type="";
      product.stand_location="";
      product.area="";
      product.area_list=[];

    }
    if(changed_param =='stan_type')
    {
      //params.pavillion_type_code="";
      params.stand_location_code="";
      product.stand_locations=[];

    }
    //console.log(JSON.stringify(params));
    this.proposalService.filter_product_details(params).then(res => {
      // console.log(res);
      if(res.responseCode==0 && res.success==true)
      {
        if(res.output.area.length==0)
        {
          product.pavillion_types=this.masterdata.pavillion_types ;
          product.stand_types = this.masterdata.stand_types ;
          // product.stand_locations = this.masterdata.stand_locations ;
          product.area_list=[];
          product.area='';
        }
        else
        {
          if(res.output.pavillion_type_code.length>0)
          {
            //console.log(res.output.pavillion_type_code);
            product.pavillion_types=[];
            for (let master_pavillion_type of this.masterdata.pavillion_types) {
              for (let filter_pavillion_type of res.output.pavillion_type_code) {
                if(master_pavillion_type.short_code==filter_pavillion_type)
                {
                  product.pavillion_types.push(master_pavillion_type);
                }
              }
            }
          }
          if(res.output.stand_type_code.length>0)
          {
            //console.log(res.output.stand_type_code);
            product.stand_types=[];
            for (let master_stand_type of this.masterdata.stand_types) {
              for (let filter_stand_type of res.output.stand_type_code) {
                if(master_stand_type.short_code==filter_stand_type)
                {
                  product.stand_types.push(master_stand_type);
                }
              }
            }
          }
          if(res.output.stand_location_code.length>0)
          {
            //console.log(res.output.stand_location_code);
            product.stand_locations=[];
            for (let master_stand_location of this.masterdata.stand_locations) {
              for (let filter_stand_location of res.output.stand_location_code) {
                if(master_stand_location.short_code==filter_stand_location)
                {
                  product.stand_locations.push(master_stand_location);
                }
              }
            }
          }
          //console.log(res.output.area);
          product.area_list=res.output.area;
          if(product.pavillion_types.length==1)
          {
            product.pavillion_type=product.pavillion_types[0].short_code;
          }
          else
          {
            let matched=false;
            for (let item of product.pavillion_types) {
              if(product.pavillion_type===item.short_code)
              {
                matched=true;
              }
            }
            if(!matched)
            {
              product.pavillion_type='';
            }
          }
          if(product.stand_types.length==1)
          {
            product.stand_type=product.stand_types[0].short_code;
          }
          else
          {
            let matched=false;
            for (let item of product.stand_types) {
              if(product.stand_type===item.short_code)
              {
                matched=true;
              }
            }
            if(!matched)
            {
              product.stand_type='';
            }
          }
          if(product.stand_locations.length==1)
          {
            product.stand_location=product.stand_locations[0].short_code;
          }
          else
          {
            let matched=false;
            for (let item of product.stand_locations) {
              if(product.stand_location===item.short_code)
              {
                matched=true;
              }
            }
            if(!matched)
            {
              product.stand_location='';
            }
          }
          if(product.area_list.length==1)
          {
            product.area=product.area_list[0];
            //this.validateProductDetails(product);
          }
          else
          {
            let matched=false;
            for (let item of product.area_list) {
              if(product.area===item)
              {
                matched=true;
              }
            }
            if(!matched)
            {
              product.area='';
            }
          }
        }
        //this.validateProductDetails(product);
        product.show_area_list=true;
      }
      else
      {
        product.pavillion_types=this.masterdata.pavillion_types ;
        product.stand_types = this.masterdata.stand_types ;
        product.stand_locations = this.masterdata.stand_locations ;
        product.area_list=[];
        product.show_area_list=false;
      }
      // document.getElementById('loader').style.display="none";
     },
     err=>{
        //console.log(err.error);
        product.pavillion_types=this.masterdata.pavillion_types ;
        product.stand_types = this.masterdata.stand_types ;
        product.stand_locations = this.masterdata.stand_locations ;
        product.area_list=[];
        product.show_area_list=false;
       //this.snackBar.open(err.error.message, '', { duration: 5000, });
       //document.getElementById('loader').style.display="none";
     });
  }

  getRupeeFormat(amount)
  {
    return this.sharedService.getRupeeFormat(amount);
  }

  getDiscountPrice(product)
  {
    if (product.discount_type=='fixed_amount') {
      return this.getRupeeFormat(this.roundNumberV1((product.price - product.discount_amount),0));
    }
    if (product.discount_type=='percentage') {
      return this.getRupeeFormat(this.roundNumberV1((product.price - ((product.discount_amount * product.price)/100)),0));
    }
  }
  getPriceWithGST(product)
  {
    if(parseFloat(product.price) > 0.00)
    {
      if(parseFloat(product.discount_amount) > 0.00)
      {
        if (product.discount_type=='fixed_amount') {
          return this.getRupeeFormat(this.roundNumberV1(((product.price - product.discount_amount) * 1.18),0));
        }
        if (product.discount_type=='percentage') {
          return this.getRupeeFormat(this.roundNumberV1(((product.price - ((product.discount_amount * product.price)/100)) * 1.18),0));
        }
      }
      else
      {
        return this.getRupeeFormat(this.roundNumberV1((product.price * 1.18),0));
      }
    }
    else
    {
      return '';
    }
  }

  getDiscountPriceAmt(product)
  {
    if (product.discount_type=='fixed_amount') {
      return this.roundNumberV1((product.price - product.discount_amount),0);
    }
    if (product.discount_type=='percentage') {
      return this.roundNumberV1((product.price - ((product.discount_amount * product.price)/100)),0);
    }
  }
  getPriceWithGSTAmt(product)
  {
    if(parseFloat(product.price) > 0.00)
    {
      if(parseFloat(product.discount_amount) > 0.00)
      {
        if (product.discount_type=='fixed_amount') {
          return this.roundNumberV1(((product.price - product.discount_amount) * 1.18),0);
        }
        if (product.discount_type=='percentage') {
          return this.roundNumberV1(((product.price - ((product.discount_amount * product.price)/100)) * 1.18),0);
        }
      }
      else
      {
        return this.roundNumberV1((product.price * 1.18),0);
      }
    }
    else
    {
      return 0.00;
    }
  }

  roundNumberV1(num, scale) {
    if(num && num!='')
    {
      if(!("" + num).includes("e")) 
      {
        return +(Math.round(parseFloat(num + "e+" + scale))  + "e-" + scale);  
      } 
      else 
      {
        var arr = ("" + num).split("e");
        var sig = ""
        if(+arr[1] + scale > 0) {
            sig = "+";
        }
        var i = +arr[0] + "e" + sig + (+arr[1] + scale);
        var j = Math.round(parseFloat(i));
        var k = +(j + "e-" + scale);
        return k;  
      }
    }
    else
    {
      return 0;
    }
  }

  checkprice()
  {
    let net_amount=0.00;
    let total_amount=0.00
    for (let product of this.products) {
      //console.log(product);
      if(product.is_valid && product.is_valid_discount)
      {
        net_amount=net_amount + this.getDiscountPriceAmt(product);
        total_amount=total_amount + this.getPriceWithGSTAmt(product);
      }
    }
    //extra_products = [];
    //sponsorship_options=[];
    for (let product of this.extra_products) {
      //console.log(product);
      if(product )
      {
        net_amount=net_amount + this.roundNumberV1((product.price*product.quantity),0);
        total_amount=total_amount + this.roundNumberV1(1.18 * (product.price*product.quantity),0);
      }
    }
    for (let product of this.sponsorship_options) {
      //console.log(product);
      if(product.is_valid )
      {
        net_amount=net_amount + this.roundNumberV1((product.price),0);
        total_amount=total_amount + this.roundNumberV1((1.18 * product.price),0);
      }
    }
    this.netamount=this.getRupeeFormat(net_amount);
    this.totalamount=this.getRupeeFormat(total_amount);
    //console.log(this.netamount);
    return true;
  }
    
  
}




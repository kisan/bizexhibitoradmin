import { Injectable } from '@angular/core';
//import { Observable } from 'rxjs/Observable';
import {Router} from "@angular/router";
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class ProposalService {

  constructor(private router:Router, private http: HttpClient) { }
  search_exhibitor(requestParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(requestParams);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"searchexhibitor.php", body, { headers: headers })
    .toPromise();
  }

  check_user_status(requestParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(requestParams);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"checkuserstatus.php", body, { headers: headers })
    .toPromise();
  }

  get_company_details_by_gstin(requestParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(requestParams);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"getcompanygstn.php", body, { headers: headers })
    .toPromise();
  }

  create_exhibitor(requestParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(requestParams);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"addexhibitor.php", body, { headers: headers })
    .toPromise();
  }
  update_exhibitor(requestParam: Object) {
    const updateUrl = `${environment.greenCloudConfig.ApiBaseUrl}updateexhibitor.php`;
    let headers = {headers : new HttpHeaders({'Content-Type':'application/json'})};

    return this.http.post(updateUrl, requestParam, headers);
  }
  uploadExhibitorLogo(file, userName: string, adminName:string) {
      const formData = new FormData;
      const uploadUrl = `${environment.oAuthConfig.ApiBaseUrl}profile_upload/`;
      formData.append('myfile', file);
      formData.append('username', userName);
      formData.append('admin_name', adminName);
      formData.append('client_id', environment.oAuthConfig.ClientId);
      formData.append('client_secret', environment.oAuthConfig.ClientSecret);
      return this.http.post(uploadUrl, formData);
  }
  uploadProductImages(file) {
    const formData = new FormData;
    // const sessionId = localStorage.getItem('sessionId');
    const uploadImgUrl = `${environment.greenCloudConfig.ApiBaseUrl}upload_file_exhibitor_products.php`;
    formData.append('file', file);
    formData.append('app_key',environment.greenCloudConfig.AppKey);
    formData.append('sessionId', localStorage.getItem("sessionId"));
      return this.http.post(uploadImgUrl, formData, {
        params: new HttpParams().set('app_key', environment.greenCloudConfig.AppKey).set('sessionId', localStorage.getItem('sessionId'))
      });
  }

  get_shopify_product_by_sku(requestParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(requestParams);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"searchShopifyProductBySKU.php", body, { headers: headers })
    .toPromise();
  }

  get_stall_by_stand_number(requestParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(requestParams);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"search_stall_by_stand_no.php", body, { headers: headers })
    .toPromise();
  }

  create_offer(requestParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(requestParams);
    //console.log(body);
   // console.log(body);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"createoffer.php", body, { headers: headers })
    .toPromise();
  }
  get_offer_details(requestParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(requestParams);
    //console.log(body);
   // console.log(body);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"getofferdetails.php", body, { headers: headers })
    .toPromise();
  }
  send_offer(requestParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(requestParams);
    //console.log(body);
   // console.log(body);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"sendoffernotification.php", body, { headers: headers })
    .toPromise();
  }
  /* get_proforma_history(requestParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(requestParams);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"getproformahistory.php", body, { headers: headers })
    .toPromise();
  } */
  get_proposal_proforma_history(requestParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(requestParams);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl + "getoffernotification.php", body, { headers: headers })
    .toPromise();
  }

  get_notification_details(requestParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(requestParams);
    //console.log(body);
   // console.log(body);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"getnotificationofferdetails.php", body, { headers: headers })
    .toPromise();
  }
  get_offer_usage(requestParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(requestParams);
    //console.log(body);
   // console.log(body);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"getofferusage.php", body, { headers: headers })
    .toPromise();
  }
  get_proposal_vs_product(requestParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(requestParams);
    //console.log(body);
   // console.log(body);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"getproposalvsproductsummary.php", body, { headers: headers })
    .toPromise();
  }
  filter_product_details(requestParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(requestParams);
    //console.log(body);
   // console.log(body);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"getfilterproductdetails.php", body, { headers: headers })
    .toPromise();
  }
  getShopifyExtraRequirment(requestParam: Object) {
    let extraReqUrl = `${environment.greenCloudConfig.ApiBaseUrl}getshoppifyextrarequirement.php`;
    return this.http.post(extraReqUrl, requestParam);
  }
  get_stalls(requestParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(requestParams);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl + "getstalllist.php", body, { headers: headers })
    .toPromise();
  }
  update_stall_status(requestParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(requestParams);
    //console.log(body);
    //console.log(environment.greenCloudConfig.ApiBaseUrl + "changestallstatus.php")
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl + "changestallstatus.php", body, { headers: headers })
    .toPromise();
  }
  get_stall_status_history(requestParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(requestParams);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl + "getstallstatushistory.php", body, { headers: headers })
    .toPromise();
  }

  uploadStandImage(file) {
    //let headers = new HttpHeaders({'Content-Type':'application/json'});
    const formData = new FormData;
    // const sessionId = localStorage.getItem('sessionId');
    const uploadImgUrl = `${environment.greenCloudConfig.ApiBaseUrl}upload_stand_images.php`;
    formData.append('file', file);
    formData.append('app_key',environment.greenCloudConfig.AppKey);
    formData.append('sessionId', localStorage.getItem("sessionId"));
      return this.http.post(uploadImgUrl, formData, {
        params: new HttpParams().set('app_key', environment.greenCloudConfig.AppKey).set('sessionId', localStorage.getItem('sessionId'))
      });
  }
  addStall(requestParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(requestParams);
    //console.log(body);
   // console.log(body);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"addstallinfo.php", body, { headers: headers })
    .toPromise();
  }
  get_stand_details(requestParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(requestParams);
    //console.log(body);
   // console.log(body);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"getstandinfo.php", body, { headers: headers })
    .toPromise();
  }
  editStall(requestParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(requestParams);
    //console.log(body);
   // console.log(body);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"editstallinfo.php", body, { headers: headers })
    .toPromise();
  }

  get_offer_notification_logs(requestParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(requestParams);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl + "getoffernotificationlogs.php", body, { headers: headers })
    .toPromise();
  }
  getExhibitorAddons(requestParam: Object) {
    let extraReqUrl = `${environment.greenCloudConfig.ApiBaseUrl}getexhibitoraddons.php`;
    return this.http.post(extraReqUrl, requestParam);
  }
}

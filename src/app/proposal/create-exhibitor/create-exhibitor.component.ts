import { Component, OnInit , Inject , EventEmitter, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProposalService  } from '../proposal.service';
import { SharedService  } from '../../shared/shared.service';
import { environment } from '../../../environments/environment';
import { DomSanitizer } from '@angular/platform-browser';
import { FormControl, FormBuilder, FormGroup, Validators, AbstractControl, NG_VALIDATORS, ValidatorFn, Validator } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ViewChild } from '@angular/core';
@Component({
  selector: 'app-create-exhibitor',
  templateUrl: './create-exhibitor.component.html',
  styleUrls: ['./create-exhibitor.component.scss']

})
export class CreateExhibitorComponent implements OnInit {

  activestep=1;
  mobile="";
  email="";
  orgname="";
  firstname="";
  lastname="";
  gstin="";
  country="India";
  country_code="+91";
  state="";
  city="";
  address="";
  pin="";
  webUrl: string;
  marktrusted=false;
  countries={};
  states={};
  objectKeys = Object.keys;
  createClicked=false;
  offer_type="";
  isMobileValid=true;
  isEmailValid=true;
  selectedFile: any;
  base64Url: any;
  prodUrl: any;
  prodImgArr = [];
  uploadedFile: any;
  prodImagePrefix: any;
  admin_name: string;
  cities:{};
  constructor(public dialog: MatDialog , private proposalService: ProposalService, private router:Router, private sharedService: SharedService ,private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.countries=Object.keys(this.sharedService.getCountries());
    this.states=this.sharedService.getStates();
    this.admin_name = localStorage.getItem('loggedInUserName');
    this.offer_type=localStorage.getItem('offer_type');
    if(this.offer_type==undefined || this.offer_type==null)
    {
      this.offer_type='proforma';
      localStorage.setItem('offer_type', 'proforma invoice');
    }
  }

  cleandata()
  {
    this.mobile="";
    this.orgname="";
    this.firstname="";
    this.lastname="";
    this.gstin="";
    this.country="India";
    this.state="";
    this.city="";
    this.address="";
    this.pin="";
    this.marktrusted=false;
  }
  @ViewChild('newExhInfo', { static: false }) myInputVariable: ElementRef;
  detectFiles(event) {
    if (event.target.files && event.target.files[0]) {
      this.selectedFile = event.target.files[0];
      if(this.selectedFile.size < 10000000 && this.selectedFile.name.endsWith(".jpg") || this.selectedFile.name.endsWith(".JPG")
      || this.selectedFile.name.endsWith(".png") || this.selectedFile.name.endsWith(".PNG")
      || this.selectedFile.name.endsWith(".jpeg") || this.selectedFile.name.endsWith(".JPEG"))
      { var reader = new FileReader();
          reader.onload = (e: any) => {
          this.base64Url = e.target.result;
        }
        reader.readAsDataURL(event.target.files[0]);

      }else{
        event.target.value = null;
        this.base64Url=null
        this.snackBar.open('Invalid image file format', '',
        {
          duration: 3000,
        });
        if(this.selectedFile.size > 10000000) {

          this.snackBar.open('Upload image with size less than 10mb', '',
          {
            duration: 3000,
          });
        } else {

        }

      }
    }
  }

  uploadProductImages(event) {
    if (event.target.files && event.target.files[0]) {
      document.getElementById('loader').style.display="block";
      this.uploadedFile = event.target.files[0];
      if(this.uploadedFile.size < 10000000 && this.uploadedFile.name.endsWith(".jpg") || this.uploadedFile.name.endsWith(".JPG")
      || this.uploadedFile.name.endsWith(".png") || this.uploadedFile.name.endsWith(".PNG")
      || this.uploadedFile.name.endsWith(".jpeg") || this.uploadedFile.name.endsWith(".JPEG"))
      {
        if(this.prodImgArr.length <= 4) {
          this.proposalService.uploadProductImages(this.uploadedFile)
          .subscribe(res => {
            if (res['success']===true) {
              this.prodImagePrefix = environment.greenCloudConfig.product_url_prefix;
              this.prodImgArr.push(res['imageDetails']['imgUrl']);
              document.getElementById('loader').style.display="none";
            }
            else{
              //this.activestep=3;
              this.snackBar.open(res['message'], null, {
                duration: 3000,
              });
            }
          }, err => {
            console.log('error', err);
            this.snackBar.open(err.error.message, '',
            {
            duration: 5000,
            });
            document.getElementById('loader').style.display="none";
          }
          );
        }
      }
      else
      {
        this.snackBar.open('Invalid file', '',
        {
        duration: 5000,
        });
        event.target.value= null;
        document.getElementById('loader').style.display="none";
      }
    }
  }

  removeProductImage(idx: number) {
    if(idx != -1) {
      this.prodImgArr.splice(idx, 1);
    }
  }

  checkMobileExists()
  {
    if(this.mobile!=undefined && this.mobile!=null && this.mobile!='' && this.mobile.length==10 )
    {
      this.isMobileValid=true;
      let searchParams = {
        "app_key": environment.greenCloudConfig.AppKey,
        "source": environment.greenCloudConfig.Source,
        "eventCode": environment.greenCloudConfig.EventCode,
        "account_type":2,"type":"mobile","value":this.country_code+this.mobile
      }
      this.proposalService.check_user_status(searchParams).then(res => {
        //console.log(res);
        if(res.responseCode==0 && res.success==true)
        {
          this.alreadyMobileExistsDialog(res.data.username);
        }
        else{
          this.activestep=2;
          setTimeout(()=>{
            document.getElementById('email').focus();
          }, 200);
        }
      },
      err=>{
        console.log(err.error);
        this.snackBar.open(err.error.message, '',
        {
        duration: 5000,
        });
      });
    }
    else
    {
      this.isMobileValid=false;
      //console.log(this.isMobileValid);
    }
  }

  checkEmailExists()
  {
    //this.alreadyEmailExistsDialog();
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(this.email!=undefined && this.email!=null && this.email!='' && re.test(String(this.email).toLowerCase()) )
    {
      //isEmailValid=true;
      let searchParams = {
        "app_key": environment.greenCloudConfig.AppKey,
        "source": environment.greenCloudConfig.Source,
        "eventCode": environment.greenCloudConfig.EventCode,
        "account_type":2,"type":"email","value":this.email
      }
      this.proposalService.check_user_status(searchParams).then(res => {
        //console.log(res);
        if(res.responseCode==0 && res.success==true)
        {
          this.alreadyEmailExistsDialog(res.data.username);
        }
        else{
          this.activestep=3;
        }
      },
      err=>{
        console.log(err.error);
        this.snackBar.open(err.error.message, '',
        {
        duration: 5000,
        });
      });
    }
    else
    {
      this.isEmailValid=false;
    }
  }

  openCreateDialog() {
    let dialogRef = this.dialog.open(CreateCustomePopupComponent,{
      panelClass: 'email_conf',
    });
    dialogRef.afterClosed().subscribe(result => {
    }); //end dialogRef
  }

  alreadyEmailExistsDialog(username) {
    let dialogRef = this.dialog.open(EmailExistsPopupComponent,
      {
        panelClass: 'already_exists',
        data:username});
    dialogRef.afterClosed().subscribe(result => {  }); //end dialogRef
  }

  alreadyMobileExistsDialog(username) {
    let dialogRef = this.dialog.open(MobileExistsPopupComponent,{
      panelClass: 'already_exists',
      data:username});
    dialogRef.afterClosed().subscribe(result => { }); //end dialogRef
  }
  onSubmit(form) {
    //console.log('form submitted');
    //console.log(form);
    if(form.valid && this.createClicked)
    {
      document.getElementById('loader').style.display="block";
      let params = {
        "app_key": environment.greenCloudConfig.AppKey, "source": environment.greenCloudConfig.Source,
        "eventCode": environment.greenCloudConfig.EventCode, "sessionId": localStorage.getItem('sessionId'),
        "orgname": this.orgname,"country_code": this.country_code,"mobile": this.mobile,"email": this.email,
        "firstname": this.firstname,"lastname": this.lastname,"forceemailchange": 0 , "gstin": this.gstin,
        "country": this.country,"state": this.state,"city": this.city,"address": this.address,"pin": this.pin,
        "website": this.webUrl, "product_images": this.prodImgArr.toString(),  "mark_trusted": this.marktrusted ,
        
      }
      this.createClicked=false;
      this.proposalService.create_exhibitor(params).then(res => {
        //console.log('new exhibitorResponse', res);
        if(res.responseCode==0 && res.success==true)
        {
          localStorage.setItem('username_for_offer', res.exhinfo.username);
          if(this.base64Url) {
            this.proposalService.uploadExhibitorLogo(this.selectedFile, res.exhinfo.username, this.admin_name)
            .subscribe(uploadRes => {
             // console.log('uploadRes', uploadRes);
              if(uploadRes['response_code']==0 && uploadRes['success']) {
                this.openCreateDialog();
              } else {
                if(uploadRes['response_code']==101) {
                  this.snackBar.open(uploadRes['message'], null, {
                    duration: 3000,
                  });
                }
              }
            }, (err) => {
              this.snackBar.open('Logo upload failed', null, {
                duration: 3000,
              });
            }
            );
          } else {
            this.openCreateDialog();
          }


        }
        else{
          //this.activestep=3;
          this.snackBar.open(res.message, null, {
            duration: 3000,
          });
        }
        document.getElementById('loader').style.display="none";
      },
      err=>{
        console.log(err.error);
        document.getElementById('loader').style.display="none";
        this.snackBar.open(err.error.message, '',
        {
         duration: 5000,
        });
      });
    }
  }

  fillgstininfo()
  {
    if(this.gstin!=undefined && this.gstin!=null && this.gstin!='' )
    {
      let searchParams = {
        "app_key": environment.greenCloudConfig.AppKey,
        "source": environment.greenCloudConfig.Source,
        "eventCode": environment.greenCloudConfig.EventCode,
        "gstIn":this.gstin
      }
      this.proposalService.get_company_details_by_gstin(searchParams).then(res => {
        //console.log(res);
        if(res.responseCode==0 && res.success==true)
        {
          //this.alreadyEmailExistsDialog(res.data.username);
          this.orgname=res.data.companyname;
          this.address=res.data.address;
          this.state=res.data.state;
          if(res.data.state=='Delhi')
          {
            this.state="Delhi (NCT)";
          }
          if(res.data.state == 'Dadra and Nagar Haveli and Daman and Diu' && res.data.district=='Dadra & Nagar Haveli')
          {
            this.state="Dadra and Nagar Haveli (UT)";
          }
          this.cities=this.states[this.state];
          if(res.data.city!=undefined && res.data.city!='' && res.data.city!='')
          {
            this.city=res.data.city;
          }
          else
          {
            this.city=res.data.district;
          }

          this.pin=res.data.pin;
        }
        else{
          //this.activestep=3;
          let snackBarRef = this.snackBar.open(res.message, null, {
            duration: 3000,
          });
          snackBarRef.afterDismissed().subscribe(() => {
            //console.log('The snack-bar was dismissed');
            this.gstin="";
          });
        }
      },
      err=>{
        console.log(err.error);
        document.getElementById('loader').style.display="none";
        let snackBarRef = this.snackBar.open(err.error.message, '',
        {
        duration: 3000,
        });
        snackBarRef.afterDismissed().subscribe(() => {
          //console.log('The snack-bar was dismissed');
          this.gstin="";
        });
      });
    }
  }

  cancelCreateExhibitor()
  {
    this.router.navigateByUrl('/proposal/search-exhibitor?offer_type='+this.offer_type);
  }

  websiteFieldSts(webField: FormControl) {
  if (webField.dirty && webField.value !== '') {
    webField.setValidators(Validators.compose([Validators.required, Validators.pattern(/^((?:http|ftp)s?:\/\/)(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|localhost|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(?::\d+)?(?:\/?|[\/?]\S+)$/i)]));
    webField.updateValueAndValidity();
  } else if (webField.dirty && webField.value === '') {
    webField.clearValidators();
    webField.updateValueAndValidity();
  }

  }

}

@Component({
  providers: [CreateExhibitorComponent],
  selector: 'app-confirm-popup',
  template: `
  <div class="confirmMsgPopup">
  <mat-dialog-content class="mrgn-b-lg mrgn-l-r">
    <h4 class="mrgn-b-md">Exhibitor created</h4>
    <p>The Exhibibitor has been created successfully .</p>
  </mat-dialog-content>
  <div class="footer-btn">
  <mat-card-footer>
    <button type="button" mat-raised-button mat-dialog-close class="grayBtn" (click)="onParentActionClicked('exhibitor');dialogRef.close()">Create another</button>
    <button type="button" mat-raised-button class="greennewBtn" (click)="onParentActionClicked('offer');dialogRef.close()">Done</button>
    </mat-card-footer>
  </div>
  </div>
  `
})

export class CreateCustomePopupComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<CreateCustomePopupComponent>, public dialog: MatDialog , private router:Router ) { }
  ngOnInit() {  }


  onParentActionClicked(action) {
    if(action=='exhibitor')
    {
      //this.router.navigateByUrl('/proposal/create-exhibitor');
      location.reload();
    }
    else
    {
      this.router.navigateByUrl('/proposal/create-offer');
    }
  }
}

@Component({
  providers: [CreateExhibitorComponent],
  selector: 'app-email-exists-popup',
  template: `
  <div class="exhitsPopup">
  <mat-dialog-content class="mrgn-b-md" *ngIf="exhinfo">
    <img src="../assets/images/exits.png">
    <h5 class="mrgn-b-none font-bold-six">Account Already Exists!</h5>
    <p>We already have an organisation account associated with entered email id -  {{exhinfo.email}}</p>
    <div class="table_main">
      <table>
        <tr>
          <td class="text-right" style="width:135px;">Organisation Name</td>
          <td class="text-center" style="width:15px;">:</td>
          <td class="text-left"><b>{{exhinfo.organisation_name}}</b></td>
        </tr>
        <tr>
          <td class="text-right">Contact Person</td>
          <td class="text-center" style="width:15px;">:</td>
          <td class="text-left"><b>{{exhinfo.first_name}} {{exhinfo.last_name}}</b></td>
        </tr>
        <tr>
          <td class="text-right">Email</td>
          <td class="text-center" style="width:15px;">:</td>
          <td class="text-left"><b style="word-break: break-all;">{{exhinfo.email}}</b></td>
        </tr>
        <tr>
          <td class="text-right">Mobile</td>
          <td class="text-center" style="width:15px;">:</td>
          <td class="text-left"><b>{{exhinfo.mobile}}</b></td>
        </tr>
      </table>
    </div>
  </mat-dialog-content>
  <div class="footer-btn mrgn-b-md">
  <mat-card-footer>
    <button class="closebtn another" mat-dialog-close>Use another email id to complete the process</button>
  </mat-card-footer>
  </div>
  </div>
  `
})

export class EmailExistsPopupComponent implements OnInit {
  exhinfo:any;
  constructor(public dialogRef: MatDialogRef<EmailExistsPopupComponent>, public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any,private proposalService: ProposalService, private router:Router,
    private sharedService: SharedService,private sanitizer: DomSanitizer) { }
  ngOnInit() {
   // console.log('this.data', this.data);
    if(this.data && this.data!='')
    {
      //document.getElementById('loader').style.display="block";
      let searchParams = {
        "app_key": environment.greenCloudConfig.AppKey,
        "source": environment.greenCloudConfig.Source,
        "eventCode": environment.greenCloudConfig.EventCode,
        "type": "username",
        "username":this.data
      }
    //  console.log(searchParams);
      this.proposalService.search_exhibitor(searchParams).then(res => {
       // console.log(res);
        if(res.responseCode==0 && res.success==true)
        {
          this.exhinfo=res.exhinfo.details;
         // this.kisan18_info=res.exhinfo.kisan18_booked_stalls_info;
        }
       // document.getElementById('loader').style.display="none";
      },
      err=>{
        console.log(err);
      });
    }
    else
    {
      this.router.navigate(['proposal/create-exhibitor']);
    }
  }

}

@Component({
  providers: [CreateExhibitorComponent],
  selector: 'app-mobile-exists-popup',
  template: `
  <div class="exhitsPopup">
  <mat-dialog-content class="mrgn-b-md" *ngIf="exhinfo">
    <img src="../assets/images/exits.png">
    <h5 class="mrgn-b-none font-bold-six">Account Already Exists!</h5>
    <p>We already have an organisation account associated with entered Mobile Number - {{exhinfo.mobile}}</p>
    <div class="table_main">
      <table>
        <tr>
          <td class="text-right" style="width:135px;">Organisation Name</td>
          <td class="text-center" style="width:15px;">:</td>
          <td class="text-left"><b>{{exhinfo.organisation_name}}</b></td>
        </tr>
        <tr>
          <td class="text-right">Contact Person</td>
          <td class="text-center" style="width:15px;">:</td>
          <td class="text-left"><b>{{exhinfo.first_name}} {{exhinfo.last_name}}</b></td>
        </tr>
        <tr>
          <td class="text-right">Email</td>
          <td class="text-center" style="width:15px;">:</td>
          <td class="text-left"><b style="word-break: break-all;">{{exhinfo.email}}</b></td>
        </tr>
        <tr>
          <td class="text-right">Mobile</td>
          <td class="text-center" style="width:15px;">:</td>
          <td class="text-left"><b>{{exhinfo.mobile}}</b></td>
        </tr>
      </table>
    </div>
  </mat-dialog-content>
  <div class="footer-btn mrgn-b-md">
  <mat-card-footer>
    <button class="closebtn another" mat-dialog-close>Use another mobile number to complete the process</button>
  </mat-card-footer>
  </div>
  </div>
  `
})

export class MobileExistsPopupComponent implements OnInit {
  exhinfo:any;
  constructor(public dialogRef: MatDialogRef<MobileExistsPopupComponent >, public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any,private proposalService: ProposalService, private router:Router,
    private sharedService: SharedService , private sanitizer: DomSanitizer) { }
  ngOnInit() {
    //console.log('this.data', this.data);
    if(this.data && this.data!='')
    {
      //document.getElementById('loader').style.display="block";
      let searchParams = {
        "app_key": environment.greenCloudConfig.AppKey,
        "source": environment.greenCloudConfig.Source,
        "eventCode": environment.greenCloudConfig.EventCode,
        "type": "username",
        "username":this.data
      }
      //console.log(searchParams);
      this.proposalService.search_exhibitor(searchParams).then(res => {
        //console.log(res);
        if(res.responseCode==0 && res.success==true)
        {
          this.exhinfo=res.exhinfo.details;
         // this.kisan18_info=res.exhinfo.kisan18_booked_stalls_info;
        }
       // document.getElementById('loader').style.display="none";
      },
      err=>{
        console.log(err);
      });
    }
    else
    {
      this.router.navigate(['proposal/create-exhibitor']);
    }
  }
}

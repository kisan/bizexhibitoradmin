import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateExhibitorComponent } from './create-exhibitor.component';

describe('CreateExhibitorComponent', () => {
  let component: CreateExhibitorComponent;
  let fixture: ComponentFixture<CreateExhibitorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateExhibitorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateExhibitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

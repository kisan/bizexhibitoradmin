import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SponsorshipLogComponent } from './sponsorship-log.component';

describe('SponsorshipLogComponent', () => {
  let component: SponsorshipLogComponent;
  let fixture: ComponentFixture<SponsorshipLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SponsorshipLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SponsorshipLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

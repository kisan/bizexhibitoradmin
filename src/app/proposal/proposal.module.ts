import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule ,ReactiveFormsModule } from '@angular/forms';
import { ProposalRoutingModule } from './proposal-routing.module';
import { CustomMaterialModule } from '../custom-material/custom-material.module';
import { SearchExhibitorComponent } from './search-exhibitor/search-exhibitor.component';
import { Kisan18StallComponent } from './kisan18-stall/kisan18-stall.component';
import { GenerateProposalComponent } from './generate-proposal/generate-proposal.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { SendProposalComponent , EmailSentPopupComponent , EmailConfPopupComponent , ReserveConfPopupComponent , ReservedPopupComponent } from './send-proposal/send-proposal.component';
import { NgxEditorModule } from 'ngx-editor';
import { TooltipModule } from 'ngx-bootstrap';
import { ProposalHistoryComponent } from './proposal-history/proposal-history.component';
import { ProposalService } from './proposal.service';
import { SharedService } from '../shared/shared.service';
import { CreateExhibitorComponent } from './create-exhibitor/create-exhibitor.component';
import { CreateCustomePopupComponent} from './create-exhibitor/create-exhibitor.component';
import { EmailExistsPopupComponent} from './create-exhibitor/create-exhibitor.component';
import { MobileExistsPopupComponent} from './create-exhibitor/create-exhibitor.component';
import { ProformaPreviewComponent } from './proforma-preview/proforma-preview.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ProposalDashboardComponent } from './dashboard/dashboard.component';
import { ChartsModule } from 'ng2-charts';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { ProformaHistoryComponent } from './proforma-history/proforma-history.component';
import { CompanyInfoComponent } from './company-info/company-info.component';
import { ProductFilterPipe } from './generate-proposal/productPipe';
import { EditComponent } from './edit-company-info/edit-company.component';
import { StallStatusComponent , StatusConfPopupComponent } from './stall-status/stall-status.component';
import { UpdateStallStatusComponent , DeleteConfPopupComponent } from './update-stall-status/update-stall-status.component';
import { CreateStallComponent , UpdatedStallPopupComponent } from './create-stall/create-stall.component';
import { LogsComponent } from './logs/logs.component';
import { SponsorshipLogComponent } from './sponsorship-log/sponsorship-log.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ProposalRoutingModule,
    CustomMaterialModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgxEditorModule,
    TooltipModule.forRoot(),
    PdfViewerModule,
    CKEditorModule,
    ChartsModule,
    Ng2GoogleChartsModule

  ],
  declarations: [ProductFilterPipe, SearchExhibitorComponent, Kisan18StallComponent,
    CreateCustomePopupComponent, MobileExistsPopupComponent, EmailExistsPopupComponent, 
    GenerateProposalComponent, SendProposalComponent, ProposalHistoryComponent, 
    CreateExhibitorComponent , ProformaPreviewComponent , EmailSentPopupComponent, 
    ProposalDashboardComponent , ProformaHistoryComponent, CompanyInfoComponent, 
    EmailConfPopupComponent, StatusConfPopupComponent, EditComponent, StallStatusComponent, UpdateStallStatusComponent, 
    CreateStallComponent , ReserveConfPopupComponent , ReservedPopupComponent , 
    UpdatedStallPopupComponent , DeleteConfPopupComponent , LogsComponent, SponsorshipLogComponent],
  entryComponents:[Kisan18StallComponent, CreateCustomePopupComponent, EmailExistsPopupComponent,
    MobileExistsPopupComponent , EmailSentPopupComponent, CompanyInfoComponent, 
    EmailConfPopupComponent, StatusConfPopupComponent, UpdateStallStatusComponent , ReserveConfPopupComponent ,
    ReservedPopupComponent,UpdatedStallPopupComponent , DeleteConfPopupComponent],
  providers: [ProposalService, SharedService],
})
export class ProposalModule { }
//platformBrowserDynamic().bootstrapModule(AppModule);

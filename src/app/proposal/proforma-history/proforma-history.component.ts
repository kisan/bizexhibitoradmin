import { Component, OnInit ,ViewChildren} from '@angular/core';
import { Router } from '@angular/router';
import { ProposalService  } from '../proposal.service';
import { SharedService  } from '../../shared/shared.service';
import { environment } from '../../../environments/environment';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-proforma-history',
  templateUrl: './proforma-history.component.html',
  styleUrls: ['./proforma-history.component.scss']
})
export class ProformaHistoryComponent implements OnInit {
  @ViewChildren('tooltip') tooltips;
  masterdata={'pavillion_types':[],"stand_types":[],"stand_locations":[]};
  offer_type="proforma invoice";
  records=[];
  issuedBy = [];
  pagesize=environment.greenCloudConfig.page_size;
  current_page=0;
  total=0;
  product_filter={"pavillion_type":"","stand_type":"","stand_location":""};
  filter={"from_name": ""};
  pages=0;
  sort_col="sent_datetime";
  sort_dir=1;
  showall=0;
  search='';
  start_item=1;
  end_item=environment.greenCloudConfig.page_size;
  is_expired="";

  constructor(private proposalService: ProposalService, private router:Router, private sharedService: SharedService , private snackBar: MatSnackBar) { }

  ngOnInit() {
   // this.offer_type='proforma';
    localStorage.setItem('offer_type', 'proforma invoice');

    this.sharedService.getMasterData().then(response => {
      // console.log('^^^^^',response);
      if(response.success)
      {
        this.masterdata=response.eventDetails;
        this.get_list();
        this.masterdata.pavillion_types.sort((a, b) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0);
      }
      else
      {
        this.snackBar.open(response.message, '', { duration: 5000, });
        document.getElementById('loader').style.display="none";
      }
    },
    err=>{
      // console.log(err.error);
      this.snackBar.open(err.error.message, '', { duration: 5000, });
      document.getElementById('loader').style.display="none";
    });
  }

  get_list(){
    document.getElementById('loader').style.display="block";
    var sort_direction="ASC";
    if(this.sort_dir==1)
    {
      sort_direction="DESC";
    }

    let ListParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode,
      "offerType": "proforma",
      "product_filter": this.product_filter,
      "pagesize":this.pagesize,
      "currentpage":this.current_page,
      "search":this.search,
      "sort_col":this.sort_col,
      "sort_dir":sort_direction,
      "filter":this.filter,
      "showall":this.showall,
      "is_expired":this.is_expired
    }
    //console.log(JSON.stringify(ListParams));
    this.proposalService.get_proposal_proforma_history(ListParams).then(res => {
      // console.log(res);
      if(res.responseCode==0 && res.success==true)
      {
          //this.isSuccess = true;
         // console.log(res);
        this.records=res.result.records;
        this.issuedBy = res.result.issuedby;
        this.total=res.result.total_count;
        this.update_stastics();
      }
      else
      {
          //this.isSuccess = false;
          // console.log(res);
          //this.message = res.message;
        this.snackBar.open(res.message, '', { duration: 5000, });
      }
      document.getElementById('loader').style.display="none";
    },
    err=>{
      // console.log(err.error);
      this.snackBar.open(err.error.message, '', { duration: 5000, });
      document.getElementById('loader').style.display="none";
    });
  }

  update_stastics() {
    if(this.showall)
    {
      this.start_item = 1;
      this.end_item = this.total;
      this.pages=1;
    }
    else
    {
      this.pages=Math.ceil(this.total/this.pagesize);
      this.start_item = ((this.current_page*this.pagesize)+1);
      this.end_item = ((this.current_page*this.pagesize)+this.pagesize);
      if( this.end_item >this.total){
        this.end_item =this.total;
      }
    }
  }

  sort_records(key) {
    if(this.sort_col != key)
    {
      this.sort_col=key;
      this.sort_dir=0;
      this.current_page = 0;
    }
    else
    {
      this.sort_dir= 1-this.sort_dir;
    }
    this.get_list();
  }

  toggle_show_all_records(){
    //this.stall_showall=1-this.stall_showall;
    this.current_page=0;
    this.get_list();
  }

  search_records(event: any) { // without type info
    if(event.keyCode == 13)
    {
      // console.log(event.target.value);
      //this.stall_search=event.target.value;
      this.current_page=0;
      this.get_list();
    }
  }
}

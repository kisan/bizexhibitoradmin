import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProposalHistoryComponent } from './proposal-history.component';

describe('ProposalHistoryComponent', () => {
  let component: ProposalHistoryComponent;
  let fixture: ComponentFixture<ProposalHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProposalHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProposalHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from 'src/environments/environment';
import { ProposalService } from '../proposal.service';
import { SharedService } from 'src/app/shared/shared.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit-company.component.html',
  styleUrls: ['./edit-company.component.scss']
})
export class EditComponent implements OnInit {
  userDetails: any;
  userDetailForm: FormGroup;
  logoImg: any;
  selectedFile: any;
  base64Url: any;
  uploadedFile: any;
  prodImagePrefix: any;
  countries={};
  states={};
  objectKeys = Object.keys;
  orgname: any;
  admin_name: string;
  timeinmilli=0;
  cities:{};
  //loading = false;
  constructor(
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private proposalService: ProposalService,
    public sharedService: SharedService,
    private router: Router
  ) { }

  ngOnInit() {
   // this.userDetails = JSON.parse(localStorage.getItem('username_details'));
    this.timeinmilli = this.sharedService.getcurrentmillies();
    let username_for_offer= localStorage.getItem('username_for_offer');
    let searchParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode,
      "type": "username",
      "username":username_for_offer
    }
    // console.log(searchParams);
    this.proposalService.search_exhibitor(searchParams).then(res => {
     //console.log(res);
      if(res.responseCode==0 && res.success==true)
      {
        this.userDetails=res.exhinfo.details;
        this.admin_name = localStorage.getItem('loggedInUserName');
        this.orgname = this.userDetails.organisation_name;
        this.prodImagePrefix = environment.greenCloudConfig.product_url_prefix;
        this.countries=Object.keys(this.sharedService.getCountries());
        this.states=this.sharedService.getStates();
        this.userForm();
        this.setcities(0);
        //document.getElementById('loader').style.display="none";
      }
      else
      {
        let snackBarRef = this.snackBar.open(res.message, '', { duration: 5000, });
        snackBarRef.afterDismissed().subscribe(() => {
          //console.log('The snack-bar was dismissed');
          //this.gstin="";
          this.router.navigate(['proposal/search-exhibitor']);

        });
      }
    },
    err=>{
      console.log(err.error);
      let snackBarRef = this.snackBar.open(err.error.message, '', { duration: 5000, });
      snackBarRef.afterDismissed().subscribe(() => {
        //console.log('The snack-bar was dismissed');
        //this.gstin="";

          this.router.navigate(['proposal/search-exhibitor']);

      });
      //document.getElementById('loader').style.display="none";
    });

  }

  userForm() {
    this.userDetailForm = this.fb.group({
      firstName: [this.userDetails.first_name, Validators.required],
      lastName: [this.userDetails.last_name, Validators.required],
      mobileNo: [this.userDetails.mobile, Validators.required],
      emailId: [this.userDetails.email, Validators.required],
      gstNo: [this.userDetails.gstin],
      address: [this.userDetails.address1, Validators.required],
      country: [this.userDetails.country, Validators.required],
      state: [this.userDetails.state, Validators.required],
      city: [this.userDetails.city, Validators.required],
      pin: [this.userDetails.pin, Validators.required],
      website: [this.userDetails.website, Validators.pattern(/^((?:http|ftp)s?:\/\/)(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|localhost|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(?::\d+)?(?:\/?|[\/?]\S+)$/i)],
      productImages:[(this.userDetails.product_images === null || this.userDetails.product_images === "" || this.userDetails.product_images === undefined)? [] : this.userDetails.product_images.split(',')],
      marktrusted:[false]
    });
    // console.log('this.userDetailForm', this.userDetailForm);

  }

  fillgstininfo()
  {
    if(this.userDetailForm.controls.gstNo.value !=undefined && this.userDetailForm.controls.gstNo.value!=null
      && this.userDetailForm.controls.gstNo.value!='' )
    {
      let searchParams = {
        "app_key": environment.greenCloudConfig.AppKey,
        "source": environment.greenCloudConfig.Source,
        "eventCode": environment.greenCloudConfig.EventCode,
        "gstIn":this.userDetailForm.controls.gstNo.value
      }
      this.proposalService.get_company_details_by_gstin(searchParams).then(res => {
        //console.log(res);
        if(res.responseCode==0 && res.success==true)
        {
          //this.alreadyEmailExistsDialog(res.data.username);
          this.orgname=res.data.companyname;
          this.userDetailForm.get('address').setValue(res.data.address);
          //this.state=res.data.state;
          this.userDetailForm.get('state').setValue(res.data.state);
          if(res.data.state=='Delhi')
          {
            this.userDetailForm.get('state').setValue("Delhi (NCT)");
          }
          if(res.data.state == 'Dadra and Nagar Haveli and Daman and Diu' && res.data.district=='Dadra & Nagar Haveli')
          {
            this.userDetailForm.get('state').setValue("Dadra and Nagar Haveli (UT)");
          }
          this.cities=this.states[this.userDetailForm.get('state').value];
          if(res.data.city!=undefined && res.data.city!='' && res.data.city!='')
          {
            //this.city=res.data.city;
            this.userDetailForm.get('city').setValue(res.data.city);
          }
          else
          {
            //this.city=res.data.district;
            this.userDetailForm.get('city').setValue(res.data.district);
          }

          //this.pin=res.data.pin;
          this.userDetailForm.get('pin').setValue(res.data.pin);
        }
        else{
          //this.activestep=3;
          let snackBarRef = this.snackBar.open(res.message, null, {
            duration: 3000,
          });
          snackBarRef.afterDismissed().subscribe(() => {
            //console.log('The snack-bar was dismissed');
            this.userDetailForm.get('gstNo').setValue("");
          });
        }
      },
      err=>{
        console.log(err.error);
        document.getElementById('loader').style.display="none";
        let snackBarRef = this.snackBar.open(err.error.message, '',
        {
        duration: 3000,
        });
        snackBarRef.afterDismissed().subscribe(() => {
          //console.log('The snack-bar was dismissed');
          this.userDetailForm.get('gstNo').setValue("");
        });
      });
    }
  }
  setcities(resetcity)
  {
    //console.log(this.userDetailForm.get('state').value);
    this.cities=this.states[this.userDetailForm.get('state').value];
    if(resetcity==1)
    {
      this.userDetailForm.get('city').setValue('');
    }
  }
  uploadOrgLogo(event) {
    if (event.target.files && event.target.files[0]) {
      this.selectedFile = event.target.files[0];
      if(this.selectedFile.size < 10000000 && this.selectedFile.name.endsWith(".jpg") || this.selectedFile.name.endsWith(".JPG")
      || this.selectedFile.name.endsWith(".png") || this.selectedFile.name.endsWith(".PNG")
      || this.selectedFile.name.endsWith(".jpeg") || this.selectedFile.name.endsWith(".JPEG"))
      { var reader = new FileReader();
          reader.onload = (e: any) => {
          this.base64Url = e.target.result;
        }
        reader.readAsDataURL(event.target.files[0]);

      }else{
        event.target.value = null;
        this.base64Url=null
        this.snackBar.open('Invalid image file format', '',
        {
          duration: 3000,
        });
        if(this.selectedFile.size > 10000000) {

          this.snackBar.open('Upload image with size less than 10mb', '',
          {
            duration: 3000,
          });
        }

      }
    }
  }
  uploadProductImages(event) {
    if (event.target.files && event.target.files[0]) {
      this.uploadedFile = event.target.files[0];
      document.getElementById('loader').style.display="block";
      if(this.uploadedFile.size < 10000000 && this.uploadedFile.name.endsWith(".jpg") || this.uploadedFile.name.endsWith(".JPG")
      || this.uploadedFile.name.endsWith(".png") || this.uploadedFile.name.endsWith(".PNG")
      || this.uploadedFile.name.endsWith(".jpeg") || this.uploadedFile.name.endsWith(".JPEG"))
      {
        if(this.userDetailForm.value.productImages.length <= 4) {
          //this.loading = true;
          this.proposalService.uploadProductImages(this.uploadedFile)
          .subscribe(res => {
            if (res['success']===true) {
              this.userDetailForm.value.productImages.push(res['imageDetails']['imgUrl']);
              //this.loading = false;
              document.getElementById('loader').style.display="none";
            }
            else{
              //this.activestep=3;
              this.snackBar.open(res['message'], null, {
                duration: 3000,
              });
            }
          }, err => {
            console.log('error', err);
            //this.loading = false;
            this.snackBar.open(err.error.message, '',
            {
            duration: 5000,
            });
            document.getElementById('loader').style.display="none";
          }
          );
        }
      }
      else{
        this.snackBar.open('Invalid file', '',
        {
        duration: 5000,
        });
        event.target.value= null;
        document.getElementById('loader').style.display="none";
      }
    }
  }
  removeProductImage(idx: number) {
    if(idx != -1) {
      this.userDetailForm.value.productImages.splice(idx, 1);
    }
  }

  submitForm(form: FormGroup) {
    if(form.valid) {
      document.getElementById('loader').style.display="block";
      let params = {
        "app_key": environment.greenCloudConfig.AppKey,
        "source": environment.greenCloudConfig.Source,
        "eventCode": environment.greenCloudConfig.EventCode,
        "sessionId": localStorage.getItem('sessionId'),
        "orgname": this.orgname,
        "username": this.userDetails.username,
        "firstname": this.userDetailForm.value.firstName,
        "lastname": this.userDetailForm.value.lastName,
        "forceemailchange": 0 ,
        "gstin": this.userDetailForm.value.gstNo,
        "country": this.userDetailForm.value.country,
        "state": this.userDetailForm.value.state,
        "city": this.userDetailForm.value.city,
        "address": this.userDetailForm.value.address,
        "pin": this.userDetailForm.value.pin,
        "website": this.userDetailForm.value.website,
        "product_images": this.userDetailForm.value.productImages.toString(),
        "mark_trusted": this.userDetailForm.value.marktrusted
      }
     //console.log(JSON.stringify(params));
      // document.getElementById('loader').style.display="block";
      this.proposalService.update_exhibitor(params).subscribe(res => {

        if (res['responseCode']==0 && res['success']==true) {

          if(this.base64Url) {
            this.proposalService.uploadExhibitorLogo(this.selectedFile, this.userDetails.username, this.admin_name)
            // res.exhinfo.username
            .subscribe(uploadRes => {
             // console.log('uploadRes', uploadRes);
              if(uploadRes['response_code']==0 && uploadRes['success']) {
                this.router.navigate(['proposal/create-offer']);
              } else {
                if(uploadRes['response_code']==101) {
                  this.snackBar.open(uploadRes['message'], '', {
                    duration: 3000,
                  });
                  document.getElementById('loader').style.display="none";
                }
              }
            }, (err) => {
              this.snackBar.open('Logo upload failed', '', {
                duration: 3000,
              });
              document.getElementById('loader').style.display="none";
            }
            );
          } else {
            this.router.navigate(['proposal/create-offer']);
          }

        } else{
          //this.activestep=3;
          this.snackBar.open(res['message'], '', {
            duration: 3000,
          });
        }
        // document.getElementById('loader').style.display="none";

      }, err=>{
        console.log(err.error);
        // document.getElementById('loader').style.display="none";
        this.snackBar.open(err.error.message, '',
        {
         duration: 3000,
        });
      }

      )

    }
  }

}

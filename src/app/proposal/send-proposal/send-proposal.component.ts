import { Component, OnInit ,ViewChild , Inject} from '@angular/core';
import { Router , ActivatedRoute} from '@angular/router';
import { ProposalService  } from '../proposal.service';
import { SharedService  } from '../../shared/shared.service';
import { environment } from '../../../environments/environment';
import { MatSnackBar } from '@angular/material/snack-bar';
//import * as ClassicEditor from '@ckeditor/ckeditor5-angular';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-send-proposal',
  templateUrl: './send-proposal.component.html',
  styleUrls: ['./send-proposal.component.scss']
})
export class SendProposalComponent implements OnInit {
 // username_for_offer:string='';
  offer_type="";
  html="";
  offer_info:any; 
  local_offer:any;
  toEmail="";
  ccEmails="";
  subject="";
  pdf_name="";
  pdf_url="";
  offer_id=0;
  notification_id=0;
  send_email=1;
  is_products=true;
  //isValidEmails=true;
  send_pdf_attachment=1;
 
  constructor(public dialog: MatDialog,private proposalService: ProposalService, private router:Router, 
    private sharedService: SharedService , private snackBar: MatSnackBar , private route: ActivatedRoute) { }

  ngOnInit() {
    
   /* this.offer_type=localStorage.getItem('offer_type');
    if(this.offer_type==undefined || this.offer_type==null)
    {
      this.offer_type='proforma';
      localStorage.setItem('offer_type', 'proforma invoice');
    }*/
    // this.username_for_offer= localStorage.getItem('username_for_offer');
    // this.local_offer=JSON.parse(localStorage.getItem('offer_info'));
    this.route.queryParams.subscribe(params => {
      this.offer_id = params['offer_id'];
      if(this.offer_id==undefined || this.offer_id==null)
      {
        this.offer_id =0;
        this.notification_id = params['notification_id']; 
        if(this.notification_id==undefined || this.notification_id==null)
        { 
          
        }
        else
        {
          this.getDetailsByNotificationId();
        }
      }
      else
      {
        this.getDetailsByOfferId();
      }
    });
   
  }

  getDetailsByOfferId()
  {
    let params = {
      "app_key": environment.greenCloudConfig.AppKey, "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode, "sessionId": localStorage.getItem('sessionId'),
      "offerId":this.offer_id
    }
    //console.log(JSON.stringify(params));
    this.proposalService.get_offer_details(params).then(res => { 
      console.log(res);
      if(res.responseCode==0 && res.success==true)
      {
        this.html=res.data.draft_email_body;
        this.toEmail=res.data.details.email;
        this.subject=res.data.draft_email_subject;
        this.pdf_name=res.data.details.pdf_url;
        this.pdf_url=res.data.details.pdf_path;
        this.offer_type=res.data.details.offer_type;
        if(res.data.products.length > 0)
        {
          this.is_products=true;
        }
        else
        {
          this.is_products=false;
        }
        if(this.offer_type=='proforma')
        {
          this.offer_type='proforma invoice';       
        }
        localStorage.setItem('offer_type', this.offer_type);
      } 
      else
      {
        this.snackBar.open(res.message, '', { duration: 5000, });
      }      
      document.getElementById('loader').style.display="none"; 
    },
    err=>{ 
      console.log(err.error);  
      this.snackBar.open(err.error.message, '', { duration: 5000, });
      document.getElementById('loader').style.display="none"; 
    }); 
  }

  getDetailsByNotificationId()
  {
    let params = {
      "app_key": environment.greenCloudConfig.AppKey, "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode, "sessionId": localStorage.getItem('sessionId'),
      "id":this.notification_id
    }
    //console.log(JSON.stringify(params));
    this.proposalService.get_notification_details(params).then(res => { 
      console.log(res);
      if(res.responseCode==0 && res.success==true)
      {
        this.html=res.data.draft_email_body;
        this.toEmail=res.data.notification_details.to_emails;
        this.subject=res.data.draft_email_subject;
        this.pdf_name=res.data.details.pdf_url;
        this.pdf_url=res.data.details.pdf_path;
        this.offer_id=res.data.details.id;
        this.ccEmails=res.data.notification_details.cc_emails;
        this.offer_type=res.data.details.offer_type;
        if(this.offer_type=='proforma')
        {
          this.offer_type='proforma invoice';       
        }
        if(res.data.products.length > 0)
        {
          this.is_products=true;
        }
        else
        {
          this.is_products=false;
        }
        localStorage.setItem('offer_type', this.offer_type);
      } 
      else
      {
        this.snackBar.open(res.message, '', { duration: 5000, });
      }      
      document.getElementById('loader').style.display="none"; 
    },
    err=>{ 
      console.log(err.error);  
      this.snackBar.open(err.error.message, '', { duration: 5000, });
      document.getElementById('loader').style.display="none"; 
    }); 
  }

  validateEmails()
  {
    let isValidEmails=true;
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
   
    if(this.toEmail!=undefined && this.toEmail!=null && this.toEmail!="" )
    {
      if(!re.test(String(this.toEmail).toLowerCase()))
      {
        isValidEmails=false;
      }
    }
   
    if(this.ccEmails!=undefined && this.ccEmails!=null && this.ccEmails!="")
    {
      let emails= this.ccEmails.split(",");
      for (let email of emails) {
        if(!re.test(String(email).toLowerCase()))
        {
          isValidEmails=false;
        }
      }
    }
    //console.log(isValidEmails);
    return isValidEmails;
  }

  onSubmit(form) {
    //console.log('form submitted');
    //console.log(form);
    if(form.valid && this.validateEmails())
    {
      if(this.send_email==0)
      {
        this.openReserveDialog();
      }
      else
      {
        this.openConfeDialog();
      }
    }
  }

  sendoffer()
  {
    document.getElementById('loader').style.display="block";
    let params = {
      "app_key": environment.greenCloudConfig.AppKey, "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode, "sessionId": localStorage.getItem('sessionId'),
      "offerId":this.offer_id , "toEmails":this.toEmail , "subject":this.subject , "body":this.html 
      ,"send_email":this.send_email ,"send_pdf_attachment":this.send_pdf_attachment ? 1 : 0};
    if(this.ccEmails!=undefined && this.ccEmails!=null && this.ccEmails!="")
    {
      params['ccEmails']=this.ccEmails;
    }
    //console.log(JSON.stringify(params));
    this.proposalService.send_offer(params).then(res => { 
     // console.log(res);
      if(res.responseCode==0 && res.success==true)
      {
        if(this.send_email==0)
        {
          this.openReservedDialog();
        }
        else
        {
          this.openSaveDialog();
        }
        
        //document.getElementById('loader').style.display="none";
      }
      else{
        //this.activestep=3;
        this.snackBar.open(res.message, null, {
          duration: 3000,
        });           
      }
      document.getElementById('loader').style.display="none";
    },
    err=>{ 
      console.log(err.error);
      document.getElementById('loader').style.display="none";  
      this.snackBar.open(err.error.message, '', 
      {
       duration: 3000,
      });
    });
  }

  openSaveDialog() {
      let dialogRef = this.dialog.open(EmailSentPopupComponent);
      dialogRef.afterClosed().subscribe(value => {
        //console.log(`Dialog sent: ${value}`); 
        //this.router.navigateByUrl('/proposal/search-exhibitor?offer_type='+this.offer_type);
        /*if(this.offer_type=='proforma invoice')
        {
          this.router.navigateByUrl('/proposal/proforma-history');
        }
        if(this.offer_type=='proposal')
        {
          this.router.navigateByUrl('/proposal/proposal-history');
        }*/
        this.router.navigateByUrl('/proposal/logs');
      });
  }

  openConfeDialog() {
      let dialogRef = this.dialog.open(EmailConfPopupComponent, {
        panelClass: 'email_conf',
        disableClose: true,
        data : {"offer_type":this.offer_type,"send_pdf_attachment":this.send_pdf_attachment}
      });
      dialogRef.afterClosed().subscribe(value => {
        //console.log(value);
        if(value==true)
        {
          this.sendoffer();
        }
      });
  }

  openReserveDialog() {
    let dialogRef = this.dialog.open(ReserveConfPopupComponent, {
      panelClass: 'email_conf',
      disableClose: true,
      data : {"offer_type":this.offer_type,"send_email":this.send_email}
    });
    dialogRef.afterClosed().subscribe(value => {
      //console.log(value);
      if(value==true)
      {
        this.sendoffer();
      }
    });
  }

  openReservedDialog() {
    let dialogRef = this.dialog.open(ReservedPopupComponent);
    dialogRef.afterClosed().subscribe(value => {
      //console.log(`Dialog sent: ${value}`); 
      //this.router.navigateByUrl('/proposal/search-exhibitor?offer_type='+this.offer_type);
      /*if(this.offer_type=='proforma invoice')
      {
        this.router.navigateByUrl('/proposal/proforma-history');
      }
      if(this.offer_type=='proposal')
      {
        this.router.navigateByUrl('/proposal/proposal-history');
      }*/
      this.router.navigateByUrl('/proposal/logs');
    });
  }

  // change_send_pdf_attachmet()
  // {
  //   console.log(this.send_pdf_attachment);
  // }
  
}



/*email sent popup */
@Component({
  selector: 'app-email-sent-popup',
  template: `
<div class="errorMsgPopup">
  <mat-dialog-actions class="pad-all-sm">
    <button mat-dialog-close class="closeBtn"> <i class="material-icons">close</i></button>
  </mat-dialog-actions>
  <mat-dialog-content>
    <mat-list>
      <mat-list-item>
        <mat-icon mat-list-icon><i class="material-icons">check</i></mat-icon>
        <h4 mat-line class="font-bold-six" style="padding-top: 10px;">Email has been sent Successfully.</h4>
      </mat-list-item>
    </mat-list>
    <mat-list-item *ngIf="offer_type=='proforma invoice'">
      The email with an invoice attached to it, has been sent to the mentioned email addresses.
    </mat-list-item>
    <mat-list-item *ngIf="offer_type=='proposal'">
      The email  has been sent to the mentioned email addresses.
    </mat-list-item>
  </mat-dialog-content>
</div>
`
})
export class EmailSentPopupComponent implements OnInit {
  offer_type="";
  constructor(public dialogRef: MatDialogRef<EmailSentPopupComponent>, public dialog: MatDialog) { }
  ngOnInit() {
    this.offer_type=localStorage.getItem('offer_type');
    if(this.offer_type==undefined || this.offer_type==null)
    {
      this.offer_type='proforma';
      localStorage.setItem('offer_type', 'proforma invoice');
    }
  }
}


/*email conf popup */
@Component({
  providers: [SendProposalComponent],
  selector: 'app-confirm-popup',
  template: `
  <div class="confirmMsgPopup" style>
  
    <h4 class="mrgn-b-md mrgn-l-md mrgn-r-md">Confirmation</h4>
    <p class="mrgn-b-none mrgn-l-md mrgn-r-md">Are you sure you want to reserve stand and send this <span *ngIf="data.send_pdf_attachment==1"> {{data.offer_type}}</span><span *ngIf="data.send_pdf_attachment==0">email</span>?</p>
  
  <div class="footer-btn">
  <mat-card-footer>
    <button type="button" style="background:#B8B8B8;" mat-raised-button mat-dialog-close class="grayBtn" (click)="dialogRef.close(false)">Cancel</button>
    <button type="button" mat-raised-button class="greennewBtn" (click)="dialogRef.close(true)">Yes, Send</button>
    </mat-card-footer>
  </div>
  </div>
  `
})
export class EmailConfPopupComponent implements OnInit {
  //offer_type="";
  constructor(public dialogRef: MatDialogRef<EmailConfPopupComponent>, public dialog: MatDialog ,  @Inject(MAT_DIALOG_DATA) public data: any,) { }
  ngOnInit() {
  
  }
}

/*reserve conf popup */
@Component({
  providers: [SendProposalComponent],
  selector: 'app-reserve-confirm-popup',
  template: `
  <div class="confirmMsgPopup" style>
  
    <h4 class="mrgn-b-md mrgn-l-md mrgn-r-md">Confirmation</h4>
    <p class="mrgn-b-none mrgn-l-md mrgn-r-md">Are you sure you want to reserve stand without sending this {{data.offer_type}}?</p>
  
  <div class="footer-btn">
  <mat-card-footer>
    <button type="button" style="background:#B8B8B8;" mat-raised-button mat-dialog-close class="grayBtn" (click)="dialogRef.close(false)">Cancel</button>
    <button type="button" mat-raised-button class="greennewBtn" (click)="dialogRef.close(true)">
    Yes,<span *ngIf="data.send_email==1"> Send </span><span *ngIf="data.send_email==0"> Reserve </span></button>
    </mat-card-footer>
  </div>
  </div>
  `
})
export class ReserveConfPopupComponent implements OnInit {
  //offer_type="";
  constructor(public dialogRef: MatDialogRef<ReserveConfPopupComponent>, 
    public dialog: MatDialog ,  @Inject(MAT_DIALOG_DATA) public data: any,
    ) { }
  ngOnInit() {
  
  }
}

/*reserved popup */
@Component({
  selector: 'app-reserved-popup',
  template: `
<div class="errorMsgPopup">
  <mat-dialog-actions class="pad-all-sm">
    <button mat-dialog-close class="closeBtn"> <i class="material-icons">close</i></button>
  </mat-dialog-actions>
  <mat-dialog-content>
    <mat-list>
      <mat-list-item>
        <mat-icon mat-list-icon><i class="material-icons">check</i></mat-icon>
        <h4 mat-line class="font-bold-six" style="padding-top: 10px;">Stand Reserved Successfully.</h4>
      </mat-list-item>
    </mat-list>  
  </mat-dialog-content>
</div>
`
})
export class ReservedPopupComponent implements OnInit {
  offer_type="";
  constructor(public dialogRef: MatDialogRef<ReservedPopupComponent>, public dialog: MatDialog) { }
  ngOnInit() {
    this.offer_type=localStorage.getItem('offer_type');
    if(this.offer_type==undefined || this.offer_type==null)
    {
      this.offer_type='proforma';
      localStorage.setItem('offer_type', 'proforma invoice');
    }
  }
}


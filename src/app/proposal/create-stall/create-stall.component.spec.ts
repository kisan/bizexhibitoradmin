import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateStallComponent } from './create-stall.component';

describe('CreateStallComponent', () => {
  let component: CreateStallComponent;
  let fixture: ComponentFixture<CreateStallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateStallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateStallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

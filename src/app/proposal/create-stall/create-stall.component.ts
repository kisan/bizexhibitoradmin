import { Component, OnInit } from '@angular/core';
import { Inject } from '@angular/core';
import { Router , ActivatedRoute } from '@angular/router';
import { any } from 'underscore';
import { ProposalService  } from '../proposal.service';
import { SharedService  } from '../../shared/shared.service';
import { environment } from '../../../environments/environment';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-create-stall',
  templateUrl: './create-stall.component.html',
  styleUrls: ['./create-stall.component.scss']
})
export class CreateStallComponent implements OnInit {
  errorMessage="";
  
  masterdata={'pavillion_types':[],"stand_types":[],"stand_locations":[]};
  stall_info = {'event_code':environment.greenCloudConfig.EventCode,'sku':'','pavillion_type':'',
    'stand_type':'','stand_location':'','area':'','stand_number':'','price':'','image_url':'','id':'','status':'available'};

  prev_stand_image="";
  maxFileSize = environment.greenCloudConfig.ImagesMaxFileSizeInMB;
  action='add';
  old_stand_number='';
  constructor(private proposalService: ProposalService, private router:Router, 
    private sharedService: SharedService ,public dialog: MatDialog, private route: ActivatedRoute,) { }
  

  ngOnInit() {
    localStorage.setItem('offer_type', '');
    this.sharedService.getMasterData().then(response => {           
      //console.log(response);
      if(response.success)
      {
        this.masterdata=response.eventDetails; 
        this.masterdata.pavillion_types.sort((a, b) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0);
        this.route.queryParams.subscribe(params => {
          if(params['id'] && params['id']!='')
          {
            //this.action='edit';
            document.getElementById('loader').style.display="block";
            let params2 = {
              "app_key": environment.greenCloudConfig.AppKey, 
              "source": environment.greenCloudConfig.Source,
              "eventCode": environment.greenCloudConfig.EventCode, 
              "sessionId": localStorage.getItem('sessionId'),
              "stallid":params['id']
            }
            this.proposalService.get_stand_details(params2).then(
              res => {
                //console.log(res);
                if(res['success'] && res['stallinfo'] && res['stallinfo']['id'])
                {
                  this.stall_info.id=res['stallinfo']['id'];
                  this.stall_info.event_code=res['stallinfo']['event_code'];
                  this.stall_info.sku=res['stallinfo']['sku'];
                  this.stall_info.pavillion_type=res['stallinfo']['pavillion_type'];
                  this.stall_info.stand_type=res['stallinfo']['stand_type'];
                  this.stall_info.stand_location=res['stallinfo']['stand_location'];
                  this.stall_info.area=res['stallinfo']['area'];
                  this.stall_info.stand_number=res['stallinfo']['stand_number'];
                  this.stall_info.price=res['stallinfo']['price'];
                  this.stall_info.image_url=res['stallinfo']['image_url'];
                  this.stall_info.status=res['stallinfo']['status'];
                  //console.log(this.stall_info);
                  this.action='edit';
                  this.old_stand_number=res['stallinfo']['stand_number'];
                }
                document.getElementById('loader').style.display="none";
         
              },
              err=>{
                console.log(err.error);
                document.getElementById('loader').style.display="none";
              }
            );
          }
        });
      }
   });
  }

  
  uploadStandImage(event) {
    if (event.target.files && event.target.files[0]) {
      document.getElementById('loader').style.display="block";
      let file = event.target.files[0];
      let fileSize = file.size / 1024 / 1024;
      if(fileSize > this.maxFileSize)
      {
        event.target.value= null;
        document.getElementById('loader').style.display="none";
        this.errorMessage="File size should not be greater than " + this.maxFileSize + "MB";
      }
      else
      {
        if( file.name.endsWith(".jpg") || file.name.endsWith(".JPG") || file.name.endsWith(".png") 
        || file.name.endsWith(".PNG")|| file.name.endsWith(".jpeg") || file.name.endsWith(".JPEG")
        || file.name.endsWith(".gif") || file.name.endsWith(".GIF"))
        {
          this.proposalService.uploadStandImage(file)
          .subscribe(res => {
            //console.log(res)
            if (res['success']===true) {
              this.errorMessage="";
              this.stall_info.image_url=res['imageDetails']['imgUrl'];
              // this.prodImagePrefix = environment.greenCloudConfig.product_url_prefix;
              // this.prodImgArr.push(res['imageDetails']['imgUrl']);
            }
            else{
              //this.activestep=3;
              // this.snackBar.open(res['message'], null, {
              //   duration: 3000,
              // });
              this.stall_info.image_url=this.prev_stand_image;
              this.errorMessage=res['message'];
            }
            document.getElementById('loader').style.display="none";
          }, err => {
            console.log('error', err);
            // this.snackBar.open(err.error.message, '',
            // {
            // duration: 5000,
            // });
            event.target.value= null;
            this.errorMessage=err.error.message;
            document.getElementById('loader').style.display="none";
          }
          );
        }
        else
        {
          // this.snackBar.open('Invalid file', '',
          // {
          // duration: 5000,
          // });
          event.target.value= null;
          document.getElementById('loader').style.display="none";
          this.errorMessage="File must be .png,.jpg,.jpeg,.gif";
        }
      }
    }
  }

  onSubmit(form) {
    //console.log('form submitted');
    //console.log(form);
    if(this.validateinfo(form) )
    {
      //console.log('submitted');
      document.getElementById('loader').style.display="block";
      let params = {
        "app_key": environment.greenCloudConfig.AppKey, 
        "source": environment.greenCloudConfig.Source,
        "eventCode": environment.greenCloudConfig.EventCode, 
        "sessionId": localStorage.getItem('sessionId'),
        "stall_info":this.stall_info
      }
      if(this.action=='add')
      {
        this.proposalService.addStall(params).then(
          res => {
            //console.log(res);
            let dialogRef = this.dialog.open(UpdatedStallPopupComponent , {data : {"action":this.action}});
            dialogRef.afterClosed().subscribe(value => {
              //document.getElementById('loader').style.display="none";
              this.router.navigateByUrl('/proposal/stands?sort_by=created_datetime');
            });
          },
          err=>{
            console.log(err.error);
            document.getElementById('loader').style.display="none";
          }
        );
      }
      if(this.action=='edit')
      {
        this.proposalService.editStall(params).then(
          res => {
            //console.log(res);
            let dialogRef = this.dialog.open(UpdatedStallPopupComponent , {data : {"action":this.action}});
            dialogRef.afterClosed().subscribe(value => {
              //document.getElementById('loader').style.display="none";
              this.router.navigateByUrl('/proposal/stands');
            });
          },
          err=>{
            console.log(err.error);
            document.getElementById('loader').style.display="none";
          }
        );
      }
    }
  }

  setSKU()
  {
    this.stall_info.sku='';
    if(this.stall_info.pavillion_type!="" && this.stall_info.stand_type!="" && 
      this.stall_info.stand_location!="" && this.stall_info.area!="" && this.stall_info.area!=null 
      && this.stall_info.area!=undefined && !isNaN(parseInt(this.stall_info.area)) && 
      this.stall_info.stand_number!=null && this.stall_info.stand_number!="" && 
      this.stall_info.stand_number!=undefined )
    {
      this.stall_info.sku=this.masterdata['details']['sku_exh_stall_prefix'] + 
        this.stall_info.pavillion_type + this.stall_info.stand_type + 
        this.stall_info.stand_location + this.stall_info.area + '_' + 
        this.stall_info.stand_number;
    }
  }

  check_stall_by_stand_number()
  {
    if (this.stall_info.stand_number != '' && this.stall_info.stand_number!=this.old_stand_number)
    {
      this.errorMessage='';
      let params = {
        "app_key": environment.greenCloudConfig.AppKey,
        "source": environment.greenCloudConfig.Source,
        "eventCode": environment.greenCloudConfig.EventCode,
        "stand_number": this.stall_info.stand_number
      }
      this.proposalService.get_stall_by_stand_number(params).then(
        res => {
          //console.log(res);
          if(res.responseCode==0 && res.success==true)
          {
            this.stall_info.sku='';
            this.errorMessage='Duplicate Stand Number. It seems we already have stand with this number.';
          }
          else
          {
            this.errorMessage='';
            this.setSKU();
          }
        },
        error=>{
          this.stall_info.sku='';
          this.errorMessage='Error in checking stand number availability.';
      });
    }
    else
    {
      this.errorMessage='';
      this.setSKU();
    }
  }

  validateinfo(form){
    if(form.valid && this.stall_info.image_url !='' )
    {
      return true;
    }
    else{
      return false;
    }
  }

  pricekeyup()
  {
    (!((/^[0-9.��]*$/i).test(this.stall_info.price))) ? this.stall_info.price = this.stall_info.price.replace(/[^0-9.��]/ig,'') : null;
  }



}

@Component({
  selector: 'app-updatestall-popup',
  template: `
<div class="errorMsgPopup">
  <mat-dialog-actions class="pad-all-sm">
    <button mat-dialog-close class="closeBtn"> <i class="material-icons">close</i></button>
  </mat-dialog-actions>
  <mat-dialog-content>
    <mat-list>
      <mat-list-item>
        <mat-icon mat-list-icon><i class="material-icons">check</i></mat-icon>
        <h4 *ngIf="data.action=='add'" mat-line class="font-bold-six" style="padding-top: 10px;">Stand Added Successfully.</h4>
        <h4 *ngIf="data.action=='edit'" mat-line class="font-bold-six" style="padding-top: 10px;">Stand Updated Successfully.</h4>
      </mat-list-item>
    </mat-list>  
  </mat-dialog-content>
</div>
`
})
export class UpdatedStallPopupComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<UpdatedStallPopupComponent>, 
    public dialog: MatDialog ,  @Inject(MAT_DIALOG_DATA) public data: any,) { }
  ngOnInit() {}
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Kisan18StallComponent } from './kisan18-stall.component';

describe('Kisan18StallComponent', () => {
  let component: Kisan18StallComponent;
  let fixture: ComponentFixture<Kisan18StallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Kisan18StallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Kisan18StallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

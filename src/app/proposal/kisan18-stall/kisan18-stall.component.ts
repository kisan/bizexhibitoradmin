import { Component, OnInit , Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute , Router ,} from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { SharedService  } from '../../shared/shared.service';

@Component({
  selector: 'app-kisan18-stall',
  templateUrl: './kisan18-stall.component.html',
  styleUrls: ['./kisan18-stall.component.scss']
})
export class Kisan18StallComponent implements OnInit {

  stall:any;
  selectedIndex =0;
  
  constructor(public dialog: MatDialog,private router:Router, @Inject(MAT_DIALOG_DATA) public data: any,
  private sanitizer: DomSanitizer, private sharedService: SharedService) { }

  
  ngOnInit() { 
    //console.log('this.data', this.data);
    console.log(this.data);
    this.stall=this.data[this.selectedIndex];
  }

  getRupeeFormat(amount)
  {
    return this.sharedService.getRupeeFormat(amount);
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchExhibitorComponent } from './search-exhibitor/search-exhibitor.component';
import { GenerateProposalComponent } from './generate-proposal/generate-proposal.component';
import { SendProposalComponent } from './send-proposal/send-proposal.component';
import { ProposalHistoryComponent } from './proposal-history/proposal-history.component';
import { CreateExhibitorComponent } from './create-exhibitor/create-exhibitor.component';
import { ProformaPreviewComponent } from './proforma-preview/proforma-preview.component';
import { ProposalDashboardComponent } from './dashboard/dashboard.component';
import { ProformaHistoryComponent } from './proforma-history/proforma-history.component';
import { EditComponent } from './edit-company-info/edit-company.component';
import { StallStatusComponent } from './stall-status/stall-status.component';
import { CreateStallComponent } from './create-stall/create-stall.component';
import { LogsComponent } from './logs/logs.component';
import { SponsorshipLogComponent } from './sponsorship-log/sponsorship-log.component';

const routes: Routes = [
  {path: 'search-exhibitor', component: SearchExhibitorComponent},
  {path: 'create-offer', component: GenerateProposalComponent},
  {path: 'create-exhibitor', component: CreateExhibitorComponent},
  {path: 'proposal-history', component: ProposalHistoryComponent},
  {path: 'proforma-history', component: ProformaHistoryComponent},
  {path: 'logs', component: LogsComponent},
  {path: 'send-offer', component: SendProposalComponent},
  {path: 'preview-offer', component: ProformaPreviewComponent},
  {path: 'dashboard', component: ProposalDashboardComponent},
  {path: 'edit-company-info', component: EditComponent},
  {path: 'stands', component: StallStatusComponent},
  {path: 'create-stand', component: CreateStallComponent},
  {path: 'sponsorship-log', component: SponsorshipLogComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProposalRoutingModule { }

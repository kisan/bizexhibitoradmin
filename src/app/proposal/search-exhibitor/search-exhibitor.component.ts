import { Component, OnInit  } from '@angular/core';
import { Router ,ActivatedRoute } from '@angular/router';
import { ProposalService  } from '../proposal.service';
import { SharedService  } from '../../shared/shared.service';
import { environment } from '../../../environments/environment';
import { NgModel } from '@angular/forms';

@Component({
  selector: 'app-search-exhibitor',
  templateUrl: './search-exhibitor.component.html',
  styleUrls: ['./search-exhibitor.component.scss']
})
export class SearchExhibitorComponent implements OnInit {

  errorMessage : string='';
  showEmailDoesNotExists: boolean=false;
  showOrgNameDoesNotExists: boolean=false;
  showMultipleAccountsExists: boolean=false;
  searchType : string='email';
  searchEmail : string='';
  searchOrgName : string='';
  showEnter: boolean=true;
  showClose: boolean=false;
  showFound: boolean=false;
  selectedUsername:string='';
  profiles:any;
  searchText:string='';
  offer_type="";
  userDetails: any;

  constructor(private proposalService: ProposalService, private router:Router, private sharedService: SharedService,private route: ActivatedRoute) { }

  ngOnInit() {

    //this.offer_type=localStorage.getItem('offer_type');

    this.route.queryParams.subscribe(params => {
      //console.log(params);
      this.offer_type = params['offer_type'];
      if(this.offer_type==undefined || this.offer_type==null)
      {
        this.offer_type='proforma invoice';
      }
      localStorage.setItem('offer_type', this.offer_type);
      let gmailId=localStorage.getItem('gmailId');
      if(gmailId==undefined || gmailId == null || gmailId=='')
      {
        this.router.navigateByUrl('/profile/gmail-setting?returnUrl='+this.sharedService.getBase64URIencode('proposal/search-exhibitor?offer_type='+this.offer_type));
      }
    });

  }

  searchExhibitor(){
    document.getElementById('loader').style.display="block";
    let searchParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode
    }
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var mobilefilter = /^([0-9]{10})$/;
    if(re.test(String(this.searchText.trim()).toLowerCase()))
    {
      this.searchType='email';
    }
    else
    {
      if(mobilefilter.test(String(this.searchText.trim()) ))
      {
        this.searchType='mobile';
      }
      else
      {
        this.searchType='organisation_name';
      }
    }
    searchParams['type']=this.searchType;
    if(this.searchType=='email')
    {
      searchParams['email']= this.searchText.trim();
    }
    if(this.searchType=='mobile')
    {
      searchParams['mobile']= '+91'+this.searchText.trim();
    }
    if(this.searchType=='organisation_name')
    {
      searchParams['organisation_name']= this.searchText.trim();
    }
    //console.log(JSON.stringify(searchParams));
    this.proposalService.search_exhibitor(searchParams).then(res => {
      //console.log(res);
      if(res.responseCode==0 && res.success==true)
      {
        //console.log(res);
        this.showClose=false;
        this.showEnter=false;
        this.showFound=true;
        if("profiles" in res)
        {
          this.showEmailDoesNotExists=false;
          this.showOrgNameDoesNotExists=false;
          this.showMultipleAccountsExists=true;
          this.profiles=res.profiles;
        }
        else
        {
          this.showEmailDoesNotExists=false;
          this.showOrgNameDoesNotExists=false;
          this.showMultipleAccountsExists=false;
          this.profiles=null;
          this.selectedUsername=res.exhinfo.details.username;
          this.userDetails=res.exhinfo.details;
          setTimeout(()=>{
                this.redirecttocreate();
          }, 500);
        }
      }
      else
      {
        if(res.responseCode==161)
        {
          this.showClose=true;
          this.showEnter=false;
          this.showFound=false;
          if(this.searchType=='email')
          {
            this.showEmailDoesNotExists=true;
            this.showOrgNameDoesNotExists=false;
            this.showMultipleAccountsExists=false;
          }
          if(this.searchType=='organisation_name')
          {
            this.showEmailDoesNotExists=false;
            this.showOrgNameDoesNotExists=true;
            this.showMultipleAccountsExists=false;
          }
        }
        else
        {
          this.errorMessage = res.message;
        }
      }
      document.getElementById('loader').style.display="none";
    });
  }

  redirecttocreate(userDetails?)
  {
    localStorage.setItem('username_for_offer', this.selectedUsername);
    //userDetails === undefined? localStorage.setItem('username_details', JSON.stringify(this.userDetails)) : localStorage.setItem('username_details', JSON.stringify(userDetails));
    //alert(this.selectedUsername);
    this.router.navigate(['proposal/edit-company-info']);
  }

  onChange(event: string, oldVal: NgModel) {                        // This function is used in html file
    if(event.charCodeAt(0) === 32 || event.charCodeAt(0) === NaN) {
      oldVal.control.patchValue(oldVal.control.value.trim());
    }
  }

}

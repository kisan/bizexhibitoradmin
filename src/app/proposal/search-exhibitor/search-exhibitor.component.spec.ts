import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchExhibitorComponent } from './search-exhibitor.component';

describe('SearchExhibitorComponent', () => {
  let component: SearchExhibitorComponent;
  let fixture: ComponentFixture<SearchExhibitorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchExhibitorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchExhibitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

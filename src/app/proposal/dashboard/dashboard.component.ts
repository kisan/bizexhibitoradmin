import { Component, OnInit } from '@angular/core';
import { Router , ActivatedRoute } from '@angular/router';
import { ProposalService  } from '../proposal.service';
import { SharedService  } from '../../shared/shared.service';
import { environment } from '../../../environments/environment';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-proposal-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class ProposalDashboardComponent implements OnInit {
  comboChart = {
    chartType: 'ComboChart',
    dataTable: [ ],
    options: {
      title : 'Proposals sent vs products sold',
      vAxis: {title: 'count'},
      hAxis: {title: 'weeks'},
      seriesType: 'bars',
      series: {1: {type: 'line'}},
      height:300,
      colors: ["#D8F5E5","#008940"],
      is3D: true,
      chartArea:{left:60,top:40,right:85,width:"83%",height:"64%"}
    }
  };
  countsummary:any
  constructor(private proposalService: ProposalService, private router:Router, 
    private sharedService: SharedService , private snackBar: MatSnackBar , private route: ActivatedRoute) { }

  ngOnInit() {

    localStorage.setItem('offer_type', '');

    let params = {
      "app_key": environment.greenCloudConfig.AppKey, "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode, "sessionId": localStorage.getItem('sessionId')
    }
    //console.log(JSON.stringify(params));
    this.proposalService.get_offer_usage(params).then(res => { 
     // console.log(res);
      if(res.responseCode==0 && res.success==true)
      {
        this.countsummary= res.count_summary;
      } 
      else
      {
        this.snackBar.open(res.message, '', { duration: 5000, });
      }      
      document.getElementById('loader').style.display="none"; 
    },
    err=>{ 
      console.log(err.error);  
      this.snackBar.open(err.error.message, '', { duration: 5000, });
      document.getElementById('loader').style.display="none"; 
    }); 

    this.proposalService.get_proposal_vs_product(params).then(res => { 
      //console.log(res);
      if(res.responseCode==0 && res.success==true)
      {
        this.comboChart.dataTable=res.Data;
      } 
      else
      {
        this.snackBar.open(res.message, '', { duration: 5000, });
      }      
      document.getElementById('loader').style.display="none"; 
    },
    err=>{ 
      console.log(err.error);  
      this.snackBar.open(err.error.message, '', { duration: 5000, });
      document.getElementById('loader').style.display="none"; 
    }); 
    
  }

}

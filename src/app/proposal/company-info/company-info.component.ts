import { Component, OnInit , Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute , Router ,} from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { SharedService  } from '../../shared/shared.service';

@Component({
  selector: 'app-company-info',
  templateUrl: './company-info.component.html',
  styleUrls: ['./company-info.component.scss']
})
export class CompanyInfoComponent implements OnInit {

  constructor(public dialog: MatDialog,private router:Router, @Inject(MAT_DIALOG_DATA) public data: any,
  private sanitizer: DomSanitizer, public sharedService: SharedService) { }

  ngOnInit() {
    //console.log('this.data', this.data);
  }


}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-proforma-preview',
  templateUrl: './proforma-preview.component.html',
  styleUrls: ['./proforma-preview.component.scss']
})
export class ProformaPreviewComponent implements OnInit {
  src="";
  offer_type="";
  offer_info:any;
  constructor() { }

  ngOnInit() {
    this.offer_type=localStorage.getItem('offer_type');
    if(this.offer_type==undefined || this.offer_type==null)
    {
      this.offer_type='proforma';
      localStorage.setItem('offer_type', 'proforma invoice');
    }
    this.offer_info=JSON.parse(localStorage.getItem('offer_info'));
    this.src=this.offer_info.pdf_url;
  }

}

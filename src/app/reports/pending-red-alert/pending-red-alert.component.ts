import { Component, OnInit, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { ReportsService } from '../reports.service';
import { SharedService } from '../../shared/shared.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-pending-red-alert',
  templateUrl: './pending-red-alert.component.html',
  styleUrls: ['./pending-red-alert.component.scss']
})
export class PendingRedAlertComponent {
  @ViewChildren('tooltip') tooltips;
  constructor(public reportService: ReportsService, private router:Router, private sharedService: SharedService) {
  
  }
  masterdata={'pavillion_types':[],"stand_types":[],"stand_locations":[]}    
  isSuccess: boolean;
  message : string;
  data=[];
  ngOnInit() {
    this.sharedService.getMasterData().then(response => {           
      //console.log(response);
      if(response.success)
      {
          this.masterdata=response.eventDetails;  
          this.get_data();
          this.isSuccess=true;
          this.masterdata.pavillion_types.sort((a, b) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0);
      }
   });
   //setTimeout(function(){   window.location.reload(true); }, 30000); 
  }
  get_data()
  {
    document.getElementById('loader').style.display="block"; 
    let stallListParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode
    }
    this.reportService.get_report_pending_red_alert_summary(stallListParams).then(res => { 
      //console.log(res);
      //console.log('loaded');
      if(res.responseCode==0 && res.success==true)
      {
        this.isSuccess = true;
        this.data=res.data;
        //console.log(this.data);        
      }
      else
      {
        this.isSuccess = false;
        //console.log(res);
        this.message = res.message;
      }
      document.getElementById('loader').style.display="none"; 
    });
  } 
  get_days_from_hours(hours)
  {
    if(!hours || hours===undefined || parseInt(hours)==NaN || parseInt(hours)<0)
    {
      return '';
    }
    else
    {
      hours=parseInt(hours);
      if(hours < 24)
      {
        return hours+' hrs';
      }
      if(hours==24)
      {
        return '1 day';
      }
      if(hours >24)
      {
        var text='';
        var day= (hours/24).toFixed(0);
        
        if(parseInt(day) == 1 )
        {
          text = text + day + " day ";
        }
        else{
          text = text + day + " days ";
        }
        var hrs=(hours%24);
        if(hrs >0 )
        {
          if(hrs == 1 )
          {
            text = text + hrs + " hr ";
          }
          else{
            text = text + hrs + " hrs ";
          }
        }
        return text;
      }
    }
  }
}

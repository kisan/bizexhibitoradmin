import { Component, OnInit } from '@angular/core';
import { ActivatedRoute , Router ,} from '@angular/router';
import { environment } from '../../../environments/environment';
import { ReportsService  } from '../reports.service';
//import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SharedService } from '../../shared/shared.service';
//import { LoginComponent } from '../../auth/login/login.component';
//import { StallDetailsComponent } from 'src/app/stalls/stall-details/stall-details.component';
@Component({
  selector: 'app-catalogue-detail',
  templateUrl: './catalogue-detail.component.html',
  styleUrls: ['./catalogue-detail.component.scss']
})
export class CatalogueDetailComponent implements OnInit {

  stallId: string;
  defaultExhibitorImage: string;
  stallDetails = {};
  stallDetailsNew = {};
  selectedFurnitureDetails = {};
  quotaDetails = [];
  pavillionImage: string;
  showFasciaName: boolean = false;
  showStallNumber: boolean = false;
  showTable: boolean = false;
  categoryList:any;
  tempCategory = [];
  finalCategory = [];
  productImages = [];
  prodImagePrefix: string;
  defaultVideoImage:String;
  defaultDocumentImage: string;
  supportCRMWebFormUrl: string;

  constructor( private route: ActivatedRoute, 
      private router:Router , public reportService: ReportsService,
       private sharedService:SharedService) { }

  ngOnInit() { 
    this.route.params.forEach((urlParameters) => {
      this.stallId = this.getBase64URIdecode(urlParameters['id']);
      // console.log("Stall Id: ", this.stallId);
    });
    this.defaultVideoImage = "../../assets/images/greenvideo.png";
    this.defaultDocumentImage = "../../assets/images/greendoc.png";
    this.loaddata();
    document.getElementsByTagName("body")[0].setAttribute("id", "bgcolorGray");
  }

  loaddata()
  {
   // document.getElementById('loader').style.display="block"; 
    let stallDetailsParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.K19EventCode,
      // "sessionId": localStorage.getItem('sessionId'),
      //"sessionId": 'S0lTQU4yMC05OTIzODAwNjU5XzE1ODQzNTMzMzg3Nzk=',
      "stallid": this.stallId
    }
    this.finalCategory = [];
    this.sharedService.getCategoryDetails().then(response => {
      this.categoryList = response["data"]["categories_list"];
      //console.log("this.categoryList",this.categoryList);
    this.reportService.getStallData(stallDetailsParams).then(stallData => {
      
      this.stallDetails = stallData;
      console.log(this.stallDetails['stallinfo']);
      this.selectedFurnitureDetails = this.stallDetails['stallinfo']['furniture_details'][0];

      if ((this.stallDetails['stallinfo']['logo_big_thumb']) && (this.stallDetails['stallinfo']['logo_big_thumb'] != "")) {
        this.defaultExhibitorImage =  this.stallDetails['stallinfo']['logo_big_thumb'];
      }
      else {
        this.defaultExhibitorImage = '../../assets/images/default_organization.png';
      }
      if( this.stallDetails["stallinfo"]["product_category"])
      {
        this.tempCategory = this.stallDetails["stallinfo"]["product_category"].split(',');
          for (let category in this.categoryList) {
            for (let j in this.tempCategory) {
              if (this.categoryList[category].name == this.tempCategory[j]) {
                this.finalCategory.push(this.categoryList[category]);
              }
            }
          }
      }
     
       
      this.prodImagePrefix = environment.greenCloudConfig.product_url_prefix;
      
      if(this.stallDetails["stallinfo"]["product_images"])
      {
          this.productImages = this.stallDetails["stallinfo"]["product_images"].split(',');
      }     
     
      this.quotaDetails = this.stallDetails['stallinfo']['quota_details'];

      if (this.stallDetails['stallinfo']['fascia_name']) {
        this.showFasciaName = true;
      }
      if (this.stallDetails['stallinfo']['alloted_stall_no']) {
        this.showStallNumber = true;
      }
      document.getElementById('loader').style.display="none"; 
      this.supportCRMWebFormUrl = environment.samvaadConfig.webFormLinkOrg+this.stallDetails['stallinfo']['booked_by_mobile'];
    });
  });
  }
 
    getBase64URIencode(x)
    {
        return this.sharedService.getBase64URIencode(x);
    }
    getBase64URIdecode(x)
    {
        return this.sharedService.getBase64URIdecode(x);
    }
  getRupeeFormat(amount)
  {
    return this.sharedService.getRupeeFormat(amount);
  }

  checkAllowedRoles(roles)
  {
    //console.log(roles);
    return this.sharedService.checkAllowedRoles(roles);
  }

}

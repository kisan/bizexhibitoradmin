import { Component, OnInit , ViewChildren,ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { ReportsService  } from '../reports.service';
import { SharedService  } from '../../shared/shared.service';
import { environment } from '../../../environments/environment';
import { NgImageSliderComponent } from 'ng-image-slider';
import * as $ from 'jquery';
@Component({
  selector: 'app-digital-catalogue-prev',
  templateUrl: './digital-catalogue-prev.component.html',
  styleUrls: ['./digital-catalogue-prev.component.scss']
})
export class DigitalCataloguePrevComponent implements OnInit {
  @ViewChildren('tooltip') tooltips;
  @ViewChildren('nav') slider: NgImageSliderComponent;
  imageObject: Array<object> = [{
    thumbImage: 'assets/images/slide1.jpg',
    link:'https://kisan.in/',
    alt: 'KISAN 2020 - 16 - 20 Dec'
}, {
    thumbImage: 'assets/images/slide2.jpg',
    link:'reports/catalogue-detail/MTUyOA',
    alt: 'De Laval' 
}, {

  thumbImage: 'assets/images/slide3.jpg',
  link:'/reports/catalogue-detail/MTM0MA',
  alt: 'Netafim' 
}
/*, {
  thumbImage: 'assets/images/slide4.jpg',
  link:'https://kisan.in/',
  alt: 'KISAN Post' 
}, {
  thumbImage: 'assets/images/slide5.jpg',
  link:'https://kisan.in/',
  alt: 'KISAN tv' 
}*/
];

masterdata={'pavillion_types':[],"stand_types":[],"stand_locations":[]}    
  isSuccess: boolean;
  message : string;
  exhibitors=[];
  pagesize=environment.greenCloudConfig.page_size;
  exhibitors_current_page=0;
  total_exhibitors=0;
  exhibitors_filter={"pavillion_type":"","stand_type":"","stand_location":"","product_category":[],"state":[],"city":[]};
  exhibitors_pages=0;
  exhibitors_sort_col="id";
  exhibitors_sort_dir=0;
  exhibitors_showall=1;
  exhibitors_search='';
  exhibitors_start_item=1;
  exhibitors_end_item=environment.greenCloudConfig.page_size;
  categoryList:any;
  states={};
  cities: any[] = [];
  applied_filters: any[] = [];

  constructor(public reportService: ReportsService,private router:Router, private sharedService: SharedService) { 
    
  }

  imageOnClick(index) {
   // console.log('index', index);
    window.open( this.imageObject[index]['link'] ,'_blank');
}
  ngOnInit() {

    $(document).ready(function(){
      $(window).scroll(function () {
          if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
          } else {
            $('#back-to-top').fadeOut();
          }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
          $('body,html').animate({
            scrollTop: 0
          }, 400);
          return false;
        });
    });

    //this.slider.imageClick.emit()
    document.getElementsByTagName("body")[0].setAttribute("id", "bgcolorGray");
    this.sharedService.getCategoryDetails().then(response => {
    let allCats = response["data"]["categories_list"];
     //console.log(response["data"]["categories_list"]);
      this.categoryList=[];
      for (let category of allCats) {
       // console.log(category);
        if(category['is_occupation']===false)
        {
          this.categoryList.push(category);
        }
      }
     // console.log(this.categoryList);
      this.reportService.getK19MasterData().then(response => {           
        //console.log(response);
        if(response.success)
        {
            this.masterdata=response.eventDetails; 
            this.states=this.sharedService.getStates(); 
            //console.log(this.states);                   
            this.get_stalls();
            this.masterdata.pavillion_types.sort((a, b) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0);
            console.log(this.masterdata.pavillion_types);
        }
      });
    });
  }

  get_stalls()
  {
    document.getElementById('loader').style.display="block"; 
    this.applied_filters=[];

    for (let state of this.exhibitors_filter.state) {
      this.applied_filters.push(state);
    }
    for (let city of this.exhibitors_filter.city) {
      this.applied_filters.push(city);
    }
    if(this.exhibitors_filter.pavillion_type)
    {
      for(let item of this.masterdata.pavillion_types)
      {
        if(item.short_code==this.exhibitors_filter.pavillion_type)
        {
          this.applied_filters.push(item.name)
        }
      }
    }
    for (let cat of this.exhibitors_filter.product_category) {
      this.applied_filters.push(cat);
    }
    var sort_dir="ASC";
    //console.log(this.exhibitors_filter);
    if(this.exhibitors_sort_dir==1)
    {
        sort_dir="DESC";
    }
    let stallListParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.K19EventCode,
      "pagesize":this.pagesize,
      "currentpage":this.exhibitors_current_page,
      "search":this.exhibitors_search,
      "sort_col":this.exhibitors_sort_col,
      "sort_dir":sort_dir,
      "filter":this.exhibitors_filter,
      "showall":this.exhibitors_showall
    }
    this.reportService.get_approved_stalls(stallListParams).then(res => { 
      //console.log(res);
      if(res.responseCode==0 && res.success==true)
      {
          this.isSuccess = true;
          // console.log(res.overview.records);
          this.exhibitors=res.overview.records;
          //console.log(this.exhibitors);
          this.total_exhibitors=res.overview.total_stalls.cnt;
          this.update_stall_stastics();
      }
      else
      {
          this.isSuccess = false;
          // console.log(res);
          this.message = res.message;
      }
      document.getElementById('loader').style.display="none"; 
    });
  }

  update_stall_stastics() {
    
    if(this.exhibitors_showall)
    {
        this.exhibitors_start_item = 1;
        this.exhibitors_end_item = this.total_exhibitors;
        this.exhibitors_pages=1;
    }
    else
    {
      this.exhibitors_pages=Math.ceil(this.total_exhibitors/this.pagesize); 
      this.exhibitors_start_item = ((this.exhibitors_current_page*this.pagesize)+1);
      this.exhibitors_end_item = ((this.exhibitors_current_page*this.pagesize)+this.pagesize);	
      if( this.exhibitors_end_item >this.total_exhibitors){
          this.exhibitors_end_item =this.total_exhibitors;
      }
    }
  }

  sort_stalls(key) {
    if(this.exhibitors_sort_col != key)
    {
        this.exhibitors_sort_col=key;
        this.exhibitors_sort_dir=0;
        this.exhibitors_current_page = 0;
    }
    else
    {
        this.exhibitors_sort_dir= 1-this.exhibitors_sort_dir;
    }
    this.get_stalls();
  }

  toggle_show_all_stalls(){
    //this.stall_showall=1-this.stall_showall;
    this.exhibitors_current_page=0;
		this.get_stalls();
  }
  search_stalls(event: any) { // without type info
    if(event.keyCode == 13)
		{
       // console.log(event.target.value);
        //this.stall_search=event.target.value;
        this.exhibitors_current_page=0;
        this.get_stalls();
    }
  }
  get_pavillion_name(short_code)
  {
    for (let pav of this.masterdata.pavillion_types) {
        if(pav.short_code==short_code)
        {
          return pav.name;
        }
    }
    return '';
  }
  get_stand_type_name(short_code)
  {
    for (let stand_type of this.masterdata.stand_types) {
        if(stand_type.short_code==short_code)
        {
          return stand_type.name;
        }
    }
    return '';
  }
  get_stand_location_name(short_code)
  {
    for (let stand_loc of this.masterdata.stand_locations) {
        if(stand_loc.short_code==short_code)
        {
          return stand_loc.name;
        }
    }
    return '';
  }
  getBase64URIencode(x)
  {
    return this.sharedService.getBase64URIencode(x);
  }
  getBase64URIdecode(x)
  {
    return this.sharedService.getBase64URIdecode(x);
  }

  getFullLogoUrl(logourl)
  {
    if ((logourl) && (logourl != "")) 
    {
      return logourl;
    }
    else 
    {
      return '../../assets/images/default_organization.png';
    }
  }
  showCities(states) {
    //console.log(states);
    this.cities = [];
    //let delcities=[];
    let selcities=[];
   // this.cities = this.states[city];
   for (let state of states) {
     //console.log(state);
     //console.log(this.states[state]);
     for (let city of this.states[state]) 
     {
       //console.log(city);
       this.cities.push(city);
       for (let item of this.exhibitors_filter.city) {
         if(city==item)
         {
          selcities.push(item);
         }
       }
     }
   }
   this.exhibitors_filter.city=selcities;
    this.get_stalls();
  }

  removefilter(filter_text)
  {
    //console.log(filter_text);
    let stateremove=false;
    if(this.exhibitors_filter.product_category.length>0)
    {
      let selcats=[];
      for (let item of this.exhibitors_filter.product_category) {
        if(filter_text != item)
        {
          selcats.push(item);
        }
      }
      this.exhibitors_filter.product_category=selcats;
    }
    if(this.exhibitors_filter.state.length>0)
    {
      let selstates=[];
      for (let item of this.exhibitors_filter.state) {
        if(filter_text != item)
        {
          selstates.push(item);
        }
        if(filter_text===item)
        {
          stateremove=true;
        }
      }
      this.exhibitors_filter.state=selstates;
    }
    if(!stateremove && this.exhibitors_filter.city.length>0)
    {
      let selcities=[];
      for (let item of this.exhibitors_filter.city) {
        if(filter_text != item)
        {
          selcities.push(item);
        }
      }
      this.exhibitors_filter.city=selcities;
    }
    if(this.exhibitors_filter.pavillion_type)
    {
      let selpavs=[];
      //for (let item1 of this.exhibitors_filter.pavillion_type) {
        for(let item of this.masterdata.pavillion_types)
        {
          //if(item1===item.short_code && filter_text==item.name)
          if(filter_text==item.name)
          {
            //selpavs.push(item1);
            this.exhibitors_filter.pavillion_type="";
          }
        }
      //}
      //this.exhibitors_filter.pavillion_type=selpavs;
    }
       //delete this.exhibitors_filter.product_category[filter_text];
    //console.log(this.exhibitors_filter.product_category);
    if(stateremove)
    {
      this.showCities(this.exhibitors_filter.state);
    }
    else
    {
      this.get_stalls();
    }
    
  }
}

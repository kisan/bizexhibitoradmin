import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DigitalCataloguePrevComponent } from './digital-catalogue-prev.component';

describe('DigitalCataloguePrevComponent', () => {
  let component: DigitalCataloguePrevComponent;
  let fixture: ComponentFixture<DigitalCataloguePrevComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DigitalCataloguePrevComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DigitalCataloguePrevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

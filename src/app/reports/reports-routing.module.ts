import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PendingSummaryComponent } from './pending-summary/pending-summary.component';
import { PendingRedAlertComponent } from './pending-red-alert/pending-red-alert.component';
import { DigitalCataloguePrevComponent } from './digital-catalogue-prev/digital-catalogue-prev.component';
import { CatalogueDetailComponent } from './catalogue-detail/catalogue-detail.component';

const routes: Routes = [
  {path: '', redirectTo: 'pending-summary',pathMatch: 'full'},
  { path: 'pending-summary', component: PendingSummaryComponent },
  { path: 'pending-red-alert', component: PendingRedAlertComponent },
  {path: 'digital-catalogue-prev', component: DigitalCataloguePrevComponent},
  // {path: 'catalogue-detail', component: CatalogueDetailComponent},
  {path: 'catalogue-detail/:id', component:CatalogueDetailComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }

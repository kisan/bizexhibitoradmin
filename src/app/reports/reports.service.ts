import { Injectable } from '@angular/core';
// import { Observable } from 'rxjs/Observable';
import {Router} from "@angular/router";
import { HttpClient , HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class ReportsService {
  isSuccess: boolean;
  message : string;
  sessionId : string;

  constructor(private router:Router, private http: HttpClient) {
  }

  get_report_pending_summary(stallListParams: any): Promise<any> {  

    let headers = new HttpHeaders({'Content-Type':'application/json'}); 
    let body = JSON.stringify(stallListParams);    
    
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"report_exhibitor_stall_pending_summary.php", body, { headers: headers })
    .toPromise()
  }

  get_report_pending_red_alert_summary(stallListParams: any): Promise<any> {  

    let headers = new HttpHeaders({'Content-Type':'application/json'}); 
    let body = JSON.stringify(stallListParams);    
    
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"report_exhibitor_stall_red_alert_summary.php", body, { headers: headers })
    .toPromise()
  }

  get_approved_stalls(stallListParams: any): Promise<any> {  

    let headers = new HttpHeaders({'Content-Type':'application/json'}); 
    let body = JSON.stringify(stallListParams);    
   // console.log(body);
    return this.http
    .post(environment.greenCloudConfig.K19ApiBaseUrl+"report_exhibitor_booked_stall_details.php", body, { headers: headers })
    .toPromise()
  }

  getStallData(stallDetailsParams: any): Promise<any> {
  
    let headers = new HttpHeaders({'Content-Type':'application/json'}); 
    let body = JSON.stringify(stallDetailsParams);
    //console.log(body);
    return this.http
    .post(environment.greenCloudConfig.K19ApiBaseUrl+"getstalldetails.php", body, { headers: headers })
    .toPromise()
  }

  getK19MasterData(): Promise<any> {
  
    let headers = new HttpHeaders({'Content-Type':'application/json'}); 
    let body = JSON.stringify({"app_key":environment.greenCloudConfig.AppKey,"eventCode":environment.greenCloudConfig.K19EventCode,"source":environment.greenCloudConfig.Source});

    return this.http
        .post(environment.greenCloudConfig.K19ApiBaseUrl+"geteventdetails.php", body, { headers: headers })
        .toPromise()
}

 
}

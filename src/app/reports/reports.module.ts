import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsService } from './reports.service';
import { FormsModule } from '@angular/forms';
import { ReportsRoutingModule } from './reports-routing.module';
import { CustomMaterialModule } from '../custom-material/custom-material.module';
import { SharedService } from '../shared/shared.service';
import { PendingSummaryComponent } from './pending-summary/pending-summary.component';
import { PendingRedAlertComponent } from './pending-red-alert/pending-red-alert.component';
import { DigitalCataloguePrevComponent } from './digital-catalogue-prev/digital-catalogue-prev.component';
import { NgImageSliderModule } from 'ng-image-slider';
import { CatalogueDetailComponent } from './catalogue-detail/catalogue-detail.component';
import { StallsService } from '../stalls/stalls.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReportsRoutingModule,
    CustomMaterialModule,
    NgImageSliderModule
  ],
  declarations: [ PendingSummaryComponent , PendingRedAlertComponent, DigitalCataloguePrevComponent, CatalogueDetailComponent ],
  providers: [ReportsService, SharedService, StallsService],
})
export class ReportsModule { }

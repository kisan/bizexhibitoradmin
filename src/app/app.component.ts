import { Component } from '@angular/core';
import { LoginComponent } from './auth/login/login.component';
import { AuthService } from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Exhibitor Admin';
  sessionId: string;

  constructor(public authService: AuthService, private loginComponent: LoginComponent) {
    this.sessionId = localStorage.getItem('sessionId')
    // console.log("Logged in sessionId: ", this.sessionId);
    
  }

  logout() {
    this.loginComponent.logout();
  }
}

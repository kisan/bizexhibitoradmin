import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { KonnectReportComponent } from './konnect-report/konnect-report.component';

const routes: Routes = [
  {path: '', redirectTo: 'konnect-report',pathMatch: 'full'},
  {path: 'konnect-report', component: KonnectReportComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KonnectRoutingModule { }

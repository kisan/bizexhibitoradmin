import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomMaterialModule } from '../custom-material/custom-material.module';
import { KonnectRoutingModule } from './konnect-routing.module';
import { KonnectService } from './konnect.service';
import { KonnectReportComponent } from './konnect-report/konnect-report.component';
import { QRCodeModule } from 'angular2-qrcode';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [KonnectReportComponent],
  imports: [
    CommonModule,
    KonnectRoutingModule,
    CustomMaterialModule,
    QRCodeModule,
    FormsModule
  ],
  providers:[
    KonnectService
  ]
})
export class KonnectModule { }

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class KonnectService {
  private readonly reportReaderSummeryUrl = `${environment.greenCloudConfig.ApiBaseUrl}getreadersummaryforadmin.php`;

  constructor(
    private http: HttpClient
  ) { 
    
  }

  getReadersummary(inviteParam) {
    return this.http.post(this.reportReaderSummeryUrl, inviteParam);
  }
}

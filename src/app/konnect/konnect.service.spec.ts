import { TestBed } from '@angular/core/testing';

import { KonnectService } from './konnect.service';

describe('KonnectService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KonnectService = TestBed.get(KonnectService);
    expect(service).toBeTruthy();
  });
});

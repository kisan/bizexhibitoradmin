import { Component, OnInit, AfterViewChecked, AfterViewInit } from '@angular/core';
import htmlToImage from 'html-to-image';
import { KonnectService } from '../konnect.service';
import { environment } from '../../../environments/environment';
import { SharedService  } from '../../shared/shared.service';




@Component({
  selector: 'app-konnect-report',
  templateUrl: './konnect-report.component.html',
  styleUrls: ['./konnect-report.component.scss']
})
export class KonnectReportComponent implements OnInit {
  reader_sort_dir: number;
  pagesize:number=environment.greenCloudConfig.page_size;
  reader_pass_type: any;
  reader_current_page: number = 0;
  reader_search: any;
  reader_filter: any;
  reader_sort_col="t1.exhibitor_scans";
  readerSummery: any;
  readerdata: any;
  reader_total_pages: number;
  reader_start_item: number;
  reader_end_item: number;
  total_reader: number;
  reader_showall:number;

  constructor(
    private konnectService:KonnectService,
    private sharedService: SharedService
  ) { }

  ngOnInit() {
    this.getReadersummary();
  }

  
  getReadersummary(){
    if(document.getElementById('loader')){
      document.getElementById('loader').style.display="block";
    }
    this.konnectService.getReadersummary(this.inviteParam())
    .subscribe(readerRes=>{
      console.log('readerRes', readerRes)
      document.getElementById('loader').style.display="none";
      this.readerSummery=readerRes['data']['summary'];
      console.log('this.readerSummery', this.readerSummery)
      this.readerdata=readerRes['data']['records'];
      this.total_reader = readerRes['data']['total_count'];
      this.update_reader_statics();

    })
  }
  inviteParam(){
    var sort_dir="ASC";
    if(this.reader_sort_dir==1){
      sort_dir="DESC";
    }
;    console.log('this.reader_showall', this.reader_showall);

    let inviteParam={
      "app_key": environment.greenCloudConfig.AppKey,
      "eventCode":environment.greenCloudConfig.EventCode,
      "source":environment.greenCloudConfig.Source,
      "sessionId": localStorage.getItem('sessionId'),
      // "passType":this.reader_pass_type,
      "pagesize":this.pagesize,
      "currentpage":this.reader_current_page,
      "search":this.reader_search,
      // "filter":this.reader_filter,
      "sort_col":this.reader_sort_col,
      "sort_dir":sort_dir,
      "showall":this.reader_showall,
    }
    return inviteParam;
}
update_reader_statics(){
  this.reader_total_pages=Math.ceil(this.total_reader/this.pagesize);
  this.reader_start_item = ((this.reader_current_page*this.pagesize)+1);
  this.reader_end_item = ((this.reader_current_page*this.pagesize)+this.pagesize);
  if( this.reader_end_item > this.total_reader){
      this.reader_end_item = this.total_reader;
  }
}
sort_reader(key){
  if(this.reader_sort_col != key){
    this.reader_sort_col = key;
    this.reader_sort_dir = 0;
    this.reader_current_page = 0;
  }else{

      this.reader_sort_dir=1-this.reader_sort_dir;
  }
  this.getReadersummary();
}
toggle_show_all_invite(){
  this.reader_current_page = 0;
  this.getReadersummary();
}
search_reader(event: any) { // without type info
  if(event.keyCode == 13)
      {
     // console.log(event.target.value);
      //this.stall_search=event.target.value;
      this.reader_current_page=0;
      this.getReadersummary();
  }
}
getRupeeFormat(amount)
  {
    return this.sharedService.getRupeeFormat(amount);
  }
  openMystallDialog(){
    // this.loadingService.showLoader();
    document.getElementById('loader').style.display='block';
    let global = this;
    document.getElementById('dnldBlk').style.display='block';
    console.log('document.getElementById(\'dnldBlk\')', document.getElementById('dnldBlk'))
    htmlToImage.toPng(document.getElementById('dnldBlk'), { quality: 0.95 })
    .then(function (dataUrl) {
      // this.qrSize=600;
      var link = document.createElement('a');
      link.download = 'reader-app.png';
      link.href = dataUrl;
      link.click();
      document.getElementById('dnldBlk').style.display='none';
      document.getElementById('loader').style.display='none';

      // global.loadingService.hideLoader();
      // global.dialogref.close();
      
    });
  }

  openMystallDialogLarge(){
    // this.loadingService.showLoader();
    let global = this;
    document.getElementById('loader').style.display='block';
    document.getElementById('dnldBlklarge').style.display='block';
    console.log('document.getElementById(\'dnldBlklarge\')', document.getElementById('dnldBlklarge'))
    htmlToImage.toPng(document.getElementById('dnldBlklarge'), { quality: 0.95 })
    .then(function (dataUrl) {
      // this.qrSize=600;
      var link = document.createElement('a');
      link.download = 'reader-app.png';
      link.href = dataUrl;
      link.click();
      document.getElementById('dnldBlklarge').style.display='none';
      document.getElementById('loader').style.display='none';
      // global.loadingService.hideLoader();
      // global.dialogref.close();
      
    });
  }
  /* export to csv */
  exportToreaderSummaryCSV(){

    document.getElementById('loader').style.display="block";
    let sort_dir="ASC";
    if(this.reader_sort_dir==1){
      sort_dir="DESC";
    }

    const exportParams = {...this.inviteParam(), ...{"showall":1}};

    this.konnectService.getReadersummary(exportParams).subscribe(response =>{
        if(response['success']){
          // var passes = response['data']['records'];
          // var csvdata = this.sharedService.JSONToCSVConvertor(passes, "reader Summary Report", true);
          // if(csvdata){
          //   var a         = document.createElement('a');
          //   a.href        = 'data:attachment/csv,' + escape(csvdata);
          //   a.target      = '_blank';
          //   a.download    = environment.greenCloudConfig.EventCode+'_ReaderSummary.csv';
          //   document.body.appendChild(a);
          //   a.click();
          // }
          var passes = response['data']['records'];
          var csvdata = this.sharedService.JSONToCSVConvertor(passes, "Reader Summary Report", true);
          console.log('csvdata', csvdata);
          if(csvdata){
            var csvBlob = new Blob([csvdata], { type: 'text/csv' });
            var csvUrl = URL.createObjectURL(csvBlob);
            var a         = document.createElement('a');
           // a.href        = 'data:attachment/csv,' + escape(csvdata);
            a.href = csvUrl;
            a.target      = '_blank';
            a.download    = environment.greenCloudConfig.EventCode+'_ReaderSummary.csv';
            document.body.appendChild(a);
            a.click();
          }
        }
        document.getElementById('loader').style.display="none";
    }, (err) => {
      document.getElementById('loader').style.display="none";
    }
    );
  }
}

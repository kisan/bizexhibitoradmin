import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KonnectReportComponent } from './konnect-report.component';

describe('KonnectReportComponent', () => {
  let component: KonnectReportComponent;
  let fixture: ComponentFixture<KonnectReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KonnectReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KonnectReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

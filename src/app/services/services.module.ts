import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CustomMaterialModule } from '../custom-material/custom-material.module';
import { TimeAgoPipe } from 'time-ago-pipe';

import { ServicesRoutingModule } from './services-routing.module';
import { ChartsModule } from 'ng2-charts'; 
import { BadgesListComponent } from './badges-list/badges-list.component';
import { InvitesComponent } from './invites/invites.component';
import { InvitesListComponent } from './invites-list/invites-list.component';
import { FurnitureComponent } from './furniture/furniture.component';
import { VendorListComponent } from './vendor-list/vendor-list.component'; 
import { BadgesComponent } from './badges/badges.component';
import { VendorDetailsComponent , ConfirmVendorFormEditPopupComponent , VendorFormSentComponent} from './vendor-details/vendor-details.component';

import { SharedService } from '../shared/shared.service';
import { SevicesService } from './services.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ServicesRoutingModule,
    CustomMaterialModule,
    ChartsModule
  ],
  declarations: [
    BadgesComponent,
    BadgesListComponent,
    InvitesComponent,
    InvitesListComponent,
    FurnitureComponent,
    VendorListComponent,
    VendorDetailsComponent,
    ConfirmVendorFormEditPopupComponent,
    TimeAgoPipe,
    VendorFormSentComponent
  ],
  entryComponents:[ConfirmVendorFormEditPopupComponent,VendorFormSentComponent],
  providers: [SevicesService, SharedService],
})
export class ServicesModule { }

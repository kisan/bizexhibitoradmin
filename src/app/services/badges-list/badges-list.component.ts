import { Component, OnInit } from '@angular/core';
import { SevicesService } from './../services.service';
import { environment } from '../../../environments/environment';
import { SharedService  } from '../../shared/shared.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-badges-list',
  templateUrl: './badges-list.component.html',
  styleUrls: ['./badges-list.component.scss']
})
export class BadgesListComponent implements OnInit {

  stall_passes = [];
  stall_passes_current_page:number = 0;
  pagesize:number = environment.greenCloudConfig.page_size;
  stall_passes_search:string = "";
  stall_passes_sort_col = "created_date";
  stall_passes_sort_dir = 1;
  stall_passes_filter = {"faircat_status":""};
  stall_passes_type:string = "onlineexhibitorbadge";
  total_stall_passes:number = 0;
  isSuccess:boolean;
  errorMessage:string;
  stall_passes_start_item = 1;
  stall_passes_end_item = environment.greenCloudConfig.page_size;
  stall_passes_total_pages = 0;
  constructor(private stallPassesService: SevicesService, private sharedService: SharedService ,   private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.get_stall_passes();
  }
  openMessage(message: string) {
    let action = null;
    this.snackBar.open(message, action, {
      duration: 10000,
    });
  }
  get_stall_passes(){

    if(document.getElementById('loader')){
      document.getElementById('loader').style.display="block";
    }
    var sort_dir="ASC";
    if(this.stall_passes_sort_dir==1){
        sort_dir="DESC";
    }

    let stallPassesParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "eventCode": environment.greenCloudConfig.EventCode,
      "source": environment.greenCloudConfig.Source,
      "pagesize":this.pagesize,
      "currentpage":this.stall_passes_current_page,
      "search": this.stall_passes_search,
      "sort_col": this.stall_passes_sort_col,
      "sort_dir":sort_dir,
      "showall":0,
      "filter": this.stall_passes_filter,
      "passType": this.stall_passes_type
    }

    this.stallPassesService.get_stall_passes(stallPassesParams).then(response =>{
      console.log(response);
      if(response.responseCode == 0 && response.success == true){
        this.isSuccess = true;
        this.stall_passes = response.overview.records;
        this.total_stall_passes = response.overview.total.cnt;
        this.update_stall_passes_statics();
      }else{
        this.isSuccess = false;
        this.errorMessage = response.message;
      }
      document.getElementById('loader').style.display="none"; 
    });

  }

  update_stall_passes_statics(){
    this.stall_passes_total_pages=Math.ceil(this.total_stall_passes/this.pagesize); 
    this.stall_passes_start_item = ((this.stall_passes_current_page*this.pagesize)+1);
    this.stall_passes_end_item = ((this.stall_passes_current_page*this.pagesize)+this.pagesize);
    if( this.stall_passes_end_item > this.total_stall_passes){
        this.stall_passes_end_item = this.total_stall_passes;
    }
  }
  sort_stall_passes(key){
    if(this.stall_passes_sort_col != key){
      this.stall_passes_sort_col = key;
      this.stall_passes_sort_dir = 0;
      this.stall_passes_current_page = 0;
    }else{
        this.stall_passes_sort_dir=1-this.stall_passes_sort_dir;
    }
    this.get_stall_passes();
  }
  search_stall_passes(event:any){
    if(event.keyCode == 13){
      this.stall_passes_current_page=0;
      this.get_stall_passes();
    } 
  }
  reload_stall_passes(){
    this.stall_passes_search = "";
    this.stall_passes_sort_col = "stall_id";
    this.stall_passes_sort_dir = 1;
    this.stall_passes_current_page = 0;
    this.get_stall_passes();
  }
 
  exportStallPasssCSV(){
    document.getElementById('loader').style.display="block";
    let stallPassesParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "eventCode": environment.greenCloudConfig.EventCode,
      "source": environment.greenCloudConfig.Source,
      "pagesize":this.pagesize,
      "currentpage":0,
      "showall":1,
      "passType": this.stall_passes_type
    }
    this.stallPassesService.get_stall_passes(stallPassesParams).then(response =>{
      if(response.responseCode == 0 && response.success == true){
        var passes = response.overview.records;
        var csvdata = this.sharedService.JSONToCSVConvertor(passes, "Badges List Report", true);
          if(csvdata){
            var a         = document.createElement('a');
            a.href        = 'data:attachment/csv,' + escape(csvdata);
            a.target      = '_blank';
            a.download    = environment.greenCloudConfig.EventCode+'_BadgesList.csv';
            document.body.appendChild(a);
            a.click();
          }
      }  
      document.getElementById('loader').style.display="none"; 
    });
  }
  resendPass(barcode){
    
    this.stallPassesService.resendPass(barcode).then(response => {
    //  console.log(response);
      if(response['success']){
       
         this.openMessage(response['message']);
      }
      else{
        this.openMessage(response['message']);
      }
    })
    .catch(response => {
      // this.logger.error("Catch for resend pass", response);
    })
  }
  getBase64URIencode(key){
    return this.sharedService.getBase64URIencode(key);
  }
}

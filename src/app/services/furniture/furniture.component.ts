import { Component, OnInit } from '@angular/core';
import { SevicesService } from './../services.service';
import { SharedService  } from '../../shared/shared.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-furniture',
  templateUrl: './furniture.component.html',
  styleUrls: ['./furniture.component.scss']
})
export class FurnitureComponent implements OnInit {

  stallFuniture=[];
  step:number = 0;
  pagesize=environment.greenCloudConfig.page_size;
  furniture_stall_current_page=0;
  total_furniture_stall=0;
  furniture_stall_filter={"pavillion_type":"","stand_type":"","stand_location":""};
  furniture_stall_pages=0;
  furniture_stall_sort_col="company_name";
  furniture_stall_sort_dir=1;
  furniture_stall_showall=0;
  furniture_stall_search="";
  furniture_stall_start_item=1;
  furniture_stall_end_item=environment.greenCloudConfig.page_size;
  isSuccess:boolean;
  errorMessage:string;
  masterData={ 
              "pavillion_types":[],
              "stand_types":[],
              "stand_locations":[]  
            };

  constructor(private furnitureService: SevicesService, private sharedService: SharedService) { }
  ngOnInit() {
    this.sharedService.getMasterData().then(response =>{
      if(response.success){
        this.masterData = response.eventDetails;
        // console.log(this.masterData);
        this.get_stall_furniture_list();
        this.masterData.pavillion_types.sort((a, b) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0);
      }
    });
  }
  
  get_stall_furniture_list(){
    document.getElementById('loader').style.display="block";
    var sort_dir="ASC";
    if(this.furniture_stall_sort_dir==1){
        sort_dir="DESC";
    }
    let stallFurnitureParams={
      "app_key": environment.greenCloudConfig.AppKey,
      "eventCode":environment.greenCloudConfig.EventCode,
      "source":environment.greenCloudConfig.Source,
      "pagesize":this.pagesize,
      "currentpage":this.furniture_stall_current_page,
      "search":this.furniture_stall_search,
      "sort_col":this.furniture_stall_sort_col,
      "sort_dir":sort_dir,
      "showall":this.furniture_stall_showall,
      "filter":this.furniture_stall_filter
    };

    this.furnitureService.get_stall_furniture_list(stallFurnitureParams).then(response =>{
        if(response.responseCode == 0 && response.success == true){
            // console.log(response.overview.records);
            this.isSuccess = true;
            this.stallFuniture = response.overview.records;
            this.total_furniture_stall = response.overview.total_stalls.cnt;
            this.update_furniture_stall_statics();
        }else{
            //no result show message
            this.isSuccess = false;
            this.errorMessage = response.message;
        }
        document.getElementById('loader').style.display="none";
    });
  }
  
  sort_furniture_stalls(key){
    if(this.furniture_stall_sort_col != key){
        this.furniture_stall_sort_col=key;
        this.furniture_stall_sort_dir=0;
        this.furniture_stall_current_page = 0;
    }else{
        this.furniture_stall_sort_dir=1-this.furniture_stall_sort_dir;
    }
    this.get_stall_furniture_list();
  }
  
  update_furniture_stall_statics(){
    if(this.furniture_stall_showall){
        this.furniture_stall_start_item = 1;
        this.furniture_stall_end_item = this.total_furniture_stall;
        this.furniture_stall_pages=1;
    }else{
      this.furniture_stall_pages=Math.ceil(this.total_furniture_stall/this.pagesize); 
      this.furniture_stall_start_item = ((this.furniture_stall_current_page*this.pagesize)+1);
      this.furniture_stall_end_item = ((this.furniture_stall_current_page*this.pagesize)+this.pagesize);
      if( this.furniture_stall_end_item > this.total_furniture_stall){
          this.furniture_stall_end_item = this.total_furniture_stall;
      }
    }  
  }

  search_stalls_by_company(event:any){
    if(event.keyCode == 13){
      this.furniture_stall_current_page=0;
      this.get_stall_furniture_list();
    } 
  }

  reload_stall_furniture_list(){
    this.furniture_stall_filter = {"pavillion_type":"","stand_type":"","stand_location":""};
    this.furniture_stall_search = "";
    this.furniture_stall_sort_col="company_name";
    this.furniture_stall_current_page=0;
    this.furniture_stall_sort_dir=1;
    this.furniture_stall_showall=0;
    this.get_stall_furniture_list();
  }

  /* export to csv */
  exportToStallFurnitureCSV(){
    document.getElementById('loader').style.display="block";
    let stallFurnitureParams={
      "app_key": environment.greenCloudConfig.AppKey,
      "eventCode":environment.greenCloudConfig.EventCode,
      "source":environment.greenCloudConfig.Source,
      "pagesize":this.pagesize,
      "currentpage":this.furniture_stall_current_page,
      "showall":1
    };
    this.furnitureService.get_stall_furniture_list(stallFurnitureParams).then(response =>{
        // console.log(response);
        if(response.responseCode == 0 && response.success == true){
          var passes = response.overview.records;
          passes.forEach(function(v){ 
            delete v.std_panel;									
            delete v.std_round_table_glass_top;
            delete v.std_halogen_400;				
            delete v.std_power;
            delete v.std_security_day;
            delete v.std_security_night;
            delete v.std_panel_set_4;});
          var csvdata = this.sharedService.JSONToCSVConvertor(passes, "Furniture Stall Report", true);
          if(csvdata){
            var a         = document.createElement('a');
            a.href        = 'data:attachment/csv,' + escape(csvdata);
            a.target      = '_blank';
            a.download    = environment.greenCloudConfig.EventCode+'_FurnitureStall.csv';
            document.body.appendChild(a);
            a.click();
          }
        }
        document.getElementById('loader').style.display="none";
    });
  }

  /* Handles mat accordion */
  setStep(index: number){
    this.step = index;
  }
  show_all_furniture_stalls(){
    this.furniture_stall_current_page=0;
    this.get_stall_furniture_list();
  }
}

import { Component, OnInit } from '@angular/core';
import { SevicesService } from './../services.service';
import { environment } from '../../../environments/environment';
import { SharedService  } from '../../shared/shared.service';

@Component({
  selector: 'app-badges',
  templateUrl: './badges.component.html',
  styleUrls: ['./badges.component.scss']
})
export class BadgesComponent implements OnInit {
  // Doughnut Chart
  public badgesIssuedChartLabels:string[] = ["Badges Issued","Remaining Badges"];
  public badgesIssuedChartData:number[] = [];
  public badgesIssuedChartType:string = 'doughnut';
  public badgesIssuedChartColors: any[] = [{ backgroundColor: ["#EFC051", "#F0F2F8"] }];

  // Doughnut Chart
  public companyIssuesBadgesChartLabels:string[] = ["Badges Issued Companies","Remaining Companies"];
  public companyIssuesBadgesChartData:number[] = [];
  public companyIssuesBadgesChartType:string = 'doughnut';
  public companyIssuesBadgesChartColors: any[] = [{ backgroundColor: ["#a3a1fb", "#F0F2F8"] }];


  badges=[];
  total_badges=0;
  pagesize:number = environment.greenCloudConfig.page_size; 
  badges_current_page:number = 0;
  badges_search:string = "";
  badges_filter = {"pavillion_type":"","stand_type":"","stand_location":""};
  badges_pass_type:string = "onlineexhibitorbadge";
  badges_start_item=1;
  badges_end_item=environment.greenCloudConfig.page_size;
  badges_total_pages=0;
  badges_sort_col="company_name";
  badges_sort_dir=0;
  masterData={ 
      "pavillion_types":[],
      "stand_types":[],
      "stand_locations":[]  
  };
  badges_report_types = ["badges_summary","badge_min1","company_badges_latest5"];
  badges_issued={"total_badges": "","used_badges": "","percent": 0};
  companies_badges={"count": 0,"total": 0,"percent": 0};
  latest_issued_badges=[];
  isSuccess:boolean;
  errorMessage:string;

  constructor(private badgesService: SevicesService, private sharedService: SharedService) { }

  ngOnInit() {
    this.sharedService.getMasterData().then(response =>{
      if(response.success){
        this.masterData = response.eventDetails;
        this.get_badges_summary();
        this.get_badges_report();
        this.masterData.pavillion_types.sort((a, b) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0);
      }
    });
  }
  get_badges_summary(){
    
    let badgesParam = {
      "app_key":environment.greenCloudConfig.AppKey,
      "eventCode":environment.greenCloudConfig.EventCode,
      "source":environment.greenCloudConfig.Source,
      "reports":this.badges_report_types
    }

    this.badgesService.get_usage_details(badgesParam).then(response =>{
      // console.log(response);  
      this.badges_issued = response.data.badges_summary;
      this.companies_badges = response.data.badge_min1;
      this.latest_issued_badges = response.data.company_badges_latest5;
      this.create_chart();
    });

  }
  get_badges_report(){

    document.getElementById('loader').style.display="block";
    var sort_dir="ASC";
    if(this.badges_sort_dir==1){
        sort_dir="DESC";
    }
    let badesParamList={
      "app_key": environment.greenCloudConfig.AppKey,
      "eventCode":environment.greenCloudConfig.EventCode,
      "source":environment.greenCloudConfig.Source,
      "passType":this.badges_pass_type,
      "pagesize":this.pagesize,
      "currentpage":this.badges_current_page,
      "search":this.badges_search,
      "filter":this.badges_filter,
      "sort_col":this.badges_sort_col,
      "sort_dir":sort_dir
    };
    this.badgesService.get_exhibitor_passes(badesParamList).then(response =>{
        if(response.responseCode == 0 && response.success == true){
          this.isSuccess = true;
          this.badges = response.overview.records;
          this.total_badges = response.overview.total_stalls.cnt;
          this.update_badges_statics();
        }else{
          this.isSuccess = false;
          this.errorMessage = response.message;
        }
      document.getElementById('loader').style.display="none";
    });
  }
  update_badges_statics(){
    this.badges_total_pages=Math.ceil(this.total_badges/this.pagesize); 
    this.badges_start_item = ((this.badges_current_page*this.pagesize)+1);
    this.badges_end_item = ((this.badges_current_page*this.pagesize)+this.pagesize);
    if( this.badges_end_item > this.total_badges){
        this.badges_end_item = this.total_badges;
    }
  }
  search_badges(event:any){
    if(event.keyCode == 13){
      this.badges_current_page=0;
      this.get_badges_report();
    } 
  }
  reload_badges(){
    this.badges_filter = {"pavillion_type":"","stand_type":"","stand_location":""};
    this.badges_search = "";
    this.badges_sort_col = "company_name";
    this.badges_sort_dir = 0;
    this.badges_current_page=0;
    this.badgesIssuedChartData.splice(0,this.badgesIssuedChartData.length);
    this.companyIssuesBadgesChartData.splice(0,this.companyIssuesBadgesChartData.length);
    this.get_badges_summary();
    this.get_badges_report();
  }
  sort_badges(key){
    if(this.badges_sort_col != key){
      this.badges_sort_col = key;
      this.badges_sort_dir = 0;
      this.badges_current_page = 0;
    }else{
        this.badges_sort_dir=1-this.badges_sort_dir;
    }
    this.get_badges_report();
  }
  create_chart(){
    this.badgesIssuedChartData=[];
    this.companyIssuesBadgesChartData=[];
    this.badgesIssuedChartData.push(parseInt(this.badges_issued.used_badges),(parseInt(this.badges_issued.total_badges)-parseInt(this.badges_issued.used_badges)));
    this.companyIssuesBadgesChartData.push(this.companies_badges.count,(this.companies_badges.total - this.companies_badges.count));
  }
  exportBadgesOverviewCSV(){
    document.getElementById('loader').style.display="block"; 
    let badesParamList={
      "app_key": environment.greenCloudConfig.AppKey,
      "eventCode":environment.greenCloudConfig.EventCode,
      "source":environment.greenCloudConfig.Source,
      "passType":this.badges_pass_type,
      "pagesize":this.pagesize,
      "currentpage":0,
      "showall":1
    };
    this.badgesService.get_exhibitor_passes(badesParamList).then(response =>{
      //console.log(response);
      var badges = response.overview.records;
      var csvdata = this.sharedService.JSONToCSVConvertor(badges, "Badges Overview Report", true);
        if(csvdata){
          var a         = document.createElement('a');
					a.href        = 'data:attachment/csv,' + escape(csvdata);
					a.target      = '_blank';
					a.download    = environment.greenCloudConfig.EventCode+'_BadgesOverview.csv';
					document.body.appendChild(a);
          a.click();
        }
        document.getElementById('loader').style.display="none"; 
    });
  }
  getLogoUrl(url){
    if(url && url != ""){
      return url;
    }else{
      return '../../assets/images/default_organization.png';
    }
  }
}

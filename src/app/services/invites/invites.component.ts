import { Component, OnInit } from '@angular/core';
import { SevicesService } from './../services.service';
import { environment } from '../../../environments/environment';
import { SharedService  } from '../../shared/shared.service';

@Component({
  selector: 'app-invites',
  templateUrl: './invites.component.html',
  styleUrls: ['./invites.component.scss']
})
export class InvitesComponent implements OnInit {

  public passType='standard';
   // Doughnut Chart
  public invitesChartLabels:string[] = ['Invites Sent', 'Remaining Invites'];
  public invitesChartData:number[] = [];
  public invitesChartType:string = 'doughnut';
  public invitesChartColors: any[] = [{ backgroundColor: ["#4ad991", "#F0F2F8"] }];

  // Doughnut Chart
  public companyInvitesChartLabels:string[] = ["Invites Sent Companies","Remaining Companies"];
  public companyInvitesChartData:number[] = [];
  public companyInvitesChartType:string = 'doughnut';
  public companyInvitesChartColors: any[] = [{ backgroundColor: ["#a3a1fb", "#F0F2F8"] }];

  invites = [];
  total_invites = 0;
  pagesize:number = environment.greenCloudConfig.page_size;
  invites_current_page:number = 0;
  invites_search:string = "";
  invites_filter = {"pavillion_type":"","stand_type":"","stand_location":""};
  invites_pass_type:string = "onlineexhibitorinvite";
  invites_start_item=1;
  invites_end_item=environment.greenCloudConfig.page_size;
  invites_total_pages=0;
  invite_showall=0;
  invites_sort_col= "company_name";
  invites_sort_dir=0;
  invite_status = "";
  masterData={
      "pavillion_types":[],
      "stand_types":[],
      "stand_locations":[]
  };
  isSuccess:boolean;
  errorMessage:string;
  invites_report_types = ["greenpass_summary","greenpass_min1","company_greenpass_latest5"];
  greenpass_summary = {"total_greenpass":"", "greenpass_invites":"", "percent":0 };
  company_invites = { "count": 0 , "percent": 0, "total": 0};
  latest_invites_issued = [];
  inviteRecords: any;
  inviteSummary: any;
  isToggle = true;
  selectedInviteSts = '';




  constructor(private inviteService: SevicesService , private sharedService: SharedService ) { }

  ngOnInit() {
    this.sharedService.getMasterData().then(response => {
      //console.log(response);
      if(response.success){
        this.masterData = response.eventDetails;
      //  this.get_invites_summary();
      //  this.get_invites_report();
        this.getExhibitorInviteSummary();
        this.masterData.pavillion_types.sort((a, b) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0);
      }
    });
  }
  get_invites_summary(){
   // document.getElementById('loader').style.display="block";
    let invitesParam = {
      "app_key":environment.greenCloudConfig.AppKey,
      "eventCode":environment.greenCloudConfig.EventCode,
      "source":environment.greenCloudConfig.Source,
      "reports":this.invites_report_types
    }
    this.inviteService.get_usage_details(invitesParam).then(response=>{
      //console.log(response);
      if(response.success){
        this.greenpass_summary = response.data.greenpass_summary;
        this.latest_invites_issued = response.data.company_greenpass_latest5;
        this.company_invites = response.data.greenpass_min1;
        this.create_chart();
        this.get_invites_report();
      }
    //  document.getElementById('loader').style.display="none";
    },error=>{document.getElementById('loader').style.display="none";});
  }
  get_invites_report(){
    document.getElementById('loader').style.display="block";
    var sort_dir="ASC";
    if(this.invites_sort_dir==1){
      sort_dir="DESC";
    }
    let invitesParamList={
      "app_key": environment.greenCloudConfig.AppKey,
      "eventCode":environment.greenCloudConfig.EventCode,
      "source":environment.greenCloudConfig.Source,
      "passType":this.invites_pass_type,
      "pagesize":this.pagesize,
      "currentpage":this.invites_current_page,
      "search":this.invites_search,
      "filter":this.invites_filter,
      "sort_col":this.invites_sort_col,
      "sort_dir":sort_dir
    };

    this.inviteService.get_exhibitor_passes(invitesParamList).then(response=>{
      //console.log(response);
      if(response.responseCode == 0 && response.success == true){
        this.isSuccess = true;
        this.invites = response.overview.records;
        this.total_invites = response.overview.total_stalls.cnt;
        this.update_invites_statics();
      }else{
        this.isSuccess = false;
        this.errorMessage = response.message;
      }
      document.getElementById('loader').style.display="none";
    },error=>{document.getElementById('loader').style.display="none";});
  }
  update_invites_statics(){
    this.invites_total_pages=Math.ceil(this.total_invites/this.pagesize);
    this.invites_start_item = ((this.invites_current_page*this.pagesize)+1);
    this.invites_end_item = ((this.invites_current_page*this.pagesize)+this.pagesize);
    if( this.invites_end_item > this.total_invites){
        this.invites_end_item = this.total_invites;
    }
  }
  sort_invites_report(key){
    if(this.invites_sort_col != key){
      this.invites_sort_col = key;
      this.invites_sort_dir = 0;
      this.invites_current_page = 0;
    }else{
        this.invites_sort_dir=1-this.invites_sort_dir;
    }
    this.get_invites_report();
  }
  sort_invites(key){
    console.log('key', key);
    console.log('key != \'company_name\'', key == 'company_name');

    if (key != 'company_name' && key != 'area' && key != 'invite_status' && key != 'count_csv' && key != 'phone_invites') {

      key = `${this.passType  == 'standard' ? 'quota': 'unlimited'}_${key}`;
      console.log('key_inside', key);
    }
    else{
      console.log('original key');

    }

    console.log('this.invites_sort_col', this.invites_sort_col);
    if(this.invites_sort_col != key){
      this.invites_sort_col = key;
      this.invites_sort_dir = 0;
      this.invites_current_page = 0;
    }else{

        this.invites_sort_dir=1-this.invites_sort_dir;
    }
    this.getExhibitorInviteSummary();
  }

  statusFilter (evt) {

    // console.log('this.invites_sort_col', this.invites_sort_col);
    // this.sort_invites(this.invites_sort_col);

    this.getExhibitorInviteSummary();

    // if(evt == 'setnsent') {
    //   // this.invites_sort_col = 'invite_status';
    //   this.invites_sort_dir = 1;
    //   this.invites_current_page = 0;
    //   this.getExhibitorInviteSummary();
    // }
    // else if (evt == '') {
    //   // this.invites_sort_col = 'company_name';
    //   this.invites_sort_dir = 0;
    //   this.invites_current_page = 0;
    //   this.getExhibitorInviteSummary();
    // }
    // else {
    //   this.sort_invites(this.invites_sort_col);
    // }

  }


  search_invites_passes(event : any){
    if(event.keyCode == 13){
      this.invites_current_page=0;
      // this.get_invites_report();
      this.getExhibitorInviteSummary();
    }
  }

  reload_invites_report(){
    this.invites_filter = {"pavillion_type":"","stand_type":"","stand_location":""};
    this.invites_search = "";
    this.invites_sort_col = "company_name";
    this.invites_sort_dir = 0;
    this.invites_current_page = 0;
    this.invitesChartData.splice(0,this.invitesChartData.length);
    this.companyInvitesChartData.splice(0,this.companyInvitesChartData.length);
    this.get_invites_summary();
  }
  getLogoUrl(url){
    if(url && url != ""){
      return url;
    }else{
      return '../../assets/images/default_organization.png';
    }
  }
  create_chart(){
    console.log('create_chart')
    this.invitesChartData = [];
    this.companyInvitesChartData = [];
    this.invitesChartData.push(parseInt(this.greenpass_summary.greenpass_invites),(parseInt(this.greenpass_summary.total_greenpass)-parseInt(this.greenpass_summary.greenpass_invites)));
    this.companyInvitesChartData.push(this.company_invites.count,this.company_invites.total-this.company_invites.count);
    console.log('this.company_invites.count', this.company_invites.count);
    console.log('this.companyInvitesChartData', this.companyInvitesChartData);
  }

  getExhibitorInviteSummary() {
    document.getElementById('loader').style.display="block";
    let sort_dir="ASC";
    if(this.invites_sort_dir==1){
      sort_dir="DESC";
    }

    this.inviteService.getExhibitorInviteSummary(this.inviteSummaryParams(sort_dir))
      .subscribe(
        summaryRes => {
        // console.log('summaryRes', summaryRes);
        document.getElementById('loader').style.display="none";
        if(summaryRes['data']['success']) {
          this.inviteRecords = summaryRes['data']['records'];
          this.inviteSummary = summaryRes['data']['summary'];
          this.total_invites = summaryRes['data']['total_count'];
          this.invitesChartData = [this.inviteSummary['sent'], this.inviteSummary['totalquota']-this.inviteSummary['sent']];
          const withsetnsendinvite =  Number(this.inviteSummary['withsetnsendinvite'])
          this.companyInvitesChartData = [this.inviteSummary['withsetnsendinvite'], this.inviteSummary['withquota'] - withsetnsendinvite];
          // this.inviteRecords.forEach((item)=>{
          //   console.log('item[\'phone_invites\']11111', item['phone_invites'])
          //   if (item['phone_invites']=="0"){
          //     item['phone_invites']=parseInt(item['phone_invites']);
          //     console.log('item[\'phone_invites\']22222', item['phone_invites'])
          //   }
          // })
          this.update_invites_statics();
        }
       },(err) => {
        document.getElementById('loader').style.display="none";
       }
      );
  }


  inviteSummaryParams(sort_dir) {
    const inviteSummaryParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "eventCode": environment.greenCloudConfig.EventCode,
      "source": environment.greenCloudConfig.Source,
      "pagesize": environment.greenCloudConfig.page_size,
      "currentpage": this.invites_current_page,
      "invite_status": this.invite_status,
      "search": this.invites_search,
      "sort_col":this.invites_sort_col,
      "sort_dir": sort_dir,
      "showall":this.invite_showall,
      "pass_type": this.passType

    }

    return inviteSummaryParams;
  }

  getRupeeFormat(amount)
  {
    return this.sharedService.getRupeeFormat(amount);
  }

  toggle_show_all_invite(){
    this.invites_current_page = 0;
    this.getExhibitorInviteSummary();
  }

  /* export to csv */
  exportToInviteSummaryCSV(){

    document.getElementById('loader').style.display="block";
    let sort_dir="ASC";
    if(this.invites_sort_dir==1){
      sort_dir="DESC";
    }

    const exportParams = {...this.inviteSummaryParams(sort_dir), ...{"showall":1}};

    this.inviteService.getExhibitorInviteSummary(exportParams).subscribe(response =>{
        if(response['data']['success']){
          var passes = response['data']['records'];
          var csvdata = this.sharedService.JSONToCSVConvertor(passes, "Invite Summary Report", true);
          if(csvdata){
            var a         = document.createElement('a');
            a.href        = 'data:attachment/csv,' + escape(csvdata);
            a.target      = '_blank';
            a.download    = environment.greenCloudConfig.EventCode+'_InviteSummary.csv';
            document.body.appendChild(a);
            a.click();
          }
        }
        document.getElementById('loader').style.display="none";
    }, (err) => {
      document.getElementById('loader').style.display="none";
    }
    );
  }
}

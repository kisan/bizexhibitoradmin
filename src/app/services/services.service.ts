import { Injectable } from '@angular/core';

//import { Observable } from 'rxjs/Observable';
import {Router} from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class SevicesService {
  private greencloudApiUrl = environment.greenCloudConfig.ApiBaseUrl;
  constructor(private router:Router, private http: HttpClient) { }
  get_vendors_list(stallListParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(stallListParams);

    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"get_all_vendor_coordinators.php", body, { headers: headers })
    .toPromise()
  }
  allow_vendor_form_edit(stallListParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(stallListParams);

    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"update_vendor_coordinator_is_editable.php", body, { headers: headers })
    .toPromise()
  }
  get_stall_furniture_list(funritureStallListParams: any): Promise<any> {

    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(funritureStallListParams);
    // console.log(body);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"report_exhibitor_furniture_details.php", body, { headers: headers })
    .toPromise()
  }
  get_exhibitor_passes(exhibitorParams:any): Promise<any>{
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(exhibitorParams);
    // console.log(body);

    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"report_exhibitor_passes_overview.php", body, { headers: headers })
    .toPromise()
  }
  get_stall_passes(stallPassesParams:any): Promise<any>{
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(stallPassesParams);

    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"report_stall_passes.php", body, { headers: headers })
    .toPromise()
  }
  get_usage_details(usageDeatilsParams:any): Promise<any>{
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(usageDeatilsParams);
    console.log(body);
    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"getusage.php", body, { headers: headers })
    .toPromise()
  }
  resendPass(barcode:string){
    let resendPassParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "eventCode": environment.greenCloudConfig.EventCode,
      "source": environment.greenCloudConfig.Source,
      "sessionId":localStorage.getItem("sessionId"),
      "barcode":barcode
    }

    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(this.greencloudApiUrl + "resend_pass.php" , JSON.stringify(resendPassParams), {headers: headers})
      .toPromise();
  }
  send_vendor_form(stallListParams: any): Promise<any> {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(stallListParams);

    return this.http
    .post(environment.greenCloudConfig.ApiBaseUrl+"sendvendorcoordinatorform.php", body, { headers: headers })
    .toPromise()
  }

  getExhibitorInviteSummary (inviteSummaryParams: Object) {
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(inviteSummaryParams);

    return this.http.post(`${environment.greenCloudConfig.ApiBaseUrl}getexhibitorinvitesummary.php`, body, { headers: headers });
  }

  getInvitelist(data):Promise<any>{
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(data);
    return this.http.post(`${environment.greenCloudConfig.ApiBaseUrl}getinvitationlist.php`,body, { headers: headers })

    .toPromise()
  }
  getuncollectedexhibitorinvites(data):Promise<any>{
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(data);
    return this.http.post(`${environment.greenCloudConfig.ApiBaseUrl}getuncollectedexhibitorinvites.php`,body, { headers: headers })

    .toPromise()
  }
  markinvitecrmcalled(data):Promise<any>{
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let body = JSON.stringify(data);
    return this.http.post(`${environment.greenCloudConfig.ApiBaseUrl}invitation_mark_crm_called.php`,body, { headers: headers })

    .toPromise()
  }
}

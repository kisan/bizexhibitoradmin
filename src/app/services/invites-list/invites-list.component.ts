import { Component, OnInit } from '@angular/core';
import { SevicesService } from './../services.service';
import { environment } from '../../../environments/environment';
import { SharedService  } from '../../shared/shared.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-invites-list',
  templateUrl: './invites-list.component.html',
  styleUrls: ['./invites-list.component.scss']
})

export class InvitesListComponent implements OnInit {

  invites_list = [];
  invites_list_current_page:number = 0;
  pagesize:number = environment.greenCloudConfig.page_size;
  invites_list_search:string = "";
  invites_list_sort_col = "created_datetime";
  invites_list_sort_dir = 1;
  invites_list_filter = {"faircat_status":""};
  invites_list_pass_type:string = "onlineexhibitorinvite";
  invites_list_total_passes:number = 0;
  isSuccess:boolean;
  errorMessage:string;
  invites_list_start_item = 1;
  invites_list_end_item = environment.greenCloudConfig.page_size;
  invites_list_total_pages = 0;

  invitedPeople: any;
  totalUsers:number;
  status = [
    { show: 'Accepted', send: 'Collected'},
    { show: 'Attended', send: 'Visited'},
  ];
  selected='';
  state: string;
  city: string;
  objectKeys = Object.keys;
  states={};
  cities: any[] = [];

  constructor(private invitesService: SevicesService, private sharedService: SharedService , private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.get_invites_list();
    this.states=this.sharedService.getStates();
  }
  openMessage(message: string) {
    let action = null;
    this.snackBar.open(message, action, {
      duration: 10000,
    });
  }

  get_invites_list(){

    if(document.getElementById('loader')){
      document.getElementById('loader').style.display="block";
    }

    var sort_dir="ASC";
    if(this.invites_list_sort_dir==1){
        sort_dir="DESC";
    }

    this.invitesService.getInvitelist(this.inviteParams(sort_dir))
    .then(updateData=>{
      document.getElementById('loader').style.display="none";
      if (updateData["success"]){
        this.invitedPeople =  updateData['exhinfo']['invitation_list'];
        this.totalUsers = updateData['exhinfo']['total_count'];
        this.update_invites_list_statics();
        }
      else {
        this.invitedPeople =  [];
        // this.dummyData();

        this.totalUsers = 0;
        this.update_invites_list_statics();

      }

    })
    .catch(err => {
      console.log('err', err)
      document.getElementById('loader').style.display="none";
    })
  }
  inviteParams(sort_dir) {

     let inviteParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "eventCode": environment.greenCloudConfig.EventCode,
      "source": environment.greenCloudConfig.Source,
      "sessionId": localStorage.getItem('sessionId'),
      "type":'exhibitor',
      "passType": 'unlimitedexhibitorinvite',
      "pagesize": this.pagesize,
      "currentpage":this.invites_list_current_page,
      "search": this.invites_list_search,
      "sort_col": this.invites_list_sort_col,
      "sort_dir": sort_dir,
      "showall":0,
      "filter":{"t5.type":"exhibitor", "t1.state": this.state, "t1.district": this.city},
      "status":this.selected
     }
     return inviteParams;
   }


  update_invites_list_statics(){
    this.invites_list_total_pages = Math.ceil(this.totalUsers/this.pagesize);
    this.invites_list_start_item = ((this.invites_list_current_page*this.pagesize)+1);
    this.invites_list_end_item = ((this.invites_list_current_page*this.pagesize)+this.pagesize);
    if( this.invites_list_end_item > this.totalUsers){
        this.invites_list_end_item = this.totalUsers;
    }
  }

  showCities(state) {
    if(state == undefined) {
      this.reload_invites_list();
    }
    else {
      this.cities = [];
      this.cities = this.states[state];
      // this.city = this.cities[0];
      this.get_invites_list();
    }

  }

  selectedCity(city) {
  if(city != undefined) {
    this.get_invites_list();
  }

  }

  sort_invites_list_passes(key){
    if(this.invites_list_sort_col != key){
      this.invites_list_sort_col = key;
      this.invites_list_sort_dir = 0;
      this.invites_list_current_page = 0;
    }else{
        this.invites_list_sort_dir=1-this.invites_list_sort_dir;
    }
    this.get_invites_list();
  }
  search_invites_passes(event:any){
    if(event.keyCode == 13){
      this.invites_list_current_page=0;
      this.get_invites_list();
    }
  }
  reload_invites_list(){
    this.invites_list_search = "";
    this.invites_list_sort_col = "created_datetime";
    // "stall_id";
    this.invites_list_sort_dir = 1;
    this.invites_list_current_page = 0;
    this.get_invites_list();
  }
  getBase64URIencode(key){
    return this.sharedService.getBase64URIencode(key);
  }
  resendPass(barcode){

    this.invitesService.resendPass(barcode).then(response => {
    //  console.log(response);
      if(response['success']){

         this.openMessage(response['message']);
      }
      else{
        this.openMessage(response['message']);
      }
    })
    .catch(response => {
      // this.logger.error("Catch for resend pass", response);
    })
  }

  /* export to csv */
  exportInviteListCSV(){

    document.getElementById('loader').style.display="block";
    let inviteParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "eventCode": environment.greenCloudConfig.EventCode,
      "source": environment.greenCloudConfig.Source}
    this.invitesService.getuncollectedexhibitorinvites(inviteParams).then(response =>{

        if(response['success']){
          var passes = response['data'];
          var csvdata = this.sharedService.JSONToCSVConvertor(passes, "Invite List Report", true);
          console.log('csvdata', csvdata);
          if(csvdata){
            var csvBlob = new Blob([csvdata], { type: 'text/csv' });
            var csvUrl = URL.createObjectURL(csvBlob);
            var a         = document.createElement('a');
           // a.href        = 'data:attachment/csv,' + escape(csvdata);
            a.href = csvUrl;
            a.target      = '_blank';
            a.download    = environment.greenCloudConfig.EventCode+'_InviteList.csv';
            document.body.appendChild(a);
            a.click();
          }
        }
        document.getElementById('loader').style.display="none";
    })
    .catch( (err) => {
      document.getElementById('loader').style.display="none";
    })
  }

  getRupeeFormat(amount)
  {
    return this.sharedService.getRupeeFormat(amount);
  }

  redirect_crm_called(invite)
  {
    let supportCRMWebFormUrl = environment.samvaadConfig.webFormLink+invite['mobile'].slice(-10);
    if(invite['crm_called']==1)
    {
      window.open( supportCRMWebFormUrl ,'_blank');
    }
    else
    {
      document.getElementById('loader').style.display="block";
      let inviteParams = {
        "app_key": environment.greenCloudConfig.AppKey,
        "eventCode": environment.greenCloudConfig.EventCode,
        "source": environment.greenCloudConfig.Source,
        "invitationCode":invite['invitation_code']
      }
      this.invitesService.markinvitecrmcalled(inviteParams).then(response =>{
        document.getElementById('loader').style.display="none";
        window.open( supportCRMWebFormUrl ,'_blank');
      })
      .catch( (err) => {
        document.getElementById('loader').style.display="none";
        window.open( supportCRMWebFormUrl ,'_blank');
      });
    }
  }
}

/**
 * *Old Code
*/

/*

  get_invites_list(){

    if(document.getElementById('loader')){
      document.getElementById('loader').style.display="block";
    }

    var sort_dir="ASC";
    if(this.invites_list_sort_dir==1){
        sort_dir="DESC";
    }

    let invitesPassesParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "eventCode": environment.greenCloudConfig.EventCode,
      "source": environment.greenCloudConfig.Source,
      "pagesize":this.pagesize,
      "currentpage":this.invites_list_current_page,
      "search": this.invites_list_search,
      "sort_col": this.invites_list_sort_col,
      "sort_dir":sort_dir,
      "showall":0,
      "filter": this.invites_list_filter,
      "passType": this.invites_list_pass_type
    }

    this.invitesService.get_stall_passes(invitesPassesParams).then(response =>{
      // console.log(response);
      if(response.responseCode == 0 && response.success == true){
        this.isSuccess = true;
        this.invites_list = response.overview.records;
        this.invites_list_total_passes = response.overview.total.cnt;
        this.update_invites_list_statics();
      }else{
        this.isSuccess = false;
        this.errorMessage = response.message;
      }
      document.getElementById('loader').style.display="none";
    });
  }





/**
 * ! ------------------------------------  OLD CODE  -------------------------------------
 */


/* export class InvitesListComponent implements OnInit {

  dummyObject: { image1: string; userName: string; mobile: number; date: string; image2: string; }[];
  users: any;
  pageNumber:number=1;
  recordsFound: boolean = true;
  selectSearchType: boolean = true;
  pages:number=1;
  startItem:number=1;
  totalUsers:number;
  endItem=environment.greenCloudConfig.page_size;
  pagesize=environment.greenCloudConfig.page_size;
  invitedPeople: any;
  searchText= '';

  status = [
    { show: 'Accepted', send: 'Collected'},
    { show: 'Attended', send: 'Visited'},
  ];
  selected='';
  defaultExhibitorImage: string;
  message:string;


  constructor(
    private invitesService: SevicesService,
    private sharedService: SharedService ,
    private snackBar: MatSnackBar
  ) { }


  ngOnInit() {
    this.getInvitelistsuggestion()
  }


getInvitelistsuggestion(){
  if(document.getElementById('loader')){
    document.getElementById('loader').style.display="block";
  }
  this.invitesService.getInvitelist(this.inviteParams())
  .then(updateData=>{
    document.getElementById('loader').style.display="none";

    if (updateData["success"]){
      this.invitedPeople =  updateData['exhinfo']['invitation_list'];
      this.totalUsers = updateData['exhinfo']['total_count'];
      this.update_stastics();
      }
    else {
      this.invitedPeople =  [];
      this.totalUsers = 0;
      this.update_stastics();


    }
  })
}
inviteParams() {

  let inviteParams = {
    "app_key": environment.greenCloudConfig.AppKey,
    "eventCode": environment.greenCloudConfig.EventCode,
    "source": environment.greenCloudConfig.Source,
    "sessionId": localStorage.getItem('sessionId'),
    "type":'exhibitor',
    "passType": 'unlimitedexhibitorinvite',
    "pagesize": this.pagesize,
    "currentpage":this.pagesize-1,
    "search": this.searchText,
    "filter":{"t5.type":"exhibitor"},
    "status":this.selected
   }
   return inviteParams;
 }

 update_stastics() {
  this.pages=Math.ceil(this.totalUsers/this.pagesize);
  this.startItem = (((this.pageNumber-1)*this.pagesize)+1);
  this.endItem = (((this.pageNumber-1)*this.pagesize)+this.pagesize);
  if( this.endItem >this.totalUsers){
      this.endItem =this.totalUsers;
  }

}

onKeydown(event: any) {
  if (event.keyCode == 13) {
    this.pageNumber=1;
    this.getInvitelistsuggestion();
  }
}
clearText() {
  this.searchText = '';
  this.recordsFound = true;
  this.selectSearchType = true;

}
refreshData(){
  this.getInvitelistsuggestion();
}
} */



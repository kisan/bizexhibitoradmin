import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/auth-guard.service';
import { BadgesComponent } from './badges/badges.component';
import { BadgesListComponent } from './badges-list/badges-list.component';
import { InvitesComponent } from './invites/invites.component';
import { InvitesListComponent } from './invites-list/invites-list.component';
import { FurnitureComponent } from './furniture/furniture.component'; 
import { VendorListComponent } from './vendor-list/vendor-list.component';
import { VendorDetailsComponent } from './vendor-details/vendor-details.component';

const routes: Routes = [
  { path: '', redirectTo: 'badges', pathMatch: 'full'},
  {path: 'badges', component: BadgesComponent},
  {path: 'badges-list', component: BadgesListComponent},
  {path: 'invites', component: InvitesComponent},
  {path: 'invites-list', component: InvitesListComponent},
  {path: 'furniture', component: FurnitureComponent},
  {path: 'vendor-list', component: VendorListComponent},
  {path: 'vendor-details/:id', component:VendorDetailsComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicesRoutingModule { }

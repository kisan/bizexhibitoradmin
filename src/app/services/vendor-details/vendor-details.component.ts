import { Component, OnInit } from '@angular/core';
import { ActivatedRoute , Router ,} from '@angular/router';
import { environment } from '../../../environments/environment';
import { SevicesService } from '../services.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SharedService } from '../../shared/shared.service';
import { LoginComponent } from '../../auth/login/login.component';
@Component({
    selector: 'app-vendor-details',
    templateUrl: './vendor-details.component.html',
    styleUrls: ['./vendor-details.component.scss']
  })
  export class VendorDetailsComponent implements OnInit {
    stallId: string;
    defaultExhibitorImage: string;
    stallImages = [];
    stallImagePrefix: string;
    stallDetails = {};

    constructor( private loginComponent: LoginComponent, private route: ActivatedRoute, 
        private router:Router , private SevicesService: SevicesService,
        public dialog: MatDialog, private sharedService:SharedService) { }
    masterdata={'pavillion_types':[],"stand_types":[],"stand_locations":[]}
    ngOnInit() {
      this.route.params.forEach((urlParameters) => {
        this.stallId = this.getBase64URIdecode(urlParameters['id']);
        // console.log("Stall Id: ", this.stallId);
      });
      this.sharedService.getMasterData().then(response => {           
        //console.log(response);
        if(response.success)
        {
          this.masterdata=response.eventDetails; 
          this.loaddata();
          this.masterdata.pavillion_types.sort((a, b) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0);
        }
      });
    }

    loaddata()
    {
      document.getElementById('loader').style.display="block"; 
      let stallDetailsParams = {
        "app_key": environment.greenCloudConfig.AppKey,
        "source": environment.greenCloudConfig.Source,
        "eventCode": environment.greenCloudConfig.EventCode,
        "sessionId": localStorage.getItem('sessionId'),
        "pagesize":environment.greenCloudConfig.page_size,
        "currentpage":0,       
        "filter":{"t1.stall_id":this.stallId}
      }    
      //this.sharedService.getCategoryDetails().then(response => {        
      this.SevicesService.get_vendors_list(stallDetailsParams).then(stallData => {
        //console.log(stallData);
        if(!stallData.success && stallData.responseCode==104)
        {
          this.loginComponent.logout();
        }
        this.stallDetails = stallData.overview.records[0];
        //console.log(this.stallDetails);
      
        if ((this.stallDetails['logo_url']) && (this.stallDetails['logo_url'] != "")) {
          this.defaultExhibitorImage =  this.stallDetails['logo_url'];
        }
        else 
        {
          this.defaultExhibitorImage = '../../assets/images/default_organization.png';
        }      
        this.stallImagePrefix = environment.greenCloudConfig.vendor_stall_designs_url_prefix;
        if(this.stallDetails["stall_design_images"])
        {
            this.stallImages = this.stallDetails["stall_design_images"].split(',');
        }
        //console.log(this.stallImages); 
       // console.log( document.getElementsByName('loader1'));    
        document.getElementById('loader').style.display="none"; 
      });
      //});
    }
    getBase64URIencode(x)
    {
      return this.sharedService.getBase64URIencode(x);
    }
    getBase64URIdecode(x)
    {
      return this.sharedService.getBase64URIdecode(x);
    }
    getRupeeFormat(amount)
    {
      return this.sharedService.getRupeeFormat(amount);
    }   
    checkAllowedRoles(roles)
    {
      //console.log(roles);
      return this.sharedService.checkAllowedRoles(roles);
    }
    get_pavillion_name(short_code)
    {
      for (let pav of this.masterdata.pavillion_types) {
          if(pav.short_code==short_code)
          {
            return pav.name;
          }
      }
      return '';
    }  
    get_stand_type_name(short_code)
    {
      for (let stand_type of this.masterdata.stand_types) {
          if(stand_type.short_code==short_code)
          {
            return stand_type.name;
          }
      }
      return '';
    }  
    get_stand_location_name(short_code)
    {
      for (let stand_loc of this.masterdata.stand_locations) {
          if(stand_loc.short_code==short_code)
          {
            return stand_loc.name;
          }
      }
      return '';
    }
    allowVendorFormEdit() {
      document.getElementById('loader').style.display="block"; 
       let stallDetailsParams = {
        "app_key": environment.greenCloudConfig.AppKey,
        "source": environment.greenCloudConfig.Source,
        "eventCode": environment.greenCloudConfig.EventCode,
        "sessionId": localStorage.getItem('sessionId'),
        "stall_id": this.stallId
      }  
      this.SevicesService.allow_vendor_form_edit(stallDetailsParams).then(res => {
        // console.log(res);
        this.loaddata();
      });
    }
    openConfirmationDialog() {
      let dialogRef = this.dialog.open(ConfirmVendorFormEditPopupComponent,{
        panelClass: 'popup_dist',
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result == 'cancel') {
          return false;
        }
        if (result == 'save') {
         // this.allowVendorFormEdit();
         this.addVendorEntryForStall();
          return true;
        }
      }); //end dialogRef
    }

  addVendorEntryForStall() {
    document.getElementById('loader').style.display="block"; 
    let stallDetailsParams = {
      "app_key": environment.greenCloudConfig.AppKey,
      "source": environment.greenCloudConfig.Source,
      "eventCode": environment.greenCloudConfig.EventCode,
      "sessionId": localStorage.getItem('sessionId'),
      "stall_id": this.stallId
    }    
    this.SevicesService.send_vendor_form(stallDetailsParams).then(res => {
      // console.log(res);
      //this.loaddata();
      this.openVendorFormSentDialog();
    });
  }
  openVendorFormSentDialog() {
    document.getElementById('loader').style.display="none";
    let dialogRef = this.dialog.open(VendorFormSentComponent);
    dialogRef.afterClosed().subscribe(value => {
      //console.log(`Dialog sent: ${value}`); 
      //this.router.navigateByUrl('/stalls/stall-details/'+this.getBase64URIencode(this.stallId));
      document.getElementById('loader').style.display="block";
      this.loaddata();
    });
  }
  }

  @Component({
    providers: [VendorDetailsComponent],
    selector: 'app-confirm-popup',
    template: `
    <div class="confirmMsgPopup mrgn-t-lg">
    <div class="pad-l-md pad-r-md">
    <mat-dialog-actions class="pad-all-sm">
    <button mat-dialog-close class="closeBtn">
    <i class="material-icons">close</i>
    </button>
    </mat-dialog-actions>
    <mat-dialog-content class="mrgn-b-md">
      <h4 class="mrgn-b-md">Confirmation</h4>
      <!--<p>Are you sure you wish to allow to re-submit contractor & stand-design details by exhibitor. </p>-->
      <p>Are you sure you want to send vendor form to this exhibitor ?</p>
    </mat-dialog-content>
  </div>
    <div class="footer-btn">
    <mat-card-footer>
      <button type="button" mat-raised-button mat-dialog-close class="grayBtn" (click)="dialogRef.close('cancel')">Cancel</button>
      <button type="button" mat-raised-button class="greennewBtn" (click)="dialogRef.close('save')">Yes</button>
      </mat-card-footer>
    </div>
    </div>
    `
  })
  export class ConfirmVendorFormEditPopupComponent implements OnInit {
    stallDetailsLocal = {};
    constructor(public dialogRef: MatDialogRef<ConfirmVendorFormEditPopupComponent>, public dialog: MatDialog,
      private vendorlDetailsComponentRef: VendorDetailsComponent) { }
  
    ngOnInit() {
    }
  }


@Component({
  selector: 'app-approve-popup',
  template: `
<div class="errorMsgPopup">
<mat-dialog-actions class="pad-all-sm">
        <button mat-dialog-close class="closeBtn">
        <i class="material-icons">close</i>
        </button>
    </mat-dialog-actions>
  <mat-dialog-content>
  <mat-list>
  <mat-list-item>
    <mat-icon mat-list-icon><i class="material-icons">check</i></mat-icon>
    <h4 mat-line class="font-bold-six">Vendor form sent successfully</h4>  
  </mat-list-item>
</mat-list>
  </mat-dialog-content>
  </div>
`
})
export class VendorFormSentComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<VendorFormSentComponent>, public dialog: MatDialog) { }
  ngOnInit() {
  }
}
  